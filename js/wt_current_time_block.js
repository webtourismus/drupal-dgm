(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.wtCurrentTimeBlock = Drupal.behaviors.wtCurrentTimeBlock || {
    attach: function (context, settings) {
      let selectorString = '';
      for (let [key, value] of Object.entries(drupalSettings.wt.current_time)) {
        selectorString += '#' + key + ' ';
      }
      let $elements = $(once('wt_currenttime_block', selectorString, context));
      $elements.each( function() {
        $(this).html(moment().locale(drupalSettings.path.currentLanguage).format(drupalSettings.wt.current_time[this.id]['timeformat']));
        window.setInterval( function(elem) {
          $(elem).html(moment().locale(drupalSettings.path.currentLanguage).format(drupalSettings.wt.current_time[elem.id]['timeformat']));
        }, drupalSettings.wt.current_time[this.id]['updateinterval'], this);
      });
    }
  }
})(jQuery, Drupal, drupalSettings, once);
