window.iFrameResizer = {
  onReady: function() {
    window.parentIFrame.sendMessage('scrollParent');
  },
  onMessage: function(message) {
    console.info(message);
    if ('entryPage' in message) {
      if (document.location.pathname == encodeURI(message.entryPage)) {
        document.querySelector('body').classList.add('is-iframe-entrypage');
      }
      else {
        document.querySelector('body').classList.add('not-iframe-entrypage');
      }
    }
  }
};
