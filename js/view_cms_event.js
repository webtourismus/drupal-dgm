(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.wtViewCmsEventDatepicker = Drupal.behaviors.wtViewCmsEventDatepicker || {
    attach: function (context, settings) {
      $.datepicker.regional['de'] = {
        closeText: 'Schließen',
        prevText: '&#x3C;Zurück',
        nextText: 'Vor&#x3E;',
        currentText: 'Heute',
        monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
        dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
        weekHeader: 'KW',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
      };
      $.datepicker.setDefaults($.datepicker.regional[drupalSettings.path.currentLanguage]);
      let $elements = $(once('wtViewCmsEventDatepicker', '#views-exposed-form-cms-event input[name=end_value]', context));
      $elements.datepicker();
    }
  }
})(jQuery, Drupal, drupalSettings, once);
