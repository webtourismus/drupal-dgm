(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.wtWebcamInput = Drupal.behaviors.wtWebcamInput || {
    attach: function (context, settings) {
      let $elements = $(once('wt_webcam_input', '[name*="field_webcam[0][first]"]', context));
      $elements.on('change', function () {
        let $descriptions = $(this).parents('.form-item').find('[id*="--description"]');
        let $fitting_child = '.webcam__' + $(this).val();
        $descriptions.children(':not(' + $fitting_child + ')').hide();
        $descriptions.children($fitting_child).show();
      }).change();
    }
  }
})(jQuery, Drupal, drupalSettings, once);
