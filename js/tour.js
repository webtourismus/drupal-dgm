(function ($, Drupal, drupalSettings, once) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.leaflet = drupalSettings.wt.leaflet || {};
  drupalSettings.wt.leaflet.locale = drupalSettings.wt.leaflet.locale || {
    "Acceleration"      : Drupal.t("Acceleration"),
    "Altitude"          : Drupal.t("Altitude"),
    "Slope"             : Drupal.t("Slope"),
    "Speed"             : Drupal.t("Speed"),
    "Total Length: "    : Drupal.t("Total Length: "),
    "Max Elevation: "   : Drupal.t("Max Elevation: "),
    "Min Elevation: "   : Drupal.t("Min Elevation: "),
    "Total Time: "      : Drupal.t("Total Time: "),
    "Total Ascent: "    : Drupal.t("Total Ascent: "),
    "Total Descent: "   : Drupal.t("Total Descent: "),
    "Min Slope: "       : Drupal.t("Min Slope: "),
    "Max Slope: "       : Drupal.t("Max Slope: "),
    "Min Speed: "       : Drupal.t("Min Speed: "),
    "Max Speed: "       : Drupal.t("Max Speed: "),
    "Avg Speed: "       : Drupal.t("Avg Speed: "),
    "Min Acceleration: ": Drupal.t("Min Acceleration: "),
    "Max Acceleration: ": Drupal.t("Max Acceleration: "),
    "Avg Acceleration: ": Drupal.t("Avg Acceleration: ")
  };
  drupalSettings.wt.leaflet.maps = drupalSettings.wt.leaflet.maps || [];
  drupalSettings.wt.leaflet.settings = drupalSettings.wt.leaflet.settings || {};
  drupalSettings.wt.leaflet.settings.map = drupalSettings.wt.leaflet.settings.map || {};
  drupalSettings.wt.leaflet.settings.map.mapTypeId = drupalSettings.wt.leaflet.settings.map.mapTypeId || 'topo';
  drupalSettings.wt.leaflet.settings.map.mapTypeIds = drupalSettings.wt.leaflet.settings.map.mapTypeIds || ['streets', 'satellite', 'topo'];
  drupalSettings.wt.leaflet.settings.map.center = drupalSettings.wt.leaflet.settings.map.center || [47.43337563099771, 9.737894943748415];
  drupalSettings.wt.leaflet.settings.map.zoom = drupalSettings.wt.leaflet.settings.map.zoom || 8;
  drupalSettings.wt.leaflet.settings.map.pegmanControl = (typeof drupalSettings.wt.leaflet.settings.map.pegmanControl == 'undefined') ? false : drupalSettings.wt.leaflet.settings.map.pegmanControl;
  drupalSettings.wt.leaflet.settings.map.locateControl = (typeof drupalSettings.wt.leaflet.settings.map.locateControl == 'undefined') ? true : drupalSettings.wt.leaflet.settings.map.locateControl;
  drupalSettings.wt.leaflet.settings.map.editInOSMControl = (typeof drupalSettings.wt.leaflet.settings.map.editInOSMControl == 'undefined') ? false : drupalSettings.wt.leaflet.settings.map.editInOSMControl;
  drupalSettings.wt.leaflet.settings.map.searchControl = (typeof drupalSettings.wt.leaflet.settings.map.searchControl == 'undefined') ? false : drupalSettings.wt.leaflet.settings.map.searchControl;
  drupalSettings.wt.leaflet.settings.map.rotateControl = (typeof drupalSettings.wt.leaflet.settings.map.rotateControl == 'undefined') ? false : drupalSettings.wt.leaflet.settings.map.rotateControl;
  drupalSettings.wt.leaflet.settings.map.minimapControl = (typeof drupalSettings.wt.leaflet.settings.map.minimapControl == 'undefined') ? false : drupalSettings.wt.leaflet.settings.map.minimapControl;
  drupalSettings.wt.leaflet.settings.map.loadingControl = (typeof drupalSettings.wt.leaflet.settings.map.loadingControl == 'undefined') ? false : drupalSettings.wt.leaflet.settings.map.loadingControl;
  drupalSettings.wt.leaflet.settings.map.layersControl = (typeof drupalSettings.wt.leaflet.settings.map.layersControl == 'undefined') ? true : drupalSettings.wt.leaflet.settings.map.layersControl;
  drupalSettings.wt.leaflet.settings.elevation = drupalSettings.wt.leaflet.settings.elevation || {};
  drupalSettings.wt.leaflet.settings.elevation.theme = drupalSettings.wt.leaflet.settings.elevation.theme || 'lightblue-theme';
  drupalSettings.wt.leaflet.settings.elevation.followMarker = (typeof drupalSettings.wt.leaflet.settings.elevation.followMarker == 'undefined') ? true : drupalSettings.wt.leaflet.settings.elevation.followMarker;
  drupalSettings.wt.leaflet.settings.elevation.autofitBounds = (typeof drupalSettings.wt.leaflet.settings.elevation.autofitBounds == 'undefined') ? true : drupalSettings.wt.leaflet.settings.elevation.autofitBounds;
  drupalSettings.wt.leaflet.settings.elevation.legend = (typeof drupalSettings.wt.leaflet.settings.elevation.legend == 'undefined') ? false : drupalSettings.wt.leaflet.settings.elevation.legend;
  drupalSettings.wt.leaflet.settings.elevation.ruler = (typeof drupalSettings.wt.leaflet.settings.elevation.ruler == 'undefined') ? false : drupalSettings.wt.leaflet.settings.elevation.ruler;
  drupalSettings.wt.leaflet.settings.elevation.slope = (typeof drupalSettings.wt.leaflet.settings.elevation.slope == 'undefined') ? 'summary' : drupalSettings.wt.leaflet.settings.elevation.slope;

  Drupal.behaviors.wtLeafletTourWithElevation = Drupal.behaviors.wtLeafletTourWithElevation || {
    attach: function (context, settings) {
      L.registerLocale('current-drupal-language', drupalSettings.wt.leaflet.locale);
      L.setLocale('current-drupal-language');
      let $elements = $(once('wtLeafletTourWithElevation', '.leaflet__map--tour', context));
      $elements.each( function () {
        var id = $(this).attr('id');
        var file = $(this).attr('data-gpx-file');
        var map = L.map(id, drupalSettings.wt.leaflet.settings.map);
        var controlElevation = L.control.elevation(drupalSettings.wt.leaflet.settings.elevation);
        controlElevation.addTo(map);
        controlElevation.load(file);
        drupalSettings.wt.leaflet.map = map;
        drupalSettings.wt.leaflet.elevation = controlElevation;
        window.setTimeout(
          function() {
            var $source = $('#' + id + ' ~ .elevation-div').find('.elevation-summary');
            var $target = $(id.replace(/leaflet__map--/, '.leaflet__statscontainer--'));
            if ($source.length && $target.length) {
              $target.append($source.removeClass('elevation-summary').addClass('leaflet__statstable'));
            }
            /* need a timeout before doing this */
            var tourMarker = new L.marker([drupalSettings.wt.leaflet.elevation._data[0].x, drupalSettings.wt.leaflet.elevation._data[0].y]);
            tourMarker.bindPopup(Drupal.t('Tour Start'));
            tourMarker.addTo(drupalSettings.wt.leaflet.map);
            if (drupalSettings?.wt_dgm?.user?.field_geo) {
              var bounds = drupalSettings.wt.leaflet.elevation.getBounds();
              var greenIcon = new L.Icon({
                iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
              });
              var userMarker = new L.marker(drupalSettings.wt_dgm.user.field_geo, {icon: greenIcon});
              userMarker.bindPopup(drupalSettings.wt_dgm.user.field_marketing_name);
              userMarker.addTo(drupalSettings.wt.leaflet.map);
              bounds.extend(userMarker.getLatLng());
              drupalSettings.wt.leaflet.map.fitBounds(bounds);
            }
          },
          1000
        );
      });
    }
  }
})(jQuery, Drupal, drupalSettings, once);
