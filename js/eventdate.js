(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.wtEventdate = Drupal.behaviors.wtEventdate || {
    attach: function (context, settings) {
      let $elements = $(once('wt_eventdate_helper__btn--hour', '.wt_eventdate_helper__btn--day', context));
      $elements.on('click', function () {
        let $startDate = $(this).parents('fieldset').find('input[type=date][name*="[value]"]');
        let $endDate = $(this).parents('fieldset').find('input[type=date][name*="[end_value]"]');
        let $endTime = $(this).parents('fieldset').find('input[type=time][name*="[end_value]"]');
        if ($startDate.val()) {
          $endDate.val($startDate.val());
          if ($endTime.length == 1) {
            $endTime.val('23:59:00');
          }
        }
      });
      let $elements2 = $(once('wt_eventdate_helper__btn--hour', '.wt_eventdate_helper__btn--hour', context));
      $elements2.on('click', function () {
        let $startDate = $(this).parents('fieldset').find('input[type=date][name*="[value]"]');
        let $startTime = $(this).parents('fieldset').find('input[type=time][name*="[value]"]');
        let $endDate = $(this).parents('fieldset').find('input[type=date][name*="[end_value]"]');
        let $endTime = $(this).parents('fieldset').find('input[type=time][name*="[end_value]"]');
        if ($startDate.val() && $startTime.val()) {
          $endDate.val($startDate.val());
          let $endTimeArray = $startTime.val().split(':');
          console.info($endTimeArray);
          $endTimeArray[0] = (parseInt($endTimeArray[0]) + 1).toString().padStart(2, '0');
          $endTime.val($endTimeArray.join(':'));
        }
      });
    }
  }
})(jQuery, Drupal, drupalSettings, once);
