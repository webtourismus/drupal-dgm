(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.wtShareIframe = Drupal.behaviors.wtShareIframe || {
    attach: function (context, settings) {
      let $elements = $(once('wt_share_iframe_switch_lang', 'input[name="eii__lang"]', context));
      $elements.on('click', function() {
        let langPrefix = $(this).val();
        $('.eii__code').each( function() {
          if (langPrefix == 'en') {
            $(this).val( $(this).val().replace(/src="https:\/\/([^\/]+)\/dgm\//, 'src="https://$1/en/dgm/') );
            $(this).val( $(this).val().replace(/'entryPage': '\/dgm\//, "'entryPage': '/en/dgm/") );
          }
          else {
            $(this).val( $(this).val().replace(/src="https:\/\/([^\/]+)\/en\/dgm\//, 'src="https://$1/dgm/') );
            $(this).val( $(this).val().replace(/'entryPage': '\/en\/dgm\//, "'entryPage': '/dgm/") );
          }
        })
      });
      let $elements2 = $(once('wt_share_iframe_adjust_offset', 'input[name="eii__offset"]', context));
      $elements2.on('change', function() {
        var offset = $(this).val() || 0;
        $('.eii__code').each( function() {
          $(this).val( $(this).val().replace(/dgmDoSmoothScroll\(obj\.iframe, \d*\)/, 'dgmDoSmoothScroll(obj.iframe, ' + offset + ')') );
        })
      });
      let $elements3 = $(once('wt_share_iframe_copy_code', '.eii__copy', context));
      $elements3.on('click', function() {
        $(this).siblings('.eii__code').select();
        document.execCommand("copy");
      });
      let $elements4 = $(once('wt_share_set_custom_page', 'input[name="eii__input--select-page"]', context));
      $elements4.on('change', function() {
        try {
          let iframeDomain = drupalSettings.wt.share_iframe.domain_iframe;
          let inputUrl = new URL($(this).val(), iframeDomain);
          let iframeUrl = new URL(inputUrl.pathname, iframeDomain);
          let $codeArea = $('.eii__code--page');
          $codeArea.val( $codeArea.val().replace(/<iframe src="([^"]*)"/, '<iframe src="' + iframeUrl.toString() + '"') );
          $codeArea.val( $codeArea.val().replace(/{'entryPage': '([^']*)'/, "{'entryPage': '" + iframeUrl.pathname + "'") );
          $('#dgm_iframe_page').attr('src', encodeURI(iframeUrl.toString()))
        }
        catch (e) { /* ignore invalid URLs  */}
      });
    }
  }
})(jQuery, Drupal, drupalSettings, once);
