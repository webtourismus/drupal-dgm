<?php

namespace Drupal\wt_dgm\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *  Changes theme bases on the DGM channel (subdomain)
 */
class ChannelThemeNegotiator implements ThemeNegotiatorInterface {
  /**
   * @var $dgmHelper DgmHelper
   */
  protected $dgmHelper;

  /**
   * @var $configFactory \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(DgmHelper $dgm_helper, ConfigFactoryInterface $config_factory) {
    $this->dgmHelper = $dgm_helper;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('wt_dgm.helper'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($this->dgmHelper->getFrontendUser()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $currentChannel = $this->dgmHelper->getChannel();
    $config = $this->configFactory->get('wt_dgm.settings');
    $theme = $config->get('channel_themes.' . $currentChannel);
    if ($theme) {
      return $theme;
    }

    return $config->get('default_channel');
  }
}
