<?php

namespace Drupal\wt_dgm\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *  Force admin theme on a selected set of pages
 */
class AdminThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * @var $configFactory \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    /**
     * Force admin theme on DGM dashboard nodes
     */
    if ($node = $route_match->getParameter('node')) {
      if (!$node instanceof Node) {
        $node = Node::load($node);
      }
      if ($node && $node->bundle() == 'dgmdashboard') {
        return TRUE;
      }
    }
    /**
     * Force admin theme on user profile pages
     */
    if ($user = $route_match->getParameter('user')) {
      if (!$user instanceof User) {
        $user = User::load($user);
      }
      if ($user) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->configFactory->get('system.theme')->get('admin');
  }
}
