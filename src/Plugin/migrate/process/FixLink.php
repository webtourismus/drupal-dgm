<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Tries to fix incorrect external links without schema (like "domain.com")
 * by adding a proper schema and ensuring a hostname is given
 *
 * Available keys:
 * - @string source: a probably incorrect URL
 * - @string prefix: if the source does not contain a scheme, prefix the source with this schema string, defaults to 'http://'
 *
 * Example:
 *
 * @code
 * source:
 *   constants:
 *     unchanged_valid_link: 'https://www.domain.com'
 *     fixable_link1: 'www.domain.com'
 *     fixable_link2: 'domain.com/path?param1=value1'
 *     not_allowed_link1: '/no-domain'
 *     not_allowed_link2: 'hello@email.com'
 *     not_allowed_link3: 'tel:0123456789'
 *     not_allowed_link4: 'ftp://domain.com'
 * process:
 *   field_homepage/uri:
 *     plugin: fix_link
 *     source: constants/XXX
 *     prefix: 'http://'
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "fix_link"
)
 */
class FixLink extends ProcessPluginBase {

  protected const DEFAULT_PREFIX = 'http://';

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      return NULL;
    }
    if (!is_string($value)) {
      throw new MigrateException(sprintf('fix_link %s: Source must be a string', var_export($value, TRUE)));
    }
    $prefix = (is_string($this->configuration['prefix'] ?? NULL) && $this->configuration['prefix'] != '') ? $this->configuration['prefix'] : self::DEFAULT_PREFIX;

    $url = trim($value);
    $arr = parse_url($url);
    if (!$arr || !($arr['scheme'] ?? FALSE)) {
      $url = $prefix . $url;
      $arr = parse_url($url);
    }
    if (!$arr) {
      throw new MigrateSkipProcessException(sprintf('fix_link %s: Could not be parsed into an URL', var_export($value, TRUE)));
    }
    if (!in_array($arr['scheme'], ['http', 'https']) || stripos($url, $arr['scheme'] . '://') !== 0) {
      throw new MigrateSkipProcessException(sprintf('fix_link %s: %s is an invalid scheme, only "http://" and "https://" links are allowed', var_export($url, TRUE), var_export($arr['scheme'], TRUE)));
    }
    if (!($arr['host'])) {
      throw new MigrateSkipProcessException(sprintf('fix_link %s: Must be an external link, but does not contain a host', var_export($url, TRUE)));
    }
    return $url;
  }
}
