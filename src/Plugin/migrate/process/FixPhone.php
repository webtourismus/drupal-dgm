<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use \libphonenumber\PhoneNumber;
use \libphonenumber\PhoneNumberFormat;
use \libphonenumber\PhoneNumberUtil;

/**
 * Tries to fix incorrect phone numbers (like "05572 / 90610")
 * by removing non-digits and prefixing a country code ("0043557290610")
 *
 * Available keys:
 * - @string source: a probably incorrect phone number
 * - @string country: ISO2 country code; if the source does not contain a country code, assume a number from this country, defaults to 'AT'
 *
 * Example:
 *
 * @code
 * source:
 *   constants:
 *     unchanged_valid_phone: 0043557290610
 *     repairable_phone1: 05572/90610
 *     repairable_phone2: +43 5572/90610
 *     repairable_phone3: 05572/90610
 *     repairable_phone4: 05572/90610 DW 4
 *     unfixable_phone2: 1
 *     unfixable_phone3: 05572/90610 oder 0664/8496850
 * process:
 *   field_phone:
 *     plugin: fix_phone
 *     source: constants/XXX
 *     country: 'AT'
 * @endcode
 *
 * Given the keys above,
 *
 * @MigrateProcessPlugin(
 *   id = "fix_phone"
)
 */
class FixPhone extends ProcessPluginBase {

  protected const DEFAULT_COUNTRY = 'AT';

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      return NULL;
    }
    if (!is_string($value)) {
      throw new MigrateException(sprintf('fix_phone %s: Source must be a string', var_export($value, TRUE)));
    }
    $country = (is_string($this->configuration['country']) && $this->configuration['country'] != '') ? $this->configuration['country'] : self::DEFAULT_COUNTRY;

    $phoneUtil = PhoneNumberUtil::getInstance();
    try {
      /** @var $nr PhoneNumber */
      $nr = $phoneUtil->parse($value, $country);
      if ($phoneUtil->isValidNumber($nr)) {
        return str_replace('+', '00', $phoneUtil->format($nr, PhoneNumberFormat::E164));
      }
      throw new \Exception('No valid phone number');
    }
    catch (\Exception $e) {
      throw new MigrateSkipProcessException(sprintf('fix_phone %s: ' . $e->getMessage(), var_export($value, TRUE)));
    }
  }
}
