<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Converts an address string into a numerical latitude/longitude points using
 * Google Maps' Geocoding API (requires billing-enabled API key).
 *
 * Returns [@float lat, @float lng] on success or null
 *
 * Required:
 * - source: @string full address
 * - api_key: @string Google API key used for billing
 *
 * Example:
 *
 * @code
 * source:
 *   constants:
 *     full_address_string: '1600 Amphitheatre Parkway, Mountain View, CA'
 *     street: 'Musterstrasse 1'
 *     zip: '12345'
 *     city: 'Musterort'
 *     country: 'Germany'
 *     comma: ','
 *     space: ' '
 * process:
 *   field_geo1:
 *     plugin: geocoding
 *     source: full_address_string
 *     api_key: MY_API_KEY
 *   field_geo2:
 *     -
 *       plugin: coalesce
 *       source:
 *         - constants/street
 *         - constants/comma
 *         - constants/space
 *         - constants/zip
 *         - constants/space
 *         - constants/city
 *         - constants/comma
 *         - constants/space
 *         - constants/country
 *     -
 *       plugin: geocoding
 *       api_key: MY_API_KEY
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "geocoding"
 * )
 *
 */
class Geocoding extends ProcessPluginBase {

  const GOOGLE_MAPS_ENDPOINT = 'https://maps.googleapis.com/maps/api/geocode/json?key=:key&address=:address';

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!is_string($value)) {
      return null;
    }
    $value = trim($value, "./-;, \t\n\r\0\x0B");
    if (strlen($value) <= 3) {
      return null;
    }

    $url = strtr(self::GOOGLE_MAPS_ENDPOINT, [':key' => $this->configuration['api_key'], ':address' => urlencode($value)]);
    $jsonString = file_get_contents($url);
    $json = json_decode($jsonString);

    if (!property_exists($json, 'results') || (!$json->results[0]->geometry->location->lat && !$json->results[0]->geometry->location->lng)) {
      return null;
    }

    return ['lat' => $json->results[0]->geometry->location->lat, 'lng' => $json->results[0]->geometry->location->lng];
  }
}
