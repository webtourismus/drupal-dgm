<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\webform\Plugin\WebformElement\DateTime;

/**
 * Transform a feratel event date structure into a DGM event daterange
 *
 * Params:
 *   startdate: YYYY-MM-DD, must not empty
 *   enddate: YYYY-MM-DD, must not empty
 *   starttime: HH:MM:SS, defaults to 00:00:00
 *   monday: boolean, defaults to false
 *   tuesday: boolean, defaults to false
 *   wednesday: boolean, defaults to false
 *   thursday: boolean, defaults to false
 *   friday: boolean, defaults to false
 *   saturday: boolean, defaults to false
 *   sunday: boolean, defaults to false
 *
 * All source params are assumed to be in Timezone Europe/Vienna
 *
 * Feratel provides something like:
 * <Dates>
 *  <Date From="2020-10-05" To="2020-10-15" />
 * </Dates>
 * <StartTimes>
 *   <StartTime Time="20:00:00" Mon="false" Tue="true" Wed="false" Thu="true" Fri="false" Sat="false" Sun="false" />
 * </StartTimes>
 *
 * - If start time is missing, 00:00:00 CET is assumed.
 * - End time will always be 23:59:00 CET
 * - If no weekdays are given, it is assumed to be a single-cardinality daterange
 *     (optionally spanning multiple days, like one weekend from Fri, 2020-10-16 to Sun, 2020-10-18)
 * - If at least one weekday is given, it is assumed to be a multi-cardinality daterange
 *     (see example above "from 2020-10-05 to 2020-10-15" ==> 4 dateranges: on each Tuesday and Thursday, each one starting on 20:00 and ending at 23:59 of same day)
 *
 * @code
 * source:
 *   constants:
 *     startdate: '2020-10-05'
 *     enddate: '2020-10-15'
 *     starttime: '20:00:00'
 *     tuesday: true
 *     thursday: true
 * process:
 *   field_daterange1:
 *     plugin: ferateldate_to_dgmdate
 *     source:
 *       - startdate
 *       - enddate
 *   field_daterange2:
 *     plugin: ferateldate_to_dgmdate
 *     source:
 *       - startdate
 *       - enddate
 *       - starttime
 *       - monday
 *       - tuesday
 *       - wednesday
 *       - thursday
 *       - friday
 *       - saturday
 *       - sunday
 *
 * @endcode
 *
 * (simplified CET) field_daterange1 pseudo-result will be like
 * [
 *   [value => '2020-10-05 00:00', endvalue => '2020-10-15 23:59'],
 * ]
 * (simplified CET) field_daterange2 pseudo-result will be like
 * [
 *   [value => '2020-10-06 20:00', endvalue => '2020-10-06 23:59'],
 *   [value => '2020-10-08 20:00', endvalue => '2020-10-08 23:59'],
 *   [value => '2020-10-13 20:00', endvalue => '2020-10-13 23:59'],
 *   [value => '2020-10-15 20:00', endvalue => '2020-10-15 23:59'],
 * ]
 *
 * The results will also be converted to UTC timezone afterwards, the real result for field_daterange1 would be
 * [
 *   [value => '2020-10-04T23:00:00', endvalue => '2020-10-14T22:59:00', canceled => 0, soldout => 0],
 * ]
 *
 * @MigrateProcessPlugin(
 *   id = "ferateldate_to_dgmdate"
 * )
 */
class FeratelDateToDgmDate extends ProcessPluginBase {

  const ENDTIME = '23:59:00';
  const CANCELED = 0;
  const SOLDOUT = 0;


  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      return NULL;
    }

    if (!is_array($value)) {
      throw new MigrateException("The input value must be an array.");
    }

    if (count($value) != 10) {
      throw new MigrateException( sprintf("The input value must be an array with 10 values, '%s' given", count($value)) );
    }

    if (empty($value[0]) || empty($value[1])) {
      throw new MigrateException("The start date and end date must not be empty.");
    }

    if (preg_match('/^20[0-9]{2}-[0-1][0-9]-[0-3][0-9]$/', $value[0]) != 1) {
      throw new MigrateException( sprintf("The start date must be formatted YYYY-MM-DD, '%s' given", $value[0]) );
    }

    if (preg_match('/^20[0-9]{2}-[0-1][0-9]-[0-3][0-9]$/', $value[1]) != 1) {
      throw new MigrateException( sprintf("The end date must be formatted YYYY-MM-DD, '%s' given", $value[1]) );
    }

    if ($value[2]) {
      if (preg_match('/^(2[0-3])|([0-1][0-9]):[0-5][0-9]:[0-5][0-9]$/', $value[2]) == 1) {
        $time = $value[2];
      }
      else {
        throw new MigrateException( sprintf("The start time must be formatted H:i:s, '%s' given", $value[2]) );
      }
    }
    else {
      $value[2] = $time = '00:00:00';
    }

    $tzVienna = new \DateTimeZone('Europe/Vienna');
    $tzUTC = new \DateTimeZone('UTC');


    $hasWeekdays = FALSE;
    for ($i=3; $i<10; $i++) {
      if (!array_key_exists($i, $value)) {
        $value[$i] = FALSE;
      }
      $value[$i] = (bool) ($value[$i] === 'false' ? FALSE : $value[$i]);
      $hasWeekdays = $hasWeekdays || $value[$i];
    }

    if (($value[0] == $value[1]) || !$hasWeekdays){
      $start = \DateTime::createFromFormat("Y-m-d H:i:s", $value[0] . ' ' . $time, $tzVienna);
      $end = \DateTime::createFromFormat("Y-m-d H:i:s", $value[1] . ' ' . self::ENDTIME, $tzVienna);
      return [0 => [
        'value' => $start->setTimezone($tzUTC)->format('Y-m-d\TH:i:s'),
        'end_value' => $end->setTimezone($tzUTC)->format('Y-m-d\TH:i:s'),
        'canceled' => self::CANCELED,
        'soldout' => self::SOLDOUT,
      ]];
    }

    //else multiple cardinality
    $begin = new \DateTime($value[0] . 'T' . $time, $tzVienna);
    $end = new \DateTime($value[1] . 'T' . self::ENDTIME, $tzVienna);
    $interval = \DateInterval::createFromDateString('1 day');
    $period = new \DatePeriod($begin, $interval, $end);

    $multiCardinalityResult = [];
    foreach ($period as $day) {
      $weekday = (int) $day->format('N');
      //the weekday booleans start at offset 3 of $value array
      if ($value[$weekday + 2]) {
        $startDate = $day->setTimezone($tzUTC)->format('Y-m-d\TH:i:s');
        $endDate = $day->setTimeZone($tzVienna)->setTime(...explode(':', self::ENDTIME))->setTimezone($tzUTC)->format('Y-m-d\TH:i:s');
        $multiCardinalityResult[] = [
          'value' => $startDate,
          'end_value' => $endDate,
          'canceled' => self::CANCELED,
          'soldout' => self::SOLDOUT,
        ];
      }
    }

    return $multiCardinalityResult;
  }
}
