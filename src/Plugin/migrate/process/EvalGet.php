<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns a source property by evaluating the source value.
 *
 * Core's "get" plugin always expects the source value to be a
 * constant/string name of a property. This plugin in contrast
 * treats the source value like a variable and first evaluates
 * it, and then returns the source matching the calculated name.
 *
 * Example:
 *
 * @code
 * source:
 *   fields:
 *     -
 *       name: foo
 *       selector: foo
 *     -
 *       name: Monday
 *       selector: monday
 *     -
 *       name: Tuesday
 *       selector: tuesday
 *     -
 *       ...
 *   constants:
 *     php_date_format: 'l'
 * process:
 *   field_bar:
 *     -
 *       plugin: callback
 *       callable:
 *         - '\Drupal\my_module\MyHelperClass'
 *         - 'calculateSourceForBar'
 *       source: foo
 *     -
 *       plugin: eval_get
 *   field_today:
 *     -
 *       plugin: callback
 *       callable: date
 *       source: constants/php_date_format
 *     -
 *       plugin: eval_get
 * @endcode
 *
 * The first example selects the source column for field_bar depending on the
 * return value of \Drupal\my_module\MyHelperClass::calculateSourceForBar($foo)
 *
 * The second example selects the source column for field_today depending on the
 * current day of week.
 *
 * @MigrateProcessPlugin(
 *   id = "eval_get"
 * )
 */
class EvalGet extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!is_null($value)) {
      return $row->get($value);
    }
    return null;
  }
}
