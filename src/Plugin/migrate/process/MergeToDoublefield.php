<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Merges two separate arrays of same length into a single array to be used
 * by for a field with multiple subvalues (like core URL or contrib double_field)
 *            AND with multiple cardinality.
 *
 * You must provide two arrays of same length as source.
 *
 * Available configuration keys:
 * - source: array of sub_arrays to be merged
 *
 * Example:
 *
 * @code
 * source:
 *   titles:
 *     - 'A search engine'
 *     - 'A social network'
 *     - 'An online shop'
 *   urls:
 *     - 'google.com'
 *     - 'facebook.com'
 *     - 'amazon.com'
 * process:
 *   my_temp_array:
 *     plugin: merge_to_doublefield
 *     source:
 *       - titles
 *       - urls
 *
 *   # double_fields can be feeded 1:1
 *   field_contrib_doublefields: '@my_temp_array'
 *
 *   # but you can remap the sub-keys "first" and "second" for other fields
 *   # using the sub_process plugin
 *   field_core_links:
 *     plugin: sub_process
 *     source: '@my_temp_array'
 *       title: first
 *       uri: second
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "merge_to_doublefield"
 * )
 *
 */
class MergeToDoublefield extends ProcessPluginBase {

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_null($value) || empty($value)) {
      return NULL;
    }

    if (!is_array($value)) {
      throw new MigrateException(sprintf('%s is not an array', var_export($value, TRUE)));
    }

    if (count($value) != 2) {
      throw new MigrateException('merge_to_doublefield requires an array of two sources');
    }

    if (is_null($value[0]) && is_null($value[1])) {
      return NULL;
    }

    if (is_scalar($value[0]) && is_scalar($value[1])) {
      $this->multiple = FALSE;
    }
    elseif (is_array($value[0]) && is_array($value[1])) {
      if  (count($value[0]) != count($value[1])) {
        throw new MigrateException('Both source arrays need to be of same length');
      }
      if (count($value[0]) == 0) {
        return NULL;
      }
      $this->multiple = TRUE;
    }
    else {
      throw new MigrateException('Both sources need to be either arrays or scalars');
    }

    if ($this->multiple) {
      $result = [];
      foreach ($value[0] as $key => $first) {
        $result[] = ['first' => $first, 'second' => $value[1][$key]];
      }
    }
    else {
      $result = ['first' => $value[0], 'second' => $value[1]];
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }
}
