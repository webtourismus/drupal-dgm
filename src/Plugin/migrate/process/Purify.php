<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use HTMLPurifier;
use HTMLPurifier_Config;

/**
 * Wrapper for HTML Purifier
 * @see http://htmlpurifier.org/
 *
 * Additional setup required, run this in Drupal root directory:
 * composer require ezyang/htmlpurifier
 *
 * Available configuration keys:
 * - source: Source property.
 *
 * Example:
 *
 * @code
 * source:
 *   constants:
 *     ugly_html: '<p>This is real content.</p><!--[if gte mso 9]><xml><msoffice:crap>REMOVE_THIS<msoffice:crap></xml><![endif]-->'
 * process:
 *   body/value:
 *     plugin: purify
 *     source: constants/ugly_html
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "purify"
  )
 */
class Purify extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    return $purifier->purify($value);
  }
}
