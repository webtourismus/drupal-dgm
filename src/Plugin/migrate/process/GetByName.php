<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns a source property by variable name.
 *
 * Core's "get" plugin always expects the source value to be a
 * constant/string name of a property. This plugin in contrast
 * treats the source value like a variable.
 *
 * Example:
 *
 * @code
 * source:
 *   fields:
 *     -
 *       name: foo
 *       selector: foo
 *     -
 *       name: Monday
 *       selector: monday
 *     -
 *       name: Tuesday
 *       selector: tuesday
 *     -
 *       ...
 *   constants:
 *     php_date_format: 'l'
 * process:
 *   field_bar:
 *     -
 *       plugin: callback
 *       callable:
 *         - '\Drupal\my_module\MyHelperClass'
 *         - 'getFieldForBar'
 *       source: bar
 *     -
 *       plugin: get_by_name
 *   field_today:
 *     -
 *       plugin: callback
 *       callable: date
 *       source: constants/php_date_format
 *     -
 *       plugin: get_by_name
 * @endcode
 *
 * The first example selects the source column for field_bar depending on the
 * return value of \Drupal\my_module\MyHelperClass::getFieldForBar($foo)
 *
 * The second example selects the source column for field_today depending on the
 * current day of week.
 *
 * @MigrateProcessPlugin(
 *   id = "get_by_name"
 * )
 */
class GetByName extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!is_null($value)) {
      return $row->get($value);
    }
    return null;
  }
}
