<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Wrapper for PHP's preg_match function, returns array of matches
 * @see https://www.php.net/manual/de/function.preg-match.php
 *
 * Available configuration keys:
 * - source: Source property.
 * - pattern: string search pattern
 * - flags: int $flags parameter for PHP's preg_match, default 0
 * - offset: int $offset parameter for PHP's preg_match, default 0
 *
 * Example:
 *
 * @code
 * source:
 *   constants:
 *     dummy_source: 'I want to get the date 2019-12-24 from this string'
 * process:
 *   field_iso_date:
 *     -
 *       plugin: preg_match
 *       source: constants/dummy_source
 *       pattern: '/(\d{4})-(\d{2})-(\d{2})/'
 *     -
 *       plugin: extract
 *       index: 0
 *   field_month:
 *     -
 *       plugin: preg_match
 *       source: constants/dummy_source
 *       pattern: '/(\d{4})-(\d{2})-(\d{2})/'
 *     -
 *       plugin: extract
 *       index: 2
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "preg_match"
 *  * )
 */
class PregMatch extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $pattern = $this->configuration['pattern'];
    $flags = $this->configuration['flags'] ?: 0;
    $offset = $this->configuration['offset'] ?: 0;
    $matches = [];

    if (is_null($pattern) || is_null($value)) {
      return [];
    }

    preg_match($pattern, $value, $matches, $flags, $offset);
    return $matches;
  }
}
