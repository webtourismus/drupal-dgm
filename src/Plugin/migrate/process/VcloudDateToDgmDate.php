<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use RRule\RSet;
use RRule\RRule;

/**
 * Transforms an array of vCloud event schedules into DGM date field vlaues
 *
 * @returns a single wrapper array containing an array of daterange-arrays
 *
 * @MigrateProcessPlugin(
 *   id = "vclouddate_to_dgmdate",
 *   handle_multiples = TRUE
 * )
 */
class VcloudDateToDgmDate extends ProcessPluginBase {

  /**
   * Translate ISO 8601 to iCal
   */
  const FREQEUNCY_MAP = [
    'Y' => RRule::YEARLY,
    'M' => RRule::MONTHLY,
    'W' => RRule::WEEKLY,
    'D' => RRule::DAILY,
  ];
  const LOCAL_TIMEZONE = 'Europe/Vienna';
  const BEGIN_OF_DAY = '00:00:00';
  const END_OF_DAY = '23:59:00';

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      return NULL;
    }

    if (!is_array($value)) {
      throw new MigrateException("The input value must be an array.");
    }
    if (array_key_exists('@id', $value)) {
      $value = [$value];
    }

    $result = [];
    $storageTimezone = new \DateTimeZone('UTC');

    foreach ($value as $schedule) {
      $localTimezone = new \DateTimeZone($schedule['scheduleTimezone'] ?? self::LOCAL_TIMEZONE);
      $rset = new RSet();
      $freq = [];

      if (isset($schedule['repeatFrequency'])) {
        preg_match('/^P(\d+)([YMWD])/', $schedule['repeatFrequency'], $freq);
        $dtStart = new \DateTime(
          $schedule['startDate'] . 'T' . ($schedule['startTime'] ?? self::BEGIN_OF_DAY),
          $localTimezone
        );
        $rrule = [
          'DTSTART' => $dtStart,
          'FREQ' => self::FREQEUNCY_MAP[$freq[2]],
          'INTERVAL' => $freq[1],
        ];
        if (isset($schedule['endDate'])) {
          $until = new \DateTime(
            $schedule['endDate'] . 'T' . ($schedule['endTime'] ?? self::END_OF_DAY),
            $localTimezone
          );
          $rrule['UNTIL'] = $until;
        }
        elseif (isset($schedule['repeatCount'])) {
          $rrule['COUNT'] = $schedule['repeatCount'];
        }
        elseif (isset($schedule['duration'])) {
          $until = clone($dtStart);
          $until->add(new \DateInterval($schedule['duration']));
          $rrule['UNTIL'] = $until;
        }
        else {
          /**
           * prevent incomplete data from creating unlimited occurrences
           */
          $rrule['COUNT'] = 1;
        }
        if (isset($schedule['byDay'])) {
          $byDay = $schedule['byDay'];
          if (!is_array($schedule['byDay'])) {
            $byDay = [$byDay];
          }
          $byDay = array_map(
            function($day) {
              $day = str_ireplace('https://schema.org/', '', $day);
              return strtoupper(substr($day, 0, 2));
            },
            $byDay);
          $rrule['BYDAY'] = $byDay;
        }
        $rset->addRRule($rrule);
      }
      else {
        $dtStart = new \DateTime(
          $schedule['startDate'] . 'T' . ($schedule['startTime'] ?? self::BEGIN_OF_DAY),
          $localTimezone
        );
        $rset->addDate($dtStart);
      }

      if (isset($schedule['dc:additionalDate'])) {
        $additionalDates = $schedule['dc:additionalDate'];
        if (!is_array($schedule['dc:additionalDate'])) {
          $additionalDates = [$additionalDates];
        }
        foreach ($additionalDates as $additionalDate) {
          $rset->addDate($additionalDate);
        }
      }

      if (isset($schedule['exceptDate'])) {
        $exceptDates = $schedule['exceptDate'];
        if (!is_array($exceptDates)) {
          $exceptDates = [$exceptDates];
        }
        foreach ($exceptDates as $exceptDate) {
          $rset->addExDate($exceptDate);
        }
      }

      /** @var $occurrence \DateTime */
      foreach ($rset->getOccurrences(365) as $occurrence) {
        $startObj = $occurrence;
        $endObj = clone($occurrence);
        $startValue = $startObj->setTimezone($storageTimezone)->format('Y-m-d\TH:i:s');
        if (isset($schedule['endTime'])) {
          $endObj->setTimezone($localTimezone);
          $endObj->setTime(...explode(':', $schedule['endTime']));
          $endValue = $endObj->setTimezone($storageTimezone)->format('Y-m-d\TH:i:s');
        }
        elseif (isset($schedule['duration'])) {
          $endObj = $endObj->setTimezone($storageTimezone)->add(new \DateInterval($schedule['duration']));
          $endValue = $endObj->format('Y-m-d\TH:i:s');
        }
        else {
          $endValue = $endObj->setTimezone($localTimezone)
            ->setTime(23, 59, 00)
            ->setTimezone($storageTimezone)
            ->format('Y-m-d\TH:i:s');
        }
        /**
         * Fix incorrect "duration" value from vcloud when duration overlaps switch between summer-/wintertime
         */
        if ($endObj->setTimezone($localTimezone)->format('H:i:s') == '01:00:00') {
          if ($startObj->setTimezone($localTimezone)->format('I') != $endObj->setTimezone($localTimezone)->format('I')) {
            $endObj->sub(new \DateInterval('PT1H'));
            $endValue = $endObj->setTimezone($storageTimezone)->format('Y-m-d\TH:i:s');
          }
        }
        if ($startValue == $endValue) {
          $endValue = $endObj->setTimezone($localTimezone)
            ->setTime(23, 59, 00)
            ->setTimezone($storageTimezone)
            ->format('Y-m-d\TH:i:s');
        }
        $todayMidnight = new \DateTime('now', $storageTimezone);
        $todayMidnightValue = $todayMidnight->format('Y-m-d\T00:00:00');
        if ($endValue >= $todayMidnightValue) {
          $result[] = [
            'value' => $startValue,
            'end_value' => $endValue,
            'soldout' => 0,
            'canceled' => 0,
          ];
        }
      }
    }
    return $result;
  }

  public function multiple() {
    return TRUE;
  }
}
