<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Transforms scalar values into a single value array.
 * Array values are returned as is.
 *
 * Available configuration keys:
 * - enforce_null: also enforce a null value into a single value array
 *
 * Example:
 *
 * @code
 * source:
 *   one_value: 'foo'
 *   two_values:
 *     - 'foo'
 *     - 'bar'
 * process:
 *   surely_an_array_1:
 *     plugin: enforce_array
 *     source: one_value
 *   surely_an_array_2:
 *     plugin: enforce_array
 *     source: two_values
 *   surely_an_array_3:
 *     plugin: enforce_array
 *     source: field_possibly_returning_null_value
 *     enforce_null: true
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "enforce_array",
 *   handle_multiples = TRUE
 * )
 */
class EnforceArray extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_array($value)) {
      return $value;
    }

    if (!is_null($value) || $this->configuration['enforce_null']) {
      return [$value];
    }

    return NULL;
  }

  public function multiple() {
    return TRUE;
  }
}
