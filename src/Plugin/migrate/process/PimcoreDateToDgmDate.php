<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Transform a pimore event date structure into a DGM event daterange
 *
 * Params: array[
 *   type       => "RecurringDate"|"FixedDate",
 *   timeFrom   => "H:i",
 *   timeTo     => "H:i",
 *   monday     => null|"true",
 *   tuesday    => null|"true",
 *   wednesday  => null|"true",
 *   thursday   => null|"true",
 *   friday     => null|"true",
 *   saturday   => null|"true",
 *   sunday     => null|"true",
 *   dateFrom   => timestamp,
 *   dateTo     => timestamp,
 *  ]
 *
 * All source params are assumed to be in Timezone Europe/Vienna
 *
 * Feratel provides something like:
 * - If timeFrom is missing, 00:00:00 CET is assumed.
 * - If timeTo is missing, 23:59:00 CET is assumed.
 * - If no weekdays are given, it is assumed to be a single-cardinality daterange
 *     (optionally spanning multiple days, like one weekend from Fri, 2020-10-16 to Sun, 2020-10-18)
 * - If at least one weekday is given, it is assumed to be a multi-cardinality daterange
 *
 * @returns a single wrapper array containing an array of daterange-arrays
 *
 * @MigrateProcessPlugin(
 *   id = "pimcoredate_to_dgmdate",
 *   handle_multiples = TRUE
 * )
 */
class PimcoreDateToDgmDate extends ProcessPluginBase {

  const STARTTIME = '23:59:00';
  const ENDTIME = '23:59:00';
  const CANCELED = 0;
  const SOLDOUT = 0;
  const ENDDATE_HARDLIMIT = '+400 days';

  protected bool $isMultiple;


  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      return NULL;
    }

    if (!is_array($value)) {
      throw new MigrateException("The input value must be an array.");
    }

    $result = [];
    if (array_key_exists('dateFrom', $value) && array_key_exists('dateTo', $value)) {
      $result = $this->generateDaterangeField($value);
    } else {
      foreach ($value as $dateInfo) {
        foreach($this->generateDaterangeField($dateInfo) as $subResult) {
          $result[] = $subResult;
        }
      }
    }
    return $result;
  }

  private function generateDaterangeField($value) {
    if (empty($value['dateFrom']) || empty($value['dateTo'])) {
      throw new MigrateException("dateFrom and dateTo must not be empty.");
    }

    if (preg_match('/^\d+$/', $value['dateFrom']) != 1) {
      throw new MigrateException( sprintf("dateFrom must be a timestamp, '%s' given", $value['dateFrom']) );
    }

    if (preg_match('/^\d+$/', $value['dateTo']) != 1) {
      throw new MigrateException( sprintf("dateTo must be a timestamp, '%s' given", $value['dateTo']) );
    }

    if ($value['timeFrom']) {
      if (preg_match('/^2[0-3]|[01][0-9]:[0-5][0-9]$/', $value['timeFrom']) == 1) {
        $timeFrom = $value['timeFrom'];
      }
      else {
        throw new MigrateException( sprintf("timeFrom must be formatted H:i:s, '%s' given", $value['timeFrom']) );
      }
    }
    else {
      $value['timeFrom'] = $timeFrom = '00:00';
    }

    if ($value['timeTo']) {
      if (preg_match('/^2[0-3]|[01][0-9]:[0-5][0-9]$/', $value['timeTo']) == 1) {
        if ($value['timeTo'] == '00:00') {
          $value['timeTo'] = '23:59';
        }
        $timeTo = $value['timeTo'];
      }
      else {
        throw new MigrateException( sprintf("timeFrom must be formatted H:i:s, '%s' given", $value['timeTo']) );
      }
    }
    else {
      $value['timeTo'] = $timeTo = '23:59';
    }

    $tzVienna = new \DateTimeZone('Europe/Vienna');
    $tzUTC = new \DateTimeZone('UTC');

    $weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    $hasWeekdays = FALSE;
    foreach ($weekdays as $i) {
      if (!array_key_exists($i, $value)) {
        $value[$i] = FALSE;
      }
      $value[$i] = (bool) (($value[$i] === 'false' || $value[$i] === 'off') ? FALSE : $value[$i]);
      $hasWeekdays = $hasWeekdays || $value[$i];
    }

    $start = \DateTime::createFromFormat("U", $value['dateFrom'], $tzUTC);
    $start->setTimezone($tzVienna)->setTime(...explode(':', $timeFrom));
    $end = \DateTime::createFromFormat("U", $value['dateTo'], $tzUTC);
    $end->setTimezone($tzVienna)->setTime(...explode(':', $timeTo));

    if (($value['dateFrom'] == $value['dateTo']) || !$hasWeekdays){
      return [[
        'value' => $start->setTimezone($tzUTC)->format('Y-m-d\TH:i:s'),
        'end_value' => $end->setTimezone($tzUTC)->format('Y-m-d\TH:i:s'),
        'canceled' => self::CANCELED,
        'soldout' => self::SOLDOUT,
      ]];
    }

    //else multiple cardinality
    $interval = \DateInterval::createFromDateString('1 day');
    $period = new \DatePeriod($start, $interval, $end);

    $multiCardinalityResult = [];
    $hardLimit = new \DateTime(self::ENDDATE_HARDLIMIT);
    foreach ($period as $day) {
      if ($day > $hardLimit) {
        break;
      }
      $weekday = strtolower($day->format('l'));
      if ($value[$weekday]) {
        $startDate = $day->setTimezone($tzUTC)->format('Y-m-d\TH:i:s');
        $endDate = $day->setTimezone($tzVienna)->setTime(...explode(':', $timeTo))->setTimezone($tzUTC)->format('Y-m-d\TH:i:s');
        $multiCardinalityResult[] = [
          'value' => $startDate,
          'end_value' => $endDate,
          'canceled' => self::CANCELED,
          'soldout' => self::SOLDOUT,
        ];
      }
    }
    return $multiCardinalityResult;
  }

  public function multiple() {
    return TRUE;
  }
}
