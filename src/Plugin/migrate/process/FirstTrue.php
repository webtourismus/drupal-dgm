<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Given a set of values, the plugin will return the first value evaluated as TRUE.
 * @see https://www.php.net/manual/language.types.boolean.php#language.types.boolean.casting
 *
 * Available configuration keys:
 * - source: The input array.
 * - default_value: (optional) The value to return if all values are evaluated as FALSE.
 *   If not provided, NULL is returned if all values are evaluated as FALSE.
 *
 * Example:
 *
 * @code
 * source:
 *   constants:
 *     value1: 0
 *     value3: ''
 *     value4: '0'
 *     value5: []
 *     value6: FALSE
 *     value7: NULL
 *     value8: 'eight wins'
 *     value9: 'nine is too late'
 * process:
 *   plugin: first_true
 *   source:
 *     - constants/value1
 *     - constants/value2
 *     - constants/value4
 *     - constants/value5
 *     - constants/value6
 *     - constants/value7
 *     - constants/value8
 *     - constants/value9
 * @endcode
 *
 * Given the example above, the plugin returns 'eight wins'
 *
 * @MigrateProcessPlugin(
 *   id = "first_true"
 * )
 */
class FirstTrue extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_scalar($value)) {
      $value = [$value];
    }
    if (is_iterable($value)) {
      foreach ($value as $val) {
        if ($val) {
          return $val;
        }
      }
    }
    if (isset($this->configuration['default_value'])) {
      return $this->configuration['default_value'];
    }
    return NULL;
  }
}
