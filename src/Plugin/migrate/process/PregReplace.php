<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Wrapper for PHP's preg_replace function
 * @see https://www.php.net/manual/function.preg-replace.php
 *
 * Available configuration keys:
 * - source: Source property.
 * - pattern: string or array search pattern
 * - replacement: string or array replacement
 * - limit: int $limit parameter for PHP's preg_replace, default -1
 *
 * Example:
 *
 * @code
 * source:
 *   constants:
 *     dummy_source: 'The quick brown fox jumps over the lazy dog.'
 * process:
 *   field_kitten:
 *     plugin: preg_replace
 *     source: constants/dummy_source
 *     pattern: '/fox/'
 *     replacement: 'kitten'
 *     limit: 1
 *   field_bear:
 *     plugin: preg_replace
 *     source: constants/dummy_source
 *     pattern:
 *       - '/quick/'
 *       - '/brown/'
 *       - '/fox/'
 *     replacement:
 *       - 'slow'
 *       - 'black'
 *       - 'bear'
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "preg_replace"
 *  * )
 */
class PregReplace extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $pattern = $this->configuration['pattern'];
    $replacement = $this->configuration['replacement'];
    $limit = (isset($this->configuration['limit']) && is_int($this->configuration['limit'])) ? $this->configuration['limit'] : -1;

    if (is_null($pattern) || is_null($replacement)) {
      return $value;
    }

    return preg_replace($pattern, $replacement, $value, $limit);
  }
}
