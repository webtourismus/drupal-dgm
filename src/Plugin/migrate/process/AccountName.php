<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\user\Entity\User;

/**
 * Returns the username/accountname by numeric user id
 *
 * @code
 * process:
 *   field_accountname:
 *     plugin: accountname
 *     source: uid
 * @endcode
 *
 *
 * @MigrateProcessPlugin(
 *   id = "accountname"
 * )
 */
class AccountName extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $user = User::load($value);
    if ($user instanceof User) {
      return $user->getAccountName();
    }
    return null;
  }
}
