<?php

namespace Drupal\wt_dgm\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Wrapper for Drupal's logger
 *
 * Available configuration keys:
 * - channel ... (string) name of the logger channel, defauls to 'migrate'
 * - full_row ... (0|1) ignore the source value and log the entire row source array instead
 *
 * Example:
 *
 * @code
 * process:
 *   example_1:
 *     plugin: logger
 *     source: some_source_field
 *   example_2:
 *     plugin: logger
 *     channel: my_custom_module
 *     full_row: 1
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "logger"
 * )
 */
class Logger extends ProcessPluginBase {

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $channel = isset($this->configuration['channel']) ? $this->configuration['channel'] : 'migrate';
    $dumpFullRow = (isset($this->configuration['full_row']) && $this->configuration['full_row']) ? TRUE : FALSE;
    $valueToLog = $value;

    if ($dumpFullRow) {
      $valueToLog = $row->getSource();
    }
    \Drupal::logger($channel)->debug(nl2br(var_export($valueToLog, TRUE)));
    return $value;
  }
}
