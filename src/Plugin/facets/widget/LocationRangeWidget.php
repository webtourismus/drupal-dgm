<?php
declare(strict_types=1);

namespace Drupal\wt_dgm\Plugin\facets\widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Plugin\facets\widget\DropdownWidget;

/**
 * A widget class that provides a range widget for geo distances
 *
 * @FacetsWidget(
 *   id = "location_range",
 *   label = @Translation("Location Range Widget"),
 *   description = @Translation("A dropdown widgets that allows to filter by configurable location ranges."),
 * )
 */
class LocationRangeWidget extends DropdownWidget {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'ranges' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $config = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state, $facet);

    $form['ranges'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ranges'),
      '#rows' => 5,
      '#cols' => 60,
      '#default_value' => $config['ranges'],
      '##resizable' => 'both',
      '#description' => $this->t('One range per line. Each line must contain (number) lower bound, (number) upper bound and (string) label, delimited by pipe, e.g. "5|10|from 5 to 10" or "0|100|less than 100".')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'location';
  }

  public function build(FacetInterface $facet) {
    $build = parent::build($facet);
    $build['#attributes']['class'] = array_diff($build['#attributes']['class'], ['js-facets-dropdown-links']);
    $build['#attributes']['class'][] = 'js-facets-location-dropdown-links';
    $build['#attached']['library'] = array_diff($build['#attached']['library'], ['facets/drupal.facets.dropdown-widget']);
    $build['#attached']['library'][] = 'wt_dgm/facets.location';
    return $build;
  }


}
