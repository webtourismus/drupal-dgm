<?php
declare(strict_types=1);

namespace Drupal\wt_dgm\Plugin\facets\query_type;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\QueryType\QueryTypePluginBase;
use Drupal\facets\Result\Result;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides support for location range facets within the Search API scope.
 *
 * @FacetsQueryType(
 *   id = "location_range",
 *   label = @Translation("Location Range Query"),
 * )
 */
class LocationRangeQuery extends QueryTypePluginBase implements ContainerFactoryPluginInterface {

  const TAG_GEODIST_FILTER = 'distance_filter';
  const TAG_GEODIST_FACETQUERY_PREFIX = 'distance_facet_';

  protected DgmHelper $dgmHelper;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, DgmHelper $dgmHelper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dgmHelper = $dgmHelper;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wt_dgm.helper')
    );
  }


  /**
   * @inheritDoc
   */
  public function execute() {
    $widget = $this->facet->getWidget();
    if ($widget['type'] != 'location_range') {
      return;
    }
    if (empty($widget['config']['ranges'])) {
      return;
    }
    $ranges = array_filter(array_map('trim', explode("\n", $widget['config']['ranges'])));
    $query = $this->query;
    $options = &$query->getOptions();

    /**
     * Currently I don't know any way to create distance facets within the framework.
     * Hack/Workaround:
     * 1. By using "solr_param_" as query option prefix, we can bypass the framework and add anything we want
     *    to the query. We use this to add the create the facets.
     * 2. In the facet query exclude the spatial query to get correct counts (ex=distance_filter)
     *    @see \Drupal\wt_dgm\EventSubscriber\FacetsSubscriber
     *
     */
    $options['solr_param_facet.query'] = [];
    foreach ($ranges as $range) {
      $values = explode('|', $range);
      if (count($values) != 3) {
        continue;
      }
      $from = $values[0];
      $to = $values[1];
      $options['solr_param_facet.query'][] = '{!key=' . self::TAG_GEODIST_FACETQUERY_PREFIX . $from . '_' . $to .' frange l=' . $from . ' u=' . $to . ' ex=' . self::TAG_GEODIST_FILTER . '}geodist()';
    }

    $field_identifier = $this->facet->getFieldIdentifier();
    $options['search_api_facets'][$field_identifier] = [
      'field' => $field_identifier,
      'limit' => $this->facet->getHardLimit(),
      'operator' => $this->facet->getQueryOperator(),
      'min_count' => $this->facet->getMinCount(),
      'missing' => FALSE,
    ];

    if (!empty($this->facet->getActiveItems())) {
      $activeFacet = $this->facet->getActiveItems();
      $activeFacet = reset($activeFacet);
      $upperBound = explode('_', $activeFacet)[1];
    }
    elseif ($user = $this->dgmHelper->getFrontendUser()) {
      $upperBound = $user->get('field_mapradius')->value;
    }
    $options['search_api_location'][0]['radius'] = $upperBound ?? '99999';
  }

  public function build() {
    $query_operator = $this->facet->getQueryOperator();

    if (!empty($this->results)) {
      $ranges = [];
      $rangeConfig = $this->facet->getWidget()['config']['ranges'];
      if ($rangeConfig) {
        $rangeConfigArr = explode("\n", trim($rangeConfig));
        array_walk($rangeConfigArr, "trim");
        foreach ($rangeConfigArr as $rangeString) {
          $range = explode("|", $rangeString);
          array_walk($range, "trim");
          $ranges[$range[0] . '_' . $range[1]] = $this->t($range[2])->render();
        }
      }

      $facet_results = [];
      foreach ($this->results as $result) {
        if ($result['count'] || $query_operator == 'or') {
          $result_filter = $result['filter'];
          if ($result_filter[0] === '"') {
            $result_filter = substr($result_filter, 1);
          }
          if ($result_filter[strlen($result_filter) - 1] === '"') {
            $result_filter = substr($result_filter, 0, -1);
          }
          $upperBoundFilterValue = '0_' . $this->query->getOption('search_api_location')[0]['radius'];
          if ($result['count'] === 0 && $result_filter !== '0_99999' && $result_filter !== $upperBoundFilterValue) {
            continue;
          }
          $result = new Result($this->facet, $result_filter, $ranges[$result_filter], $result['count']);
          if ($upperBoundFilterValue == $result_filter) {
            $result->setActiveState(TRUE);
          }
          $facet_results[] = $result;
        }
      }
      $this->facet->setResults($facet_results);
    }

    return $this->facet;
  }
}
