<?php

namespace Drupal\wt_dgm\Plugin\geolocation\Location;

use Drupal\geolocation\LocationInterface;
use Drupal\geolocation\LocationBase;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fixed coordinates map center.
 *
 * @Location(
 *   id = "dgm_user",
 *   name = @Translation("DGM Frontend User"),
 *   description = @Translation("Location of the DGM frontend user of the domain."),
 * )
 */
class DgmUser extends LocationBase implements LocationInterface {

    /**
     * @var \Drupal\wt_dgm\DgmHelper
     */
    protected $dgmHelper;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, DgmHelper $dgmHelper) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);

        $this->dgmHelper = $dgmHelper;
    }

    /**Q
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('wt_dgm.helper')
        );
    }

  /**
   * {@inheritdoc}
   */
  public function getCoordinates($center_option_id, array $center_option_settings, $context = NULL) {
    $user = $this->dgmHelper->getFrontendUser();
    if (!$user || $user->get('field_geo')->isEmpty()) {
        return [];
    }

    return [
      'lat' => (float) $user->get('field_geo')->first()->lat,
      'lng' => (float) $user->get('field_geo')->first()->lng,
    ];
  }

}
