<?php

namespace Drupal\wt_dgm\Plugin\geolocation\Location;


use Drupal\geolocation\Plugin\geolocation\Location\FixedCoordinates;

/**
 * Fixed coordinates map center.
 *
 * PluginID for compatibility with v1.
 *
 * @Location(
 *   id = "exposed_value",
 *   name = @Translation("From exposed form"),
 *   description = @Translation("Use exposed form values as center."),
 * )
 */
class ExposedValue extends FixedCoordinates {

  /**
   * {@inheritdoc}
   */
  public static function getDefaultSettings() {
    return [
      'lat_parameter' => 'geo_proximity_center_exposed_lat',
      'lng_parameter' => 'geo_proximity_center_exposed_lng',
      'latitude' => '',
      'longitude' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm($option_id = NULL, array $settings = [], $context = NULL) {
    $settings = $this->getSettings($settings);

    $form = [];
    $form['lat_parameter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latitude parameter name'),
      '#default_value' => $settings['lat_parameter'],
      '#size' => 60,
      '#maxlength' => 128,
    ];
    $form['lng_parameter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Longitude parameter name'),
      '#default_value' => $settings['lng_parameter'],
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['latitude'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latitude fallback'),
      '#default_value' => $settings['latitude'],
      '#size' => 60,
      '#maxlength' => 128,
    ];
    $form['longitude'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Longitude fallback'),
      '#default_value' => $settings['longitude'],
      '#size' => 60,
      '#maxlength' => 128,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCoordinates($center_option_id, array $center_option_settings, $context = NULL) {
    $settings = $this->getSettings($center_option_settings);
    $fallback = [
      'lat' => (float) $settings['latitude'],
      'lng' => (float) $settings['longitude'],
    ];

    return $fallback;
  }
}
