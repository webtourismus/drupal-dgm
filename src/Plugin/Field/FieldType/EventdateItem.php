<?php

namespace Drupal\wt_dgm\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * Defines the 'wt_eventdate' field type.
 *
 * @FieldType(
 *   id = "wt_eventdate",
 *   label = @Translation("Event date"),
 *   default_widget = "wt_eventdate",
 *   default_formatter = "daterange_default",
 *   list_class ="\Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList"
 * )
 *
 */
class EventdateItem extends DateRangeItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['soldout'] = [
      'description' => 'Event is sold out',
      'type' => 'int',
      'size' => 'tiny',
    ];
    $schema['columns']['canceled'] = [
      'description' => 'Event is canceled',
      'type' => 'int',
      'size' => 'tiny',
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['soldout'] = DataDefinition::create('boolean')
      ->setLabel(t('Event is sold out'))
      ->setRequired(TRUE);
    $properties['canceled'] = DataDefinition::create('boolean')
      ->setLabel(t('Event is canceled'))
      ->setRequired(TRUE);
    return $properties;

  }
}
