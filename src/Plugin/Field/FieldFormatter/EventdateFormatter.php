<?php

namespace Drupal\wt_dgm\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeCustomFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Plugin implementation of the 'Custom' formatter for 'eventdate' fields.
 *
 * This formatter renders the data range as plain text, with a fully
 * configurable date format using the PHP date syntax and separator.
 *
 * @FieldFormatter(
 *   id = "wt_eventdate",
 *   label = @Translation("Event date"),
 *   field_types = {
 *     "wt_eventdate"
 *   }
 * )
 */
class EventdateFormatter extends DateRangeCustomFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'flags' => 0,
        'onlynext' => 0,
        'showcount' => 0,
      ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['flags'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Render canceled/soldout flags'),
      '#default_value' => $this->getSetting('flags'),
    );
    $form['onlynext'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Render only next date'),
      '#default_value' => $this->getSetting('onlynext'),
      '#description' => $this->t('If enabled only the next upcoming date will be rendered. If there are no upcoming events, only the last date will be rendered.'),
    );
    $form['showcount'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Show total no. of dates'),
      '#default_value' => $this->getSetting('showcount'),
      '#description' => $this->t('If enabled, the total no. of dates will be rendered next to the date.'),
    );

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // @todo Evaluate removing this method in
    // https://www.drupal.org/node/2793143 to determine if the behavior and
    // markup in the base class implementation can be used instead.
    $elements = [];
    $separator = $this->getSetting('separator');

    foreach ($items as $delta => $item) {
      if (!empty($item->start_date) && !empty($item->end_date)) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $start_date */
        $startDate = $item->start_date;
        /** @var \Drupal\Core\Datetime\DrupalDateTime $endDate */
        $endDate = $item->end_date;

        $format = $this->getSetting('date_format');
        $timezone = $this->getSetting('timezone_override') ?: $startDate->getTimezone()->getName();
        $elements[$delta] = [
          'datepart' => [
            '#markup' => $this->dateFormatter->format($startDate->getTimestamp(), 'custom', $format, $timezone != '' ? $timezone : NULL),
          ],
        ];
        $multiDay = 'single';
        if ($endDate->getTimestamp() - $startDate->getTimestamp() > 60*60*24) {
          $multiDay = 'multi';
          $elements[$delta]['datepart']['#markup'] .= $separator . $this->dateFormatter->format($endDate->getTimestamp(), 'custom', $format, $timezone != '' ? $timezone : NULL);
        }
        if ($elements[$delta]['datepart']['#markup']) {
          $elements[$delta]['datepart']['#markup'] = '<span class="eventdate__date">' . $elements[$delta]['datepart']['#markup'] . '</span>';
        }

        $starttime = $this->dateFormatter->format($startDate->getTimestamp(), 'custom', 'H:i', $timezone != '' ? $timezone : NULL);
        $endtime = $this->dateFormatter->format($endDate->getTimestamp(), 'custom', 'H:i', $timezone != '' ? $timezone : NULL);
        if ($starttime != '00:00' || $endtime != '23:59') {
          $elements[$delta]['separator'] = [
            '#markup' => '<span class="eventdate__separator">, </span>',
          ];
        }

        if ($starttime != '00:00') {
          $elements[$delta]['timepart'] = [
            '#markup' => $starttime,
          ];
        }
        else {
          $elements[$delta]['timepart'] = [
            '#markup' => '',
          ];
        }
        if ($endtime != '23:59' && $starttime != $endtime) {
          if ($starttime != '00:00') {
            $elements[$delta]['timepart']['#markup'] .= $separator . $endtime;
          }
          else {
            $elements[$delta]['timepart']['#markup'] .= $this->t('until') . ' ' . $endtime;
          }
        }
        if ($elements[$delta]['timepart']['#markup']) {
          $elements[$delta]['timepart']['#markup'] = '<span class="eventdate__time">' . $elements[$delta]['timepart']['#markup'] . '</span>';
        }

        if ($this->getSetting('flags')) {
          if ($item->soldout) {
            $elements[$delta]['soldout']['#markup'] = '<span class="eventdate__flag eventdate__flag--soldout">' . $this->t('sold out') .'</span>';
          }
          if ($item->canceled) {
            $elements[$delta]['canceled']['#markup'] = '<span class="eventdate__flag eventdate__flag--canceled">' . $this->t('canceled') .'</span>';
          }
        }

        if ($this->getSetting('showcount')) {
          $numberOfDates = count($items);
          if ($numberOfDates > 1) {
            $node = $items->getEntity();
            /* if we have got mulitple dates, we override the soldout information (of next date), as not all dates might be sold out */
            $elements[$delta]['soldout'] = Link::createFromRoute(
              $this->formatPlural($numberOfDates - 1, '+ @count date', '+ @count dates'),
              'entity.node.canonical',
              ['node' => $node->id()], ['attributes' => ['class' => 'eventdate__flag eventdate__flag--count'], 'fragment' => 'scroll=.eventdates--more']
            )->toRenderable();
          }
        }
      }
    }

    if (count($items) > 1 && $this->getSetting('onlynext')) {
      $nextDateIsVolatile = FALSE;
      // initialize with last element
      $nextDateElement = $elements[count($elements) - 1];
      foreach ($items as $delta => $item) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $endDate */
        if ($item->end_date->getTimestamp() >= strtotime('today midnight')) {
          $nextDateElement = $elements[$delta];
          $nextDateIsVolatile = TRUE;
          break;
        }
      }
      if ($nextDateElement) {
        $elements = [$nextDateElement];
        if ($nextDateIsVolatile) {
          $elements['#cache'] = [
            'tags' => [\Drupal\wt_cms\CmsHelper::CACHETAG_MIDNIGHT],
          ];
        }
      }
    }

    return $elements;
  }

}
