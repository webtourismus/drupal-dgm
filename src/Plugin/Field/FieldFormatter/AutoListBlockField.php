<?php

namespace Drupal\wt_dgm\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\block_field\Plugin\Field\FieldFormatter\BlockFieldFormatter;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\views\Element\View;
use Drupal\views\Plugin\Block\ViewsBlock;
use Drupal\wt_cms\CmsHelper;

/**
 * Plugin implementation of the 'block_field' formatter.
 *
 * @FieldFormatter(
 *   id = "autlist_blockfield",
 *   label = @Translation("Block field with filters from Auto-List block"),
 *   field_types = {
 *     "block_field"
 *   }
 * )
 */
class AutoListBlockField extends BlockFieldFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var $parentEntity \Drupal\block_content\Entity\BlockContent */
    $parentEntity = $items->getEntity();
    if ($parentEntity->getEntityTypeId() != 'block_content' || $items->getFieldDefinition()->getName() != 'field_view') {
      return parent::viewElements($items, $langcode);
    }

    $elements = [];
    foreach ($items as $delta => $item) {
      /** @var \Drupal\block_field\BlockFieldItemInterface $item */
      $block = $item->getBlock();

      if (!($block instanceof ViewsBlock)) {
        continue;
      }

      // copy & paste parent::viewElements()
      if ($block instanceof ContextAwarePluginInterface) {
        try {
          $contexts = \Drupal::service('context.repository')->getRuntimeContexts($block->getContextMapping());
          \Drupal::service('context.handler')->applyContextMapping($block, $contexts);
        }
        catch (ContextException $e) {
          continue;
        }
      }
      if (!$block || !$block->access(\Drupal::currentUser())) {
        continue;
      }

      // apply my custom filters and sort options to the views block
      $view = $block->getViewExecutable();
      $display = $view->current_display;

      $args = [$parentEntity->id()];
      $view->setArguments([$parentEntity->id()]);

      $filters = [];

      if ($parentEntity->hasField('field_contentypes')) {
        $filters['type'] = array_column($parentEntity->get('field_contentypes')->getValue(), 'value');
      }
      foreach ($parentEntity->getFields() as $fieldName => $field) {
        if ($field->getFieldDefinition()->getType() == 'entity_reference' && $field->getFieldDefinition()->getSetting('target_type') == 'taxonomy_term') {
          if (!$field->isEmpty()) {
            $filters[$fieldName . '_target_id'] = array_column($field->getValue(), 'target_id');
          }
        }
      }
      $sort = $parentEntity->get('field_sort')->value;
      $filters['sort_by'] = $sort;
      if ($sort == 'random') {
        $parentEntity->addCacheTags([CmsHelper::CACHETAG_MIDNIGHT]);
      }
      $view->setExposedInput($filters);

      $view->setItemsPerPage($parentEntity->get('field_quantity')->value);

      $view->display_handler->setOption('title', $parentEntity->get('field_title')->value);

      /**
       * Views will append the current block's filter settings to 'use_more_url'.
       * This can be harmful if linking to a different view with different
       * filter/sort options. For those cases we a custom $display_handler
       * option 'unprocessed_link_url' is added.
       */
      if ($parentEntity->hasField('field_link')) {
        $moreLink = $parentEntity->get('field_link');
        if ($moreLink->isEmpty()) {
          $view->display_handler->setOption('use_more', FALSE);
        }
        else {
          $view->display_handler->setOption('use_more', TRUE);
          $view->display_handler->setOption('use_more_text', $moreLink->title ?? $this->t('more')->render());
          $view->display_handler->setOption('link_display', 'custom_url');
          $view->display_handler->setOption('link_url', $moreLink->first()->getUrl()->toString());
          $view->display_handler->setOption('unprocessed_link_url', $moreLink->first()->getUrl()->toString());
        }
      }

      //@see \Drupal\views\Plugin\Block\ViewsBlock::build()
      $output = $view->buildRenderable($display, $args, FALSE);
      $output = View::preRenderViewElement($output);

      $elements[$delta] = $output;

      CacheableMetadata::createFromRenderArray($elements[$delta])
        ->addCacheableDependency($parentEntity)
        ->applyTo($elements[$delta]);
    }
    return $elements;
  }

}
