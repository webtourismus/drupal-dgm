<?php

namespace Drupal\wt_dgm\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Renders customizeable values from a raw XML or JSON string
 *
 * @FieldFormatter(
 *   id = "rawdata_formatter",
 *   label = @Translation("Raw data value"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class RawdataFormatter extends BasicStringFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'twigstring' => '{{ field_rawdata.somevalue }}',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['twigstring'] = array(
      '#type' => 'textarea',
      '#maxlength' => 4000,
      '#title' => $this->t('Value to render'),
      '#default_value' => $this->getSetting('twigstring'),
      '#description' => $this->t('You can access values using Twig notation. All field values are accessible in the variable <em>field_rawdata</em>.<br>Use <em>{{ dd(field_rawdata) }}</em> to get a complete list of values for debugging.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $itemValue = trim($item->value);
      $rawdata = NULL;

      if (strpos($itemValue, '<') === 0) {
        $type = 'xml';
        if (!strpos($itemValue, '<?xml') === 0) {
          $itemValue = '<?xml version="1.0" encoding="UTF-8" ?>' . $itemValue;
        }
        $rawdata = simplexml_load_string($itemValue);
      }
      elseif (strpos($itemValue, '{') === 0 || strpos($itemValue, '[') === 0) {
        $rawdata = json_decode($itemValue, FALSE, 512, JSON_INVALID_UTF8_IGNORE);
      }

      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => $this->getSetting('twigstring'),
        '#context' => ['field_rawdata' => $rawdata],
      ];
    }

    return $elements;
  }
}
