<?php

namespace Drupal\wt_dgm\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;

/**
 * Plugin implementation of the 'wt_eventdate' widget.
 *
 * @FieldWidget(
 *   id = "wt_eventdate",
 *   label = @Translation("Event date field widget"),
 *   field_types = {
 *     "wt_eventdate"
 *   }
 * )
 */
class EventdateWidget extends DateRangeDefaultWidget implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['soldout'] = [
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->soldout),
      '#title' => $this->t('sold out'),
    ];

    $element['canceled'] = [
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->canceled),
      '#title' => $this->t('canceled'),
    ];

    $element['helper'] = [
      '#type' => 'inline_template',
      '#template' => '<div class="wt_eventdate_helper">'
        . $this->t('set end date to', [], ['context' => 'Event date'])
        . ': '
        . '<button type="button" class="button button--small wt_eventdate_helper__btn wt_eventdate_helper__btn--hour">'
        . $this->t('1 hour later', [], ['context' => 'Event date'])
        . '</button><button type="button" class="button button--small wt_eventdate_helper__btn wt_eventdate_helper__btn--day">'
        . $this->t('end of day', [], ['context' => 'Event date'])
        . '</button>'
        . '</div>',
    ];

    $element['#attached']['library'][] = 'wt_dgm/eventdate';

    return $element;
  }

}
