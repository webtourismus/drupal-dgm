<?php

namespace Drupal\wt_dgm\Plugin\Menu\LocalTask;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\User;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Local task plugin to render dynamic tab title dynamically.
 */
class DgmNodeParentVariantLocalTask extends LocalTaskDefault implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  protected $dgmHelper;
  protected $currentRouteMatch;
  protected $currentUser;

  public function __construct(array $configuration, $plugin_id, array $plugin_definition, DgmHelper $dgmHelper, RouteMatchInterface $current_route_match, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dgmHelper = $dgmHelper;
    $this->currentRouteMatch = $current_route_match;
    $this->currentUser = $current_user;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wt_dgm.helper'),
      $container->get('current_route_match'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(Request $request = NULL) {
    // $this->getRouteParameters($this->currentRouteMatch) are overwrites of
    // route parameters from routing.yml inside task.yml
    $bundle = $this->getRouteParameters($this->currentRouteMatch)['bundle'] ?? $this->currentRouteMatch->getParameter('bundle');
    $variant = $this->getRouteParameters($this->currentRouteMatch)['variant'] ?? $this->currentRouteMatch->getParameter('variant') ?? '_default';
    $user = User::load($this->currentUser->id());
    return $this->dgmHelper->getDgmNode($user, $bundle, $variant)->label();

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cacheContext = parent::getCacheContexts();
    return Cache::mergeContexts($cacheContext, ['user', 'session', 'url.site']);
  }
}
