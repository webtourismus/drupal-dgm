<?php

namespace Drupal\wt_dgm\Plugin\Menu\LocalTask;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Local task plugin to render dynamic tab title dynamically.
 */
class DgmNodeVariantLocalTask extends LocalTaskDefault implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  protected $dgmHelper;
  protected $currentRouteMatch;

  public function __construct(array $configuration, $plugin_id, array $plugin_definition, DgmHelper $dgmHelper, RouteMatchInterface $current_route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dgmHelper = $dgmHelper;
    $this->currentRouteMatch = $current_route_match;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wt_dgm.helper'),
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(Request $request = NULL) {
    // $this->getRouteParameters($this->currentRouteMatch) are overwrites of
    // route parameters from routing.yml inside task.yml
    $bundle = $this->getRouteParameters($this->currentRouteMatch)['bundle'] ?? $this->currentRouteMatch->getParameter('bundle');
    $variant = $this->getRouteParameters($this->currentRouteMatch)['variant'] ?? $this->currentRouteMatch->getParameter('variant') ?? '_default';
    return $this->dgmHelper->getLocalTaskVariantTitle($bundle, $variant);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cacheContext = parent::getCacheContexts();
    return Cache::mergeContexts($cacheContext, ['user', 'session', 'url.site']);
  }

}
