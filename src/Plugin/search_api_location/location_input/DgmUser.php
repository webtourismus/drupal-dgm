<?php

namespace Drupal\wt_dgm\Plugin\search_api_location\location_input;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\search_api_location\LocationInput\LocationInputPluginBase;
use Drupal\user\Entity\User;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Represents the Raw Location Input.
 *
 * @LocationInput(
 *   id = "dgm_user",
 *   label = @Translation("DGM Frontend user"),
 *   description = @Translation("Filter by the current DGM Frontend user fields 'geo' and 'map_radius'."),
 * )
 */
class DgmUser extends LocationInputPluginBase implements ContainerFactoryPluginInterface {

  protected DgmHelper $dgmHelper;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->dgmHelper = $container->get('wt_dgm.helper');
    return $instance;
  }

  public function getForm(array $form, FormStateInterface $form_state, array $options) {
    $form['expose_button']['#access'] = FALSE;
    unset($form['operator']['#options']['between']);
    unset($form['operator']['#options']['>']);
    $form['fake_point'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Center point'),
      '#default_value' => $this->t('Latitude and longitude of "field_geo" from current DGM Frontend "user" entity.'),
      '#size' => '60',
      '#disabled' => TRUE,
      '#description' => $this->t('This filter has no input and can not be exposed.')
    ];
    $form['fake_radius'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Radius'),
      '#default_value' => $this->t('Value of "field_map_radius" from current DGM Frontend "user" entity.'),
      '#size' => '60',
      '#disabled' => TRUE,
      '#description' => $this->t('This filter has no input and can not be exposed.')
    ];
    /**
     * Contrary to documentation, a distance radius of 0 does not work, neither for sort nor for facets.
     * As a workaround set the distance limit incredibly high (note: equatorial circumference = 40075km)
     */
    $form['value']['distance']['from'] = [
      '#type' => 'hidden',
      '#value' => 99999,
    ];
    return $form;
  }

  public function hasInput(array $input, array $settings) {
    $user = $this->dgmHelper->getFrontendUser();
    if ($user instanceof User && !$user->get('field_geo')->isEmpty()) {
      return TRUE;
    }
    return FALSE;
  }

  public function getParsedInput(array $input) {
    $user = $this->dgmHelper->getFrontendUser();
    if ($user instanceof User && !$user->get('field_geo')->isEmpty()) {
      $string = $user->get('field_geo')->lat . ',' . $user->get('field_geo')->lng;
      return $string;
    }
    return NULL;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['info'] = [
      '#type' => 'inline_template',
      '#template' => "<p>This plugin has no configuration options. It collects the point and the radius from the current DGM frontend user entity.</p>",
    ];
    $form['radius_type'] = [
      '#type' => 'hidden',
      '#value' => 'dummy_database_value',
    ];
    $form['radius_units'] = [
      '#type' => 'hidden',
      '#value' => 'km',
    ];
    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // nothing to validate
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // nothing to store
  }

}
