<?php

namespace Drupal\wt_dgm\Plugin\views\filter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\filter\BooleanOperator;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */

/**
 * Filter handler for the DGM frontend user.
 *
 * inspired by "Domain Access Search API" contrib module
 * https://www.drupal.org/project/domain_access_search_api
 *
 * @ViewsFilter("dgm_season_search_api")
 *
 * @see _wt_dgm_views_data_alter__searchapi__current_dgm_user()
 */
class DgmSeasonSearchApi extends BooleanOperator implements ContainerFactoryPluginInterface {

  protected DgmHelper $dgmHelper;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->dgmHelper = $container->get('wt_dgm.helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->value_value = $this->t('Is the DGM frontend season for the domain (Search API)');
  }

  public function query() {
    $this->ensureMyTable();

    $season = NULL;
    $user = $this->dgmHelper->getFrontendUser();
    if ($user instanceof User) {
      $season = $this->dgmHelper->getFrontendSeason($user);
    }

    if ($season && !empty($this->value)) {
      $this->query->addWhere($this->options['group'], $this->realField, $season, "=");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'url.site';
    $contexts[] = 'season'; /* is this correct? because season might vary by user */
    return $contexts;
  }

  public function getCacheTags() {
    if ($this->dgmHelper->getFrontendUser() instanceof User) {
      return Cache::mergeTags(parent::getCacheTags(), $this->dgmHelper->getFrontendUser()->getCacheTags());
    }
    return parent::getCacheTags();
  }
}
