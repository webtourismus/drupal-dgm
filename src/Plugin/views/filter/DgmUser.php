<?php

namespace Drupal\wt_dgm\Plugin\views\filter;

use Drupal\Core\Database\Query\Condition;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\filter\BooleanOperator;

/**
 *
 */

/**
 * Filter handler for the DGM frontend user.
 *
 * inspired by CurrentUser filter from core user module
 *
 * @see file 'wt_dgm.views.inc'
 *
 * @ViewsFilter("dgm_user")
 */
class DgmUser extends BooleanOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->value_value = $this->t('Is the DGM frontend user for the domain');
  }

  public function query() {
    $this->ensureMyTable();

    $field = $this->tableAlias . '.' . $this->realField . ' ';
    $or = new Condition('OR');

    if (empty($this->value)) {
      $or->condition($field, '***DGM_USER***', '<>');
      if ($this->accept_null) {
        $or->isNull($field);
      }
    }
    else {
      $or->condition($field, '***DGM_USER***', '=');
    }
    $this->query->addWhere($this->options['group'], $or);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();

    // This filter depends on the current domain.
    $contexts[] = 'url.site';

    return $contexts;
  }

}
