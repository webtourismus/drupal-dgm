<?php

namespace Drupal\wt_dgm\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block showing the current time and updating it with Moment.js
 *
 * @Block(
 *   id = "wt_current_time",
 *   admin_label = @Translation("Current time"),
 * )
 */
class CurrentTimeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $blockId = Html::getUniqueId('currenttime');

    $attached = [];
    $attached['library'][] = 'wt_dgm/moment';
    $attached['drupalSettings']['wt']['current_time'][$blockId] = [
      'timeformat' => $config['timeformat'],
      'updateinterval' => $config['updateinterval']
    ];

    return [
      '#type' => 'inline_template',
      '#template' => '<div class="currenttime" id="{{ block_id }}">{{ time_string }}</div>',
      '#context' => [
        'block_id' => $blockId,
        'time_string' => '', // start empty so cacheable, will be updated by JS
      ],
      '#attached' => $attached,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['timeformat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time format'),
      '#description' => $this->t('Using Moment.js notation'),
      '#required' => TRUE,
      '#default_value' => isset($config['timeformat']) ? $config['timeformat'] : 'HH:mm | D MMM YY',
    ];

    $form['updateinterval'] = [
      '#type' => 'number',
      '#title' => $this->t('JS update interval in milliseconds'),
      '#description' => $this->t('E.g. to update the time once a minute enter "60000" (60 * 1000)'),
      '#required' => TRUE,
      '#min' => 1000,
      '#step' => 1000,
      '#default_value' => isset($config['updateinterval']) ? $config['updateinterval'] : '60000',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['timeformat'] = $values['timeformat'];
    $this->configuration['updateinterval'] = $values['updateinterval'];
  }
}
