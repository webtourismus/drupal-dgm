<?php

namespace Drupal\wt_dgm\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DGM Infopoint menu node by frontend user
 *
 * @Block(
 *   id = "wt_infopoint_menu",
 *   admin_label = @Translation("Infopoint Menu"),
 * )
 */
class InfopointMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected DgmHelper $dgmHelper;
  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, DgmHelper $dgmHelper, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dgmHelper = $dgmHelper;
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wt_dgm.helper'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var $user \Drupal\user\Entity\User */
    $user = $this->dgmHelper->getFrontendUser();
    if (!$user) {
      return [];
    }
    $cacheTags = $user->getCacheTags();
    $menuNode = $this->dgmHelper->getDgmNode($user, 'dgmmenu', '_current');
    $menuRenderArray = [];
    if ($menuNode) {
      $menuRenderArray = $this->entityTypeManager->getViewBuilder('node')->view($menuNode);
      $menuRenderArray += ['#entity' => $menuNode];
      $cacheTags = Cache::mergeTags($cacheTags, $menuNode->getCacheTags());
    }

    return [
      '#theme' => 'infopointmenu',
      '#infopointmenu' => $menuRenderArray,
      '#cache' => [
        'tags' => $cacheTags,
      ]
    ];
  }
}
