<?php

namespace Drupal\wt_dgm\Plugin\migrate_plus\data_parser;

use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\SimpleXml;


/**
 * Similar SimpleXml, but provides a special source field named "full_xml_source"
 * the entire source XML file (below "item_selector"s).
 * Useful if the source contains data just for one target entity and you want to
 * save the entire source
 *
 * @code
 * source:
 *   plugin: url
 *   data_fetcher_plugin: http
 *   data_parser_plugin: simple_xml_source
 *   item_selector: '/result/data'
 * fields:
 *   -
 *     name: full_xml_source
 *   -
 *     name: other_normal_field
 *     selector: /foo
 * @endcode
 *
 * @DataParser(
 *   id = "simple_xml_source",
 *   title = @Translation("SimpleXML with access to special field full_xml_source")
 * )
 */
class SimpleXmlSource extends SimpleXml {

  protected function fetchNextRow(): void {
    $target_element = array_shift($this->matches);

    // If we've found the desired element, populate the currentItem and
    // currentId with its data.
    if ($target_element !== FALSE && !is_null($target_element)) {
      foreach ($this->fieldSelectors() as $field_name => $xpath) {
        if ($field_name == 'full_xml_source') {
          $this->currentItem['full_xml_source'] = $target_element->asXML();
        }
        else {
          foreach ($target_element->xpath($xpath) as $value) {
            if ($value->children() && !trim((string) $value)) {
              $this->currentItem[$field_name] = $value;
            }
            else {
              $this->currentItem[$field_name][] = (string) $value;
            }
          }
        }
      }
      // Reduce single-value results to scalars.
      foreach ($this->currentItem as $field_name => $values) {
        if (is_array($values) && count($values) == 1) {
          $this->currentItem[$field_name] = reset($values);
        }
      }
    }
  }
}
