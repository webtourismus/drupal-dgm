<?php

namespace Drupal\wt_dgm\Plugin\migrate_plus\data_parser;

use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\SimpleXml;


/**
 * Similar SimpleXml, but unwraps the real Xml payload from a nested
 * and namespaced HTML-encoded string
 *
 * @DataParser(
 *   id = "feratel_xml",
 *   title = @Translation("Feratel XML")
 * )
 */
class FeratelXml extends SimpleXml {

  protected function openSourceUrl($url) {
    // Clear XML error buffer. Other Drupal code that executed during the
    // migration may have polluted the error buffer and could create false
    // positives in our error check below. We are only concerned with errors
    // that occur from attempting to load the XML string into an object here.
    libxml_clear_errors();

    $xml_data = $this->getDataFetcherPlugin()->getResponseContent($url);
    $xml_data = str_replace('<?xml version="1.0" encoding="utf-8"?>', '', $xml_data);
    $xml_data = str_replace('<string xmlns="http://tempuri.org/">', '', $xml_data);
    $xml_data = str_replace('</string>', '', $xml_data);
    $xml_data = html_entity_decode($xml_data);
    $xml_data = str_replace(' xmlns:xsd="http://www.w3.org/2001/XMLSchema"', '', $xml_data);
    $xml_data = str_replace(' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"', '', $xml_data);
    $xml_data = str_replace(' xmlns="http://interface.deskline.net/DSI/XSD"', '', $xml_data);
    $xml = simplexml_load_string(trim($xml_data));
    foreach (libxml_get_errors() as $error) {
      $error_string = self::parseLibXmlError($error);
      throw new MigrateException($error_string);
    }
    $this->registerNamespaces($xml);
    $xpath = $this->configuration['item_selector'];
    $this->matches = $xml->xpath($xpath);
    return TRUE;
  }
}
