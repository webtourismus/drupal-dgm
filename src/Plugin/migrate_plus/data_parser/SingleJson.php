<?php

namespace Drupal\wt_dgm\Plugin\migrate_plus\data_parser;

use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;


/**
 * Similar to Json data parser. But Json expects always an array of items. If the source returns a single object
 * (non-numeric list), it will not work inside the migrate YAML workflow.
 * This plugin wraps the
 * {single_result_object: 'a', 'foo': 'bar'}
 * inside an flat numneric array like
 * [0 => {single_result_object: 'a', 'foo': 'bar'}]
 *
 * This is somewhat similar to setting
 * item_selector: FALSE
 * but this would only work on root level and we could not make deep selections with the line above.
 *
 * @code
 * source:
 *   plugin: url
 *   data_fetcher_plugin: http
 *   data_parser_plugin: single_json
 *   item_selector: 'path/to/data/single_result_object'
 * @endcode
 *
 * @DataParser(
 *   id = "single_json",
 *   title = @Translation("JSON data parser which transform the selected item from an object to an array")
 * )
 */
class SingleJson extends Json {

  /**
   * Retrieves the JSON data and returns it as an array.
   *
   * @param string $url
   *   URL of a JSON feed.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   */
  protected function getSourceData(string $url): array {
    $response = $this->getDataFetcherPlugin()->getResponseContent($url);

    // Convert objects to associative arrays.
    $source_data = json_decode($response, TRUE, 512, JSON_THROW_ON_ERROR);

    // If json_decode() has returned NULL, it might be that the data isn't
    // valid utf8 - see http://php.net/manual/en/function.json-decode.php#86997.
    if (is_null($source_data)) {
      $utf8response = mb_convert_encoding($response, 'UTF-8');
      $source_data = json_decode($utf8response, TRUE, 512, JSON_THROW_ON_ERROR);
    }

    // Backwards-compatibility for depth selection.
    if (is_int($this->itemSelector)) {
      return $this->selectByDepth($source_data);
    }

    // Treat source data as a single object to import if itemSelector is
    // explicitly set to FALSE.
    if ($this->itemSelector === FALSE) {
      return [$source_data];
    }
    elseif (!array_is_list($source_data)) {
      //$source_data = [$source_data];
    }

    // Otherwise, we're using xpath-like selectors.
    $selectors = explode('/', trim((string) $this->itemSelector, '/'));
    foreach ($selectors as $selector) {
      if (is_array($source_data) && array_key_exists($selector, $source_data)) {
        $source_data = $source_data[$selector];
      }
    }
    return $source_data;
  }
}
