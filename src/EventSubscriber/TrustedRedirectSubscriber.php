<?php

namespace Drupal\wt_dgm\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\EnforcedResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class TrustedRedirectSubscriber
 *
 * DGM sometimes redirects to other (sub-) domains (depending on user and node type).
 * But D9 no longer allows external redirects, causing error after form redirects
 *
 *
 * @package Drupal\wt_dgm\EventSubscriber
 */
class TrustedRedirectSubscriber implements EventSubscriberInterface {

  /**
   * @var $requestStack RequestStack
   */
  protected RequestStack $requestStack;


  /**
   * @var $configFactory ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  public function __construct(RequestStack $request_stack, ConfigFactoryInterface $config_factory) {
    $this->requestStack = $request_stack;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.factory'),
    );
  }

  public function DgmSubdomainTrustedRedirect(ResponseEvent $event) {
    $response = $event->getResponse();
    // form redirects are wrapped inside an EnforcedResponse
    if (!($response instanceof EnforcedResponse)) {
      return;
    }
    $response = $response->getResponse();
    if (!($response instanceof RedirectResponse)) {
      return;
    }
    if ($response instanceof TrustedRedirectResponse) {
      return;
    }
    $targetDomain = parse_url($response->getTargetUrl(), PHP_URL_HOST);
    if ($targetDomain == $this->requestStack->getCurrentRequest()->getHost()) {
      return;
    }

    $config = $this->configFactory->get('wt_dgm.settings');
    $trustedDomains = array_unique(array_merge($config->get('guide_domains'), $config->get('portal_domains')));
    foreach ($trustedDomains as $trustedDomain) {
      $lengthIncludingDot = strlen($trustedDomain) + 1;
      if ($targetDomain == $trustedDomain || substr($targetDomain, -$lengthIncludingDot) == '.' . $trustedDomain) {
        $trustedResponse = new TrustedRedirectResponse($response->getTargetUrl());
        $event->setResponse($trustedResponse);
        break;
      }
    }
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['DgmSubdomainTrustedRedirect', 9000];
    return $events;
  }
}
