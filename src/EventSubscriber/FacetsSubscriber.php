<?php

namespace Drupal\wt_dgm\EventSubscriber;

use Drupal\search_api_solr\Event\PostExtractFacetsEvent;
use Drupal\search_api_solr\Event\PreQueryEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Drupal\wt_dgm\Plugin\facets\query_type\LocationRangeQuery;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FacetsSubscriber implements EventSubscriberInterface {

  /**
   * Tag the geofilt query so it can be excluded for spatial distance facets.
   */
  public function tagGeofiltQuery(PreQueryEvent $event) {
    $solrQuery = $event->getSolariumQuery();
    /** @var  $filter \Solarium\QueryType\Select\Query\FilterQuery */
    foreach ($solrQuery->getFilterQueries() as $filter) {
      if (empty($filter->getTags()) && $filter->getQuery() == '{!geofilt}') {
        $filter->addTag(LocationRangeQuery::TAG_GEODIST_FILTER);
      }
    }
  }

  public function moveFacetQueryResultToFacetField(PostExtractFacetsEvent $event) {
    $solrData = $event->getSolariumResult()->getData();
    $query = $event->getSearchApiQuery();
    if (
      empty($solrData['facet_counts']['facet_queries']) ||
      empty($query->getOption('search_api_location'))
    ) {
      return;
    }
    $facets = $event->getFacets();
    $location = $query->getOption('search_api_location');
    $location = reset($location);
    $field = $location['field'];
    $distanceFacets = [];
    foreach ($solrData['facet_counts']['facet_queries'] as $label => $count) {
      if (strpos($label, LocationRangeQuery::TAG_GEODIST_FACETQUERY_PREFIX) !== 0) {
        continue;
      }
      $distanceFacets[] = [
        'filter' => substr($label, strlen(LocationRangeQuery::TAG_GEODIST_FACETQUERY_PREFIX)),
        'count' => $count
      ];
    }
    $facets[$field] = $distanceFacets;
    $event->setFacets($facets);
  }

  static function getSubscribedEvents() {
    $events[SearchApiSolrEvents::PRE_QUERY][] = ['tagGeofiltQuery'];
    $events[SearchApiSolrEvents::POST_EXTRACT_FACETS][] = ['moveFacetQueryResultToFacetField'];
    return $events;
  }

}
