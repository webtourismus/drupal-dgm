<?php

namespace Drupal\wt_dgm\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class RemoveXFrameOptionsSubscriber
 *
 * Remove the HTTP Header Option 'X-Frame-Options' which is added
 * by Drupal to allow embedding the DGM as an I-Frame into other sites
 *
 * @package Drupal\wt_dgm\EventSubscriber
 */
class RemoveXFrameOptionsSubscriber implements EventSubscriberInterface {

  public function RemoveXFrameOptions(ResponseEvent $event) {
    $response = $event->getResponse();
    $response->headers->remove('X-Frame-Options');
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['RemoveXFrameOptions', -10];
    return $events;
  }
}
