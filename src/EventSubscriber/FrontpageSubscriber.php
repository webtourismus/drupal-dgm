<?php

namespace Drupal\wt_dgm\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FrontpageSubscriber implements EventSubscriberInterface {

  /** @var \Drupal\wt_dgm\DgmHelper */
  protected $dgmHelper;

  /** @var \Drupal\Core\Path\PathMatcherInterface */
  protected $pathMatcher;

  /** @var $stateManager \Drupal\Core\State\State */
  protected $stateManager;

  /** @var $configFactory \Drupal\Core\Config\ConfigFactoryInterface */
  protected $configFactory;

  /** @var $languageManager LanguageManagerInterface */
  protected $languageManager;

  /** @var $currentUser AccountInterface */
  protected $currentUser;

  public function __construct(DgmHelper $dgm_helper, PathMatcherInterface $path_matcher, State $state, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, AccountInterface $current_user) {
    $this->dgmHelper = $dgm_helper;
    $this->pathMatcher = $path_matcher;
    $this->stateManager = $state;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->currentUser = $current_user;
  }


  public function redirectByChannel(RequestEvent $event) {
    // Make sure front page module is not run when using cli (drush).
    // Make sure front page module does not run when installing Drupal either.
    if (PHP_SAPI === 'cli' || InstallerKernel::installationAttempted()) {
      return;
    }

    // Don't run when site is in maintenance mode.
    if ($this->stateManager->get('system.maintenance_mode')) {
      return;
    }

    // Ignore non index.php requests (like cron).
    if (!empty($_SERVER['SCRIPT_FILENAME']) && realpath(DRUPAL_ROOT . '/index.php') != realpath($_SERVER['SCRIPT_FILENAME'])) {
      return;
    }

    $redirectToChannel = NULL;
    if ($this->dgmHelper->getFrontendUser() && $this->pathMatcher->isFrontPage()) {
      $redirectToChannel = $this->dgmHelper->getChannel();
    }

    if ($redirectToChannel) {
      /*
      $url = Url::fromRoute('wt_dgm.frontend.' . $redirectToChannel,
        ['language' => $this->languageManager->getCurrentLanguage()]);
      */
      $url = Url::fromRoute('wt_dgm.frontend.' . $redirectToChannel);
      // prevent infinite redirection if channel frontpage is same as system frontpage
      if ($url && $this->configFactory->get('system.site')
          ->get('page.front') != $url->toString()) {
        $response = new LocalRedirectResponse($url->toString());
        // setting this to uncacheable is very important to prevent infinite redirects
        // module "internal page cache" must be disabled too, because it does not support max-age
        $response->getCacheableMetadata()->setCacheMaxAge(0);
        $response->addCacheableDependency($this->dgmHelper->getFrontendUser());
        $response->setMaxAge(0);
        $event->setResponse($response);
        return;
      }
    }

    if ($this->dgmHelper->isAdminDomain() && $this->pathMatcher->isFrontPage() && $this->currentUser->isAnonymous()) {
      $request = $event->getRequest();
      $route = $request->attributes->get('_route');

      if (strpos($route, 'user.') !== 0) {
        $loginUrl = Url::fromRoute('user.login')->toString();
        $response = new LocalRedirectResponse($loginUrl);
        $response->getCacheableMetadata()->setCacheMaxAge(0);
        $response->addCacheableDependency($this->dgmHelper->getFrontendUser());
        $response->setMaxAge(0);
        $event->setResponse($response);
        return;
      }
    }
  }

  public function redirectDgmAdmin(RequestEvent $event) {
    if ($this->pathMatcher->isFrontPage() && $this->dgmHelper->hasDgmDomain() && !$this->dgmHelper->isDgmAdmin()) {
      $url = Url::fromRoute('wt_dgm.admin.dashboard');
      $event->setResponse(new RedirectResponse($url->toString()));
    }
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectByChannel'];
    $events[KernelEvents::REQUEST][] = ['redirectDgmAdmin'];
    return $events;
  }

}
