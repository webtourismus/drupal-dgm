<?php

namespace Drupal\wt_dgm\EventSubscriber;

use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\pathauto\PathautoGeneratorInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\replicate\Events\ReplicateAlterEvent;
use Drupal\replicate\Events\ReplicateEntityFieldEvent;
use Drupal\replicate\Events\ReplicatorEvents;
use Drupal\replicate\Replicator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Event subscriber that handles cloning through the Replicate module.
 */
class ReplicateSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\replicate\Replicator
   */
  protected $replicator;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Drupal\pathauto\PathautoGeneratorInterface
   */
  protected $pathautoGenerator;

  /**
   * ReplicateSubscriber constructor.
   *
   * @param \Drupal\replicate\Replicator $replicator
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\pathauto\PathautoGeneratorInterface $pathauto_generator
   */
  public function __construct(Replicator $replicator, AccountInterface $account, TimeInterface $time, RouteMatchInterface $route_match, PathautoGeneratorInterface $pathauto_generator) {
    $this->replicator = $replicator;
    $this->account = $account;
    $this->time = $time;
    $this->routeMatch = $route_match;
    $this->pathautoGenerator = $pathauto_generator;
  }

  /**
   * Replicates image file entities when the parent entity is being replicated.
   *
   * @param \Drupal\replicate\Events\ReplicateEntityFieldEvent $event
   */
  public function onFieldClone(ReplicateEntityFieldEvent $event) {
    $field_item_list = $event->getFieldItemList();
    if ($field_item_list->getItemDefinition()
        ->getSetting('target_type') == 'file') {
      foreach ($field_item_list as $field_item) {
        $field_item->entity = $this->replicator->replicateEntity($field_item->entity);
      }
    }
  }

  /**
   * updates date, owner and remote settings on entity clone
   *
   * @param \Drupal\replicate\Events\ReplicateEntityFieldEvent $event
   */
  public function onEntityClone(ReplicateAlterEvent $event) {
    $entity = $event->getEntity();
    if ($entity instanceof FieldableEntityInterface) {
      //update timestamps on clone
      $timestamp = $this->time->getRequestTime();
      if ($entity->hasField('created')) {
        $entity->set('created', $timestamp);
        $this->translateField($entity, 'created', $timestamp);
      }
      if ($entity->hasField('changed')) {
        $entity->set('changed', $timestamp);
        $this->translateField($entity, 'changed', $timestamp);
      }
      //update owner on clone
      if ($entity->hasField('uid') && $entity->getEntityTypeId() != 'user') {
        if ($this->routeMatch->getRouteName() == 'wt_dgm.user.copytemplate') {
          $targetUser = $this->routeMatch->getParameter('user')->id();
          $entity->set('uid', $targetUser);
          $this->translateField($entity, 'uid', $targetUser);
        }
        else {
          $entity->set('uid', $this->account->id());
          $this->translateField($entity, 'uid', $this->account->id());
        }
      }
      //remove remote fields on clone
      if ($entity->hasField('remote_id')) {
        $entity->set('remote_id', NULL);
        $this->translateField($entity, 'remote_id', NULL);
      }
      if ($entity->hasField('remote_datasource')) {
        $entity->set('remote_datasource', NULL);
        $this->translateField($entity, 'remote_datasource', NULL);
      }
      if ($entity->hasField('remote_email')) {
        $entity->set('remote_email', NULL);
        $this->translateField($entity, 'remote_email', NULL);
      }
      //remove channel sharing on clone
      if ($entity->hasField('channels') && !$this->account->hasPermission('manage channel publishing')) {
        $entity->set('channels', NULL);
        $this->translateField($entity, 'channels', NULL);
      }
    }
  }

  public function afterClone(\Drupal\replicate\Events\AfterSaveEvent $event) {
    $entity = $event->getEntity();
    if ($entity instanceof FieldableEntityInterface && $entity->hasField('path')) {
      if ($entity instanceof TranslatableInterface && $entity->get('path')->getFieldDefinition()->isTranslatable()) {
        foreach ($entity->getTranslationLanguages() as $translation) {
          $translation = $entity->getTranslation($translation->getId());
          $this->pathautoGenerator->createEntityAlias($translation, 'insert', ['force' => true]);
        }
      }
      else {
        $this->pathautoGenerator->createEntityAlias($entity, 'insert', ['force' => true]);
      }
    }
  }
    /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ReplicatorEvents::REPLICATE_ALTER][] = 'onEntityClone';
    $events[ReplicatorEvents::replicateEntityField('image')][] = 'onFieldClone';
    $events[ReplicatorEvents::AFTER_SAVE][] = 'afterClone';
    return $events;
  }

  private function translateField(FieldableEntityInterface $entity, $fieldName, $value) {
    if ($entity instanceof TranslatableInterface && $entity->get($fieldName)->getFieldDefinition()->isTranslatable()) {
      foreach ($entity->getTranslationLanguages() as $translation) {
        $translation = $entity->getTranslation($translation->getId());
        $translation->set($fieldName, $value);
      }
    }
  }
}
