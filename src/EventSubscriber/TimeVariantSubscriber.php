<?php

namespace Drupal\wt_dgm\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TimeVariantSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\wt_dgm\DgmHelper
   */
  protected $dgmHelper;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(DgmHelper $dgm_helper, RouteMatchInterface $routeMatch, TimeInterface $time, EntityTypeManagerInterface $entityTypeManager) {
    $this->dgmHelper = $dgm_helper;
    $this->routeMatch = $routeMatch;
    $this->time = $time;
    $this->entityTypeManager = $entityTypeManager;
  }


  public function setMaxAgeByDgmUserTimeVariant(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }
    $response = $event->getResponse();

    $user = $this->dgmHelper->getFrontendUser();
    if (!$user) {
      return;
    }

    $node = $this->routeMatch->getParameter('node');
    if (!$node) {
      return;
    }
    if (is_int($node)) {
      $node = $this->entityTypeManager->getStorage('node')->load($node);
    }
    if (!($node instanceof Node)) {
      return;
    }
    if (!$node->hasField(DgmHelper::TIMECONTEXT_FIELDNAME)) {
      return;
    }
    if ($node->get(DgmHelper::TIMECONTEXT_FIELDNAME)->isEmpty()) {
      return;
    }
    $now = $this->time->getRequestTime();
    $nightStart = \DateTime::createFromFormat('H:i', $user->get('field_night_start')->value)->getTimestamp();
    $nightEnd = \DateTime::createFromFormat('H:i', $user->get('field_night_end')->value)->getTimestamp();
    $first = min($nightStart, $nightEnd);
    $last = max($nightStart, $nightEnd);

    if ($now < $first) {
      $max_age = $first  - $now;
    }
    elseif ($now < $last) {
      $max_age = $last  - $now;
    }
    else {
      $max_age = $first + 86400 - $now;
    }

    // @see https://www.lullabot.com/articles/common-max-age-pitfalls-with-drupal-cache
    $response->setMaxAge($max_age);
    $date = new \DateTime('@' . ($now + $max_age));
    $response->setExpires($date);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['setMaxAgeByDgmUserTimeVariant'];
    return $events;
  }

}
