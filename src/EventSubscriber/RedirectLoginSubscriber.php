<?php

namespace Drupal\wt_dgm\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RedirectLoginSubscriber implements EventSubscriberInterface {

  /** @var \Drupal\wt_dgm\DgmHelper */
  protected $dgmHelper;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /** @var $configFactory \Drupal\Core\Config\ConfigFactoryInterface */
  protected $configFactory;

  public function __construct(DgmHelper $dgm_helper, RouteMatchInterface $routeMatch, ConfigFactoryInterface $config_factory) {
    $this->dgmHelper = $dgm_helper;
    $this->routeMatch = $routeMatch;
    $this->configFactory = $config_factory;
  }


  public function redirectUserLogin(RequestEvent $event) {
    // Make sure this is not run when using cli (drush).
    // Make sure this does not run when installing Drupal either.
    if (PHP_SAPI === 'cli' || InstallerKernel::installationAttempted()) {
      return;
    }

    // Ignore non index.php requests (like cron).
    if (!empty($_SERVER['SCRIPT_FILENAME']) && realpath(DRUPAL_ROOT . '/index.php') != realpath($_SERVER['SCRIPT_FILENAME'])) {
      return;
    }

    if (!in_array($this->routeMatch->getRouteName(), ['user.pass', 'user.login'])) {
      return;
    }

    $host = $event->getRequest()->getHost();
    $config = $this->configFactory->get('wt_dgm.settings');
    $allowedLoginDomains = array_unique(array_merge([$config->get('default_guide_domain')], $config->get('portal_domains'), $config->get('admin_domains')));
    if (in_array($host, $allowedLoginDomains)) {
      return;
    }

    /**
     * All the user login/reset pages are unstyled on DGM user domains/themes,
     * therefore redirect to the default admin domain to get a proper login mask
     */
    $response = new TrustedRedirectResponse('https://' . $config->get('default_admin_domain') . $event->getRequest()->getBaseUrl());
    $response->getCacheableMetadata()->setCacheMaxAge(0);
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectUserLogin'];
    return $events;
  }

}
