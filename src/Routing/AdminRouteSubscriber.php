<?php

namespace Drupal\wt_dgm\Routing;

use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class AdminRouteSubscriber extends RouteSubscriberBase {

  /**
   * AccountProxyInterface definition.
   *
   * @var CurrentRouteMatch
   */
  protected $currentRoute;

  public static function create(ContainerInterface $container) {
    return new static($container->get('current_route_match'));
  }

  /**
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   CurrentRoute.
   */
  public function __construct(CurrentRouteMatch $current_route_match) {
    $this->currentRoute = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change masquerade to an admin route
    if ($route = $collection->get('masquerade.block')) {
      $route->setOption('_admin_route', TRUE);
    }
  }
}
