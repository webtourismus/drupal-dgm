<?php

namespace Drupal\wt_dgm\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Listens to the dynamic route events.
 */
class UserRouteSubscriber extends RouteSubscriberBase {

  /**
   * AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * RedirectAnonymousSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   CurrentUser.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   CurrentRoute.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config.
   * @param \Drupal\Core\Routing\RouteProvider $routeProvider
   *   Route.
   */
  public function __construct( AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Define access for '/user/{user}' and change to admin route
    if ($route = $collection->get('entity.user.canonical')) {
      $route->setRequirement('_custom_access', '\Drupal\wt_dgm\Access\UserAccessCheck::access');
      $route->setOption('_admin_route', TRUE);
    }
  }
}
