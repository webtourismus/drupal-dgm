<?php

namespace Drupal\wt_dgm\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class EntityUsageRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Modify the LocalTask of entity_usage module to use our own ListController
    if ($route = $collection->get('entity.node.entity_usage')) {
      $route->setDefault('_controller', '\Drupal\wt_dgm\Controller\EntityUsageLocalTaskController::listUsageLocalTask');
      $route->setDefault('_title_callback', '\Drupal\wt_dgm\Controller\EntityUsageLocalTaskController::getTitleLocalTask');
      $route->setRequirement('_custom_access', '\Drupal\wt_dgm\Controller\EntityUsageLocalTaskController::checkAccessLocalTask');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // run after entity_usage
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 98];
    return $events;
  }
}
