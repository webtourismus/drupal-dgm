<?php

namespace Drupal\wt_dgm\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserTemplateForm extends ContentEntityConfirmFormBase {
  /**
   * @var \Drupal\replicate\Replicator
   */
  protected $replicator;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $router;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->replicator = $container->get('replicate.replicator');
    $form->router = $container->get('current_route_match');
    return $form;
  }

  /**
   * @return \Drupal\user\Entity\User|null
   */
  protected function getUser() {
    $user = $this->router->getParameter('user');
    // some routes don't upcast their parameters
    if (!($user instanceof User)) {
      /** @var $user \Drupal\user\Entity\User */
      $user = User::load($user);
    }

    return $user;
  }

  function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('uid', $this->getUser()->id());
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();

    if (count($nodeIds) && empty($form_state->getUserInput()) && !$form_state->isRebuilding()) {
      $this->messenger()
        ->addWarning($this->t('This user already owns %count pages!', ['%count' => count($nodeIds)]));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $targetUser = $this->getUser();
    if (!$targetUser) {
      throw new NotFoundHttpException();
    }

    $sourceAccountName = $this->config('wt_dgm.settings')->get('template_accountname');
    $sourceUsers = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $sourceAccountName]);
    $sourceUser = reset($sourceUsers);

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('uid', $sourceUser->id());
    $query->condition('status', 1);
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();
    $nodes = Node::loadMultiple($nodeIds);

    $singletonNodeTypes = ['dgmtv', 'dgmmobile', 'dgminfopoint', 'dgmmenu', 'dgmform', 'a_z'];

    $replicateSummary = [];
    foreach ($nodes as $sourceNode) {
      if (in_array($sourceNode->bundle(), $singletonNodeTypes)) {
        $query = $this->entityTypeManager->getStorage('node')->getQuery();
        $query->condition('uid', $targetUser->id());
        $query->condition('type', $sourceNode->bundle());
        $query->accessCheck(FALSE);
        if ($query->count()->execute()) {
          $this->messenger()->addWarning($this->t('Skipped copying %type because target user already owns a page of this type.', ['%type' => $sourceNode->type->entity->label()]));
          continue;
        }
      }

      $replicatedNode = $this->replicator->replicateEntity($sourceNode);
      $replicateSummary[] = ['%type' => $replicatedNode->type->entity->label(), '%source_id' => $sourceNode->id(), '%target_id' => $replicatedNode->id(), '%label' => $replicatedNode->label()];
    }

    foreach ($replicateSummary as $summary) {
      $this->messenger()->addMessage($this->t('%type "%label" has been copied from ID %source_id to ID %target_id', $summary));

      // used to hide the "already owns pages" warning after an initial submit
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to copy all template pages to user "%id"?', ['%id' => $this->getUser()->getAccountName()]);
  }


  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Copy template pages');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute("entity.user.canonical", ['user' => $this->getUser()->id()]);
  }
}
