<?php

namespace Drupal\wt_dgm\Form;

use Drupal\Core\Form\FormBase;
use Drupal\wt_dgm\DgmHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\wt_cms\CacheContext\SeasonCacheContext;

/**
 * Base form to create or delete alternate variants of DGM nodes
 *
 * @package Drupal\wt_dgm\Form
 */
abstract class DgmNodeVariantBaseForm extends FormBase {
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * @var \Drupal\replicate\Replicator
   */
  protected $replicator;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $router;

  /**
   * @var \Drupal\wt_dgm\DgmHelper
   */
  protected $dgmHelper;

  protected $bundle;
  protected $variant;
  protected $fieldname;
  protected $otherVariant;
  protected $bundleLabel;
  protected $variantLabel;
  protected $otherVariantLabel;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->entityTypeManager = $container->get('entity_type.manager');
    $form->replicator = $container->get('replicate.replicator');
    $form->router = $container->get('current_route_match');
    $form->dgmHelper = $container->get('wt_dgm.helper');

    $form->bundle = $form->router->getParameter('bundle');
    $form->bundleLabel = $form->entityTypeManager->getStorage('node_type')->load($form->bundle)->label();
    $form->variant = $form->router->getParameter('variant');
    if ($form->variant == SeasonCacheContext::SUMMER_SEASON && in_array($form->bundle, DgmHelper::SEASONCONTEXT_NODES)) {
      $form->fieldname = SeasonCacheContext::FIELDNAME;
      $form->otherVariant = SeasonCacheContext::WINTER_SEASON;
      $form->variantLabel = SeasonCacheContext::getSeasons()[$form->variant];
      $form->otherVariantLabel = SeasonCacheContext::getSeasons()[$form->otherVariant];
    }
    elseif ($form->variant == DgmHelper::NIGHTTIME && in_array($form->bundle, DgmHelper::TIMECONTEXT_NODES)) {
      $form->fieldname = DgmHelper::TIMECONTEXT_FIELDNAME;
      $form->otherVariant = DgmHelper::DAYTIME;
      $form->variantLabel = DgmHelper::getTimeContexts()[$form->variant];
      $form->otherVariantLabel = DgmHelper::getTimeContexts()[$form->otherVariant];
    }
    else {
      throw new \Exception('Invalid variant "'. $form->variant . '" for bundle "'. $form->bundle . '"');
    }

    return $form;
  }
}
