<?php

namespace Drupal\wt_dgm\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\Entity\User;
use Drupal\wt_cms\CacheContext\SeasonCacheContext;
use Drupal\wt_dgm\DgmHelper;

class DgmNodeCreateVariantForm extends DgmNodeVariantBaseForm {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dgmnode_create_variant_form';
  }

  function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('Do you want to create a "%variant" variant of "%bundle"?', ['%variant' => $this->variantLabel, '%bundle' => $this->bundleLabel]);

    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('type', $this->bundle);
    $query->condition('uid', $user->id());
    $query->accessCheck(FALSE);
    $otherNodeIds = $query->execute();
    $otherNodeId = reset($otherNodeIds);
    /** @var $otherNode Node */
    $otherNode = $this->entityTypeManager->getStorage('node')->load($otherNodeId);

    $isCreateVariantAllowed = FALSE;
    if ($otherNode->get($this->fieldname)->isEmpty()) {
      $isCreateVariantAllowed = TRUE;
    }
    switch ($this->variant) {
      case SeasonCacheContext::SUMMER_SEASON:
        $start = $user->get('field_summer_start')->date->format('j. F');
        $end = $user->get('field_summer_end')->date->format('j. F');
        break;
      case DgmHelper::NIGHTTIME:
        $start = $user->get('field_night_start')->value;
        $end = $user->get('field_night_end')->value;
        break;
      default:
        $isCreateVariantAllowed = FALSE;
        break;
    }


    if ($user->getAccountName() == $this->config('wt_dgm.settings')->get('template_accountname')) {
      $this->messenger()->addWarning($this->t('The template user "%user" is special and is not allowed to have variants.', ['%user' => $user->getAccountName()]));
      $isCreateVariantAllowed = FALSE;
    }

    if ($isCreateVariantAllowed) {
      $form['info1'] = [
        '#type' => 'item',
        '#markup' => $this->t('You are creating a "%variant_new" variant of "%bundle". The existing "%bundle" will be converted into a "%variant_old" variant.', ['%variant_new' => $this->variantLabel, '%variant_old' => $this->otherVariantLabel, '%bundle' => $this->bundleLabel]),
      ];
      $userSettingsUrl = Url::fromRoute('wt_dgm.user.settings', ['user' => $user->id()])->toString();
      $form['info2'] = [
        '#type' => 'item',
        '#markup' => $this->t('The "%variant_new" variant will only be displayed from @start to @end, any other time the "%variant_old" variant will be displayed. You can edit this time in your <a href=":url">account settings</a>.', ['%variant_new' => $this->variantLabel, '%variant_old' => $this->otherVariantLabel, '@start' => $start, '@end' => $end, ':url' => $userSettingsUrl]),
      ];
      $form['info3'] = [
        '#type' => 'item',
        '#markup' => $this->t('You can delete your "%variant_new" variant at any time. When you do this, your "%variant_old" variant will become your single, main "%bundle" again and will be displayed all the time.', ['%variant_new' => $this->variantLabel, '%variant_old' => $this->otherVariantLabel, '%bundle' => $this->bundleLabel]),
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create variant'),
      '#attributes' => ['class' => ['button', 'button--primary']],
      '#submit' => [
        [$this, 'submitForm'],
      ],
    ];

    if (!$isCreateVariantAllowed) {
      $form['actions']['submit']['#attributes']['disabled']  = 'disabled';
      $this->messenger()->addError($this->t('You are not allowed to create variants.'));
    }

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => Url::fromRoute($this->getCancelRoute()),
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('uid', $this->currentUser()->id());
    $query->condition('type', $this->bundle);
    $query->notExists($this->fieldname);
    $query->accessCheck(FALSE);
    $otherNodeIds = $query->execute();
    $otherNodeId = reset($otherNodeIds);
    $otherNode = Node::load($otherNodeId);

    $otherNode->set($this->fieldname, $this->variant);
    $newlyCreatedVariantNode = $this->replicator->replicateEntity($otherNode);
    if ($newlyCreatedVariantNode->hasTranslation('en')) {
      $forceUpdateTitleInTranslatedNode = $newlyCreatedVariantNode->getTranslation('en');
      $forceUpdateTitleInTranslatedNode->save();
    }
    $otherNode->set($this->fieldname, $this->otherVariant);
    $otherNode->save();
    if ($otherNode->hasTranslation('en')) {
      $forceUpdateTitleInTranslatedNode = $otherNode->getTranslation('en');
      $forceUpdateTitleInTranslatedNode->save();
    }

    $this->messenger()->addMessage($this->t('Variant "%variant" has been created', ['%variant' => $this->variantLabel]));
    \Drupal::service('plugin.manager.menu.local_task')->clearCachedDefinitions();
    $form_state->setRedirect($this->getSuccessRoute());
  }

  protected function getCancelRoute() {
    if ($this->bundle == 'a_z') {
      $parentRoute = 'info';
    }
    else {
      $parentRoute = 'guide';
    }
    $beautifiedBundle = str_replace('dgm', '', $this->bundle);
    return "wt_dgm.admin.{$parentRoute}.{$beautifiedBundle}";
  }

  protected function getSuccessRoute() {
    $beautifiedVariant = str_replace('time', '', $this->variant);
    return "{$this->getCancelRoute()}.{$beautifiedVariant}";
  }
}
