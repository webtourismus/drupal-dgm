<?php

namespace Drupal\wt_dgm\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

class DgmNodeDeleteVariantForm extends DgmNodeVariantBaseForm {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dgmnode_delete_variant_form';
  }

  function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('Do you want to delete "%variant" variant of "%bundle"?', ['%variant' => $this->variantLabel, '%bundle' => $this->bundleLabel]);

    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('type', $this->bundle);
    $query->condition('uid', $user->id());
    $query->condition($this->fieldname, $this->variant);
    $query->accessCheck(FALSE);
    $nodeToDeleteIds = $query->execute();
    $nodeToDeleteId = reset($nodeToDeleteIds);
    /** @var $nodeToDelete Node */
    $nodeToDelete = $this->entityTypeManager->getStorage('node')->load($nodeToDeleteId);

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('type', $this->bundle);
    $query->condition('uid', $user->id());
    $query->condition($this->fieldname, $this->otherVariant);
    $query->accessCheck(FALSE);
    $otherNodeIds = $query->execute();
    $otherNodeId = reset($otherNodeIds);
    /** @var $node Node */
    $otherNode = $this->entityTypeManager->getStorage('node')->load($otherNodeId);

    $isDeleteVariantAllowed = TRUE;
    if (count($nodeToDeleteIds) != 1) {
      $this->messenger()->addWarning($this->t('Can not delete variant "%variant" of "%bundle" because user @user does not have exactly one node of this type.', ['%variant' => $this->variantLabel, '%bundle' => $this->bundleLabel, '@user' => $user->getAccountName()]));
      $isDeleteVariantAllowed = FALSE;
    }
    if (count($otherNodeIds) != 1) {
      $this->messenger()->addWarning($this->t('Can not delete variant "%variant" of "%bundle" because user @user does not own exactly one opposite variant "%variant_other".', ['%variant' => $this->variantLabel, '%bundle' => $this->bundleLabel, '@user' => $user->getAccountName(), '%variant_other' => $this->otherVariantLabel]));
      $isDeleteVariantAllowed = FALSE;
    }

    if ($isDeleteVariantAllowed) {
      $form['info1'] = [
        '#type' => 'item',
        '#markup' => $this->t('You are deleting "%variant" variant of "%bundle". The other variant "%variant_other" will become your single, main "%bundle" again and will be displayed all the time.', ['%variant' => $this->variantLabel, '%variant_other' => $this->otherVariantLabel, '%bundle' => $this->bundleLabel]),
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete variant'),
      '#attributes' => ['class' => ['button', 'button--primary']],
      '#submit' => [
        [$this, 'submitForm'],
      ],
    ];

    if (!$isDeleteVariantAllowed) {
      $form['actions']['submit']['#attributes']['disabled']  = 'disabled';
      $this->messenger()->addError($this->t('You are not allowed to delete this variant.'));
    }

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => Url::fromRoute($this->getCancelRoute()),
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('uid', $this->currentUser()->id());
    $query->condition('type', $this->bundle);
    $query->condition($this->fieldname, $this->variant);
    $query->accessCheck(FALSE);
    $nodeToDeleteIds = $query->execute();
    $nodeToDeleteId = reset($nodeToDeleteIds);
    $nodeToDelete = Node::load($nodeToDeleteId);
    $nodeToDelete->delete();

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('type', $this->bundle);
    $query->condition('uid', $this->currentUser()->id());
    $query->condition($this->fieldname, $this->otherVariant);
    $query->accessCheck(FALSE);
    $otherNodeIds = $query->execute();
    $otherNodeId = reset($otherNodeIds);
    $otherNode = Node::load($otherNodeId);
    $otherNode->set($this->fieldname, NULL);
    $otherNode->save();
    if ($otherNode->hasTranslation('en')) {
      $forceUpdateTitleInTranslatedNode = $otherNode->getTranslation('en');
      $forceUpdateTitleInTranslatedNode->save();
    }

    $this->messenger()->addMessage($this->t('Variant "%variant" has been deleted', ['%variant' => $this->variantLabel]));
    // @see https://drupal.stackexchange.com/a/273293/72678
    \Drupal::service('plugin.manager.menu.local_task')->clearCachedDefinitions();
    $form_state->setRedirect($this->getSuccessRoute());
  }

  protected function getSuccessRoute() {
    if ($this->bundle == 'a_z') {
      $parentRoute = 'info';
    }
    else {
      $parentRoute = 'guide';
    }
    $beautifiedBundle = str_replace('dgm', '', $this->bundle);
    return "wt_dgm.admin.{$parentRoute}.{$beautifiedBundle}";
  }

  protected function getCancelRoute() {
    $beautifiedVariant = str_replace('time', '', $this->variant);
    return "{$this->getSuccessRoute()}.{$beautifiedVariant}";
  }
}
