<?php

namespace Drupal\wt_dgm;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\user\Entity\User;
use Drupal\wt_cms\CacheContext\SeasonCacheContext;
use Drupal\wt_dgm\DgmVocabularyListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class DgmHelper {

  use StringTranslationTrait;

  /**
   * Only entities owning a field with this name are depending on the
   * season cache-context
   *
   * @see wt_dgm_entity_build_defaults_alter()
   */
  public const TIMECONTEXT_FIELDNAME = 'field_time_context';

  public const ALWAYS = '';
  public const NIGHTTIME = 'night';
  public const DAYTIME = 'day';

  public const SEASONCONTEXT_NODES = ['a_z', 'dgmmobile', 'dgminfopoint', 'dgmmenu'];

  public const TIMECONTEXT_NODES = ['dgmtv'];

  public static function getTimeContexts() {
    return [
      self::NIGHTTIME => t('nighttime'),
      self::DAYTIME => t('daytime'),
    ];
  }

  /**
   * @var $entityTypeManager EntityTypeManagerInterface;
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * @var $requestStack RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * @var $configFactory ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * @var $currentUser AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * @var $userCache array['domain_name' => $user]
   */
  protected array $userCache;

  /**
   * @var $channelCache array['domain_name' => 'channel']
   */
  protected array $channelCache;

  /**
   * DgmHelper constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Session\AccountInterface $current_user
   */
  public function __construct(RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, AccountInterface $current_user) {
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->userCache = [];
    $this->channelCache = [];
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('current_user'),
    );
  }

  /**
   * Returns the current DGM frontend user based on the domain of the
   * request.
   *
   * Used to sort and filter content for anonymous users.
   * This is NOT the currently logged-in user and must not be used for
   * permissions.
   *
   * @return \Drupal\user\Entity\User|null
   */
  public function getFrontendUser() {
    $host = $this->requestStack->getCurrentRequest()->getHost();
    if (!$host) {
      return NULL;
    }

    //simple caching
    if (array_key_exists($host, $this->userCache)) {
      return $this->userCache[$host];
    }
    else {
      //initialize as NULL
      $this->userCache[$host] = NULL;
    }

    $config = $this->configFactory->get('wt_dgm.settings');
    $protectedSubdomains = $config->get('protected_subdomains');
    $dgmDomains = $config->get('guide_domains');

    // any direct subdomain of a "wt_dgm.settings.guide_domains"
    // (except protected ones) is valid DGM user domain
    $subdomain = NULL;
    foreach ($dgmDomains as $dgmDomain) {
      $dgmDomain = '.' . $dgmDomain;
      if (substr_compare($host, $dgmDomain, -strlen($dgmDomain)) === 0) {
        // offset -0 to start from behind
        $subdomain = substr($host, 0, strpos($host, $dgmDomain, -0));
      }
    }

    if (!$subdomain || in_array($subdomain, $protectedSubdomains) || strpos($subdomain, '.') !== FALSE) {
      return $this->userCache[$host]; // NULL
    }

    // we use suffixes on the username to target individual channels like TV or touch
    // e.g. "johndoe-tv.domain.com" --> DGM user "johndoe" channel "TV"
    $channels = $config->get('channel_themes');
    $defaultChannel = $config->get('default_channel');
    $suffixedChannels = $channels;
    if (array_key_exists($defaultChannel, $suffixedChannels)) {
      unset($suffixedChannels[$defaultChannel]);
    }
    foreach ($suffixedChannels as $channel => $theme) {
      if (substr($subdomain, -strlen("-{$channel}")) === "-{$channel}") {
        $subdomain = substr($subdomain, 0, -strlen("-{$channel}"));
      }
    }


    $users = $this->entityTypeManager
      ->getStorage('user')
      ->loadByProperties(['name' => $subdomain, 'status' => 1]);
    /**
     * @var $user \Drupal\user\Entity\User
     */
    $user = reset($users);
    if ($user && $user->hasPermission('has guest guide subdomain')) {
      $this->userCache[$host] = $user;
    }

    return $this->userCache[$host];
  }

  /**
   * Returns the current DGM channel
   *
   * @return string|null
   */
  public function getChannel() {
    $user = $this->getFrontendUser();
    if (!$user) {
      return NULL;
    }

    //simple caching
    $host = $this->requestStack->getCurrentRequest()->getHost();
    $config = $this->configFactory->get('wt_dgm.settings');
    $channels = $config->get('channel_themes');
    if (array_key_exists($host, $this->channelCache)) {
      return $this->channelCache[$host];
    }
    else {
      //initialize with default channel
      $this->channelCache[$host] = $config->get('default_channel');
    }

    foreach ($channels as $channel => $theme) {
      if (strpos($host, $user->getAccountName() . '-' . $channel . '.') === 0) {
        $this->channelCache[$host] = $channel;
      }
    }

    return $this->channelCache[$host];
  }

  /**
   * TRUE if the current user has his own DGM account
   *
   * @return bool
   */
  public function isDgmUser() {
    return $this->currentUser->hasPermission('use guest guide');
  }

  /**
   * TRUE if the current user has his own DGM subdomain
   *
   * @return bool
   */
  public function hasDgmDomain() {
    return $this->currentUser->hasPermission('has guest guide subdomain');
  }

  /**
   * TRUE if the current user is allowed into the DGM admin interface
   *
   * @return bool
   */
  public function isAllowedDgmAdminUi() {
    return $this->currentUser->hasPermission('access guest guide admin ui');
  }

  /**
   * TRUE if the current user can create content for multiple channels
   * or for other users
   *
   * @return bool
   */
  public function isChannelPublisher() {
    return $this->currentUser->hasPermission('manage channel publishing');
  }

  /**
   * TRUE if the current user administer global DGM settings
   * usually this permission is bundled with global content edit permission
   *
   * @return bool
   */
  public function isDgmAdmin() {
    return $this->currentUser->hasPermission('administer guest guide');
  }

  public function isAdminDomain() {
    $host = $this->requestStack->getCurrentRequest()->getHost();
    if (!$host) {
      return FALSE;
    }

    $config = $this->configFactory->get('wt_dgm.settings');
    $adminDomains = $config->get('admin_domains');
    foreach ($adminDomains as $adminDomain) {
      if ($adminDomain == $host) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns the field name of the variant for DGM doubleton nodes
   *
   * @param Node|string $nodeEntityOrStringBundle
   *
   * @return string|null fieldName
   */
  public static function getVariantField($nodeOrBundle) {
    if ($nodeOrBundle instanceof Node) {
      if ($nodeOrBundle->hasField(self::TIMECONTEXT_FIELDNAME)) {
        return self::TIMECONTEXT_FIELDNAME;
      }
      if ($nodeOrBundle->hasField(SeasonCacheContext::FIELDNAME)) {
        return SeasonCacheContext::FIELDNAME;
      }
    }
    elseif (is_string($nodeOrBundle)) {
      /** @var $nodeType \Drupal\node\Entity\NodeType */
      $allFields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $nodeOrBundle);
      if (isset($allFields[self::TIMECONTEXT_FIELDNAME])) {
        return self::TIMECONTEXT_FIELDNAME;
      }
      if (isset($allFields[SeasonCacheContext::FIELDNAME])) {
        return SeasonCacheContext::FIELDNAME;
      }
    }
    return NULL;
  }

  /**
   * Returns the variant value label of DGM doubleton nodes
   *
   * @param Node|string $nodeEntityOrStringBundle
   *
   * @return string|null fieldName
   */
  public static function getVariantLabel(Node $node) {
    if (!($node instanceof Node)) {
      return NULL;
    }
    $fieldName = self::getVariantField($node);
    if (!$fieldName) {
      return NULL;
    }
    if ($node->get($fieldName)->isEmpty()) {
      return '';
    }
    $value = $node->get($fieldName)->value;
    if (!$value) {
      return '';
    }
    if ($fieldName == self::TIMECONTEXT_FIELDNAME) {
      return self::getTimeContexts()[$value];
    }
    if ($fieldName == SeasonCacheContext::FIELDNAME) {
      return SeasonCacheContext::getSeasons()[$value];
    }
  }

  /**
   * Returns the admin "View" menu/route/task title of a bundle with variants.
   * Depending on singleton/doubleton this is either "View" or "View @variant"
   *
   * @param string $bundle
   * @param string $variant '_default'|'_current'|<variant_value>
   *
   * @see getDgmNode for "_magic" $variant values
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getViewVariantTitle(string $bundle, string $variant = '_default') {
    $user = User::load($this->currentUser->id());
    $node = $this->getDgmNode($user, $bundle, $variant);
    if ($node instanceof NodeInterface) {
      $label = self::getVariantLabel($node);
    }
    if ($label) {
      return $this->t('View @variant', ['@variant' => $label]);
    }
    return $this->t('View');
  }

  /**
   * Returns the admin "Edit" menu/route/task title of a bundle with variants.
   * Depending on singleton/doubleton this is either "@type" or "@type @variant"
   *
   * @param string $bundle
   * @param string $variant '_default'|'_current'|<variant_value>
   *
   * @see getDgmNode for "_magic" $variant values
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getLocalTaskVariantTitle(string $bundle, string $variant = '_default') {
    $user = User::load($this->currentUser->id());
    $node = $this->getDgmNode($user, $bundle, $variant);
    if ($node instanceof NodeInterface) {
      $variantLabel = self::getVariantLabel($node);
      $bundleLabel = $this->entityTypeManager->getStorage('node_type')->load($bundle)->label();
      return $this->t('@type @variant', ['@type' => $bundleLabel, '@variant' => $variantLabel]);
    }
    return $this->t('Edit');
  }

  /**
   * Returns the special DGM node entity given by user and bundle.
   *
   * DGM nodes are 'a_z', 'dgmtv', 'dgmmobile', 'dgminfopoint' and 'dgmmenu'
   * - 'dgmtv' can be singleton or doubleton per user
   *   - if DgmHelper::TIMECONTEXT_FIELDNAME is not empty
   *   - @see self::TIMECONTEXT_NODES
   * - ['a_z', 'dgmmobile', 'dgminfopoint', 'dgmmenu'] can be singleton or doubleton per user
   *   - if SeasonCacheContext::FIELDNAME is not empty
   *   - @see self::SEASONCONTEXT_NODES
   *
   * @param \Drupal\Core\Session\AccountInterface $user DGM frontend user
   * @param string $bundle node bundle name
   * @param string|null $variant
   *         _current ... get the node matching the current variant
   *         _default|NULL ... get the node matching the default variant
   *         any other value ... get the node matching the given value
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDgmNode(User $user, string $bundle, string $variant = NULL) {
    if (!($user instanceof User) || !$bundle) {
      return NULL;
    }
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('type', $bundle);
    $query->condition('uid', $user->id());
    $query->condition('status', 1);
    switch ($variant) {
      case '_current':
        if (in_array($bundle, self::SEASONCONTEXT_NODES)) {
          $group = $query->orConditionGroup()
            ->condition(SeasonCacheContext::FIELDNAME, $this->getFrontendSeason($user))
            ->notExists(SeasonCacheContext::FIELDNAME);
          $query->condition($group);
        }
        if (in_array($bundle, self::TIMECONTEXT_NODES)) {
          $group = $query->orConditionGroup()
            ->condition(DgmHelper::TIMECONTEXT_FIELDNAME, $this->getFrontendDaytime($user))
            ->notExists(DgmHelper::TIMECONTEXT_FIELDNAME);
          $query->condition($group);
        }
        break;
      case '_default':
      case NULL:
        if (in_array($bundle, self::SEASONCONTEXT_NODES)) {
          $group = $query->orConditionGroup()
            // winter is the default season
            ->condition(SeasonCacheContext::FIELDNAME, SeasonCacheContext::WINTER_SEASON)
            ->notExists(SeasonCacheContext::FIELDNAME);
          $query->condition($group);
        }
        if (in_array($bundle, self::TIMECONTEXT_NODES)) {
          $group = $query->orConditionGroup()
            // day is the default daytime
            ->condition(DgmHelper::TIMECONTEXT_FIELDNAME, DgmHelper::DAYTIME)
            ->notExists(DgmHelper::TIMECONTEXT_FIELDNAME);
          $query->condition($group);
        }
        break;
      default:
        if (in_array($bundle, [...self::TIMECONTEXT_NODES, ...self::SEASONCONTEXT_NODES])) {
          $query->condition($this->getVariantField($bundle), $variant);
        }
        break;
    }
    $query->sort('nid');
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nodeIds);
    $node = reset($nodes);
    return $node;
  }

  /**
   * Returns the current season variant for a DGM user
   *
   * @return string|null winter|summer|NULL
   */
  public function getFrontendSeason(User $user) {
    if (!$user) {
      return NULL;
    }

    $season = NULL;
    $start = $user->get('field_summer_start');
    $end = $user->get('field_summer_end');
    $now = new \Datetime();
    if ($start->date->format('md') <= $now->format('md') && $end->date->format('md') >= $now->format('md')) {
      $season = SeasonCacheContext::SUMMER_SEASON;
    }
    else {
      $season = SeasonCacheContext::WINTER_SEASON;
    }
    return $season;
  }

  /**
   * Returns the current daytime variant for a DGM user
   *
   * @return string|null day|night|NULL
   */
  public function getFrontendDaytime(User $user) {
    if (!$user) {
      return NULL;
    }

    $daytime = NULL;
    $start = \DateTime::createFromFormat('H:i', $user->get('field_night_start')->value);
    $end  = \DateTime::createFromFormat('H:i', $user->get('field_night_end')->value);
    $now = new \Datetime();
    if ($start <= $end) {
      if ($start->format('Hi') <= $now->format('Hi') && $end->format('Hi') > $now->format('Hi')) {
        $daytime = self::NIGHTTIME;
      }
      else {
        $daytime = self::DAYTIME;
      }
    }
    else {
      if ($start->format('Hi') > $now->format('Hi') && $end->format('Hi') <= $now->format('Hi')) {
        $daytime = self::DAYTIME;
      }
      else {
        $daytime = self::NIGHTTIME;
      }

    }
    return $daytime;
  }

  public function getOverrideContents(EntityInterface $entity, string $storageType = NULL) {
    if ($this->isDgmUser()) {
      $userId = $this->currentUser->id();
    }
    elseif ($this->getFrontendUser()) {
      $userId = $this->getFrontendUser()->id();
    }
    else {
      $userId = NULL;
    }

    $entityProps = [
      'user_id' => $userId,
      'field_entity_type' => $entity->getEntityTypeId(),
      'field_entity_id' => $entity->id(),
    ];
    if ($storageType) {
      $entityProps['type'] = $storageType;
    }
    return $this->entityTypeManager->getStorage('storage')->loadByProperties($entityProps);
  }

  public function getVocabularyImageUri($vocabulary): ?string {
    if (is_string($vocabulary)) {
      $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($vocabulary);
    }
    if (!($vocabulary instanceof Vocabulary) || !in_array($vocabulary->id(), DgmVocabularyListBuilder::OVERRIDE_VOCABULARIES)) {
      return NULL;
    }
    $overrides = $this->getOverrideContents($vocabulary, DgmVocabularyListBuilder::STORAGE_BUNDLE);
    $override = reset($overrides);
    if ($override) {
      return $override->get('field_image')->first()->entity->getFileUri();
    }
    return "public://defaultimages/{$vocabulary->id()}.jpg";
  }
}

