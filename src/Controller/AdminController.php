<?php

namespace Drupal\wt_dgm\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\system\SystemManager;
use Drupal\wt_dgm\DgmHelper;
use Drupal\wt_cms\CacheContext\SeasonCacheContext;
use Hashids\Hashids;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminController extends ControllerBase {

  /** @var $routeMatch \Drupal\Core\Routing\CurrentRouteMatch */
  protected $routeMatch;

  /** @var $dgmHelper DgmHelper */
  protected $dgmHelper;

  /**
   * The active menu trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * System Manager Service.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('wt_dgm.helper'),
      $container->get('menu.active_trail'),
      $container->get('system.manager'),
    );
  }

  public function __construct(CurrentRouteMatch $current_route_match, DgmHelper $dgmHelper, MenuActiveTrailInterface $menu_active_trail, SystemManager $system_manager) {
    $this->routeMatch = $current_route_match;
    $this->dgmHelper = $dgmHelper;
    $this->menuActiveTrail = $menu_active_trail;
    $this->systemManager = $system_manager;
  }
  /**
   * @see \Drupal\system\Controller\SystemController::systemAdminMenuBlockPage()
   *
   * Duplicated from \Drupal\wt_cms\Controller\AdminControllber because $menu_name as route parameter
   * from routing.yml broke $this->menuActiveTrail->getActiveLink()
   *
   * @todo refactor this into a service
   */
  public function toolbarMenuBlockPage($menu_name = 'dgm') {
    $link = $this->menuActiveTrail->getActiveLink($menu_name);
    if ($link && $content = $this->systemManager->getAdminBlock($link)) {
      $output = [
        '#theme' => 'admin_block_content',
        '#content' => $content,
      ];
    }
    else {
      $output = [
        '#markup' => t('You do not have any administrative items.'),
      ];
    }
    return $output;

  }

  /**
   * Redirects to the dashboard node (acts as a startpage for DGM admin)
   */
  public function dashboard() {
    $query = $this->entityTypeManager()->getStorage('node')->getQuery();
    $query->condition('type', 'dgmdashboard');
    $query->sort('nid');
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();
    $nodes = $this->entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($nodeIds);
    $node = reset($nodes);

    if ($node && $node->hasLinkTemplate('canonical')) {
      return $this->redirect(
        $node->toUrl('canonical')->getRouteName(),
        $node->toUrl('canonical')->getRouteParameters());
    }

    throw new NotFoundHttpException();
  }

  public function getCreateNodeTypeTitle($node_type) {
    $label = NodeType::load($node_type)->label();
    return $this->t('Create @name', $label);
  }

  /**
   * Renders the form for with custom address and contact fields for DGM users
   */
  public function userDataEdit() {
    $user = $this->routeMatch->getParameter('user');
    $form = $this->entityTypeManager()
      ->getFormObject('user', 'dgmdata')
      ->setEntity($user);
    return $this->formBuilder()->getForm($form);
  }

  /**
   * Renders the form for with styling/color fields and DGM app settings fields
   */
  public function userSettingsEdit() {
    $user = $this->routeMatch->getParameter('user');
    $form = $this->entityTypeManager()
      ->getFormObject('user', 'dgmdesign')
      ->setEntity($user);
    return $this->formBuilder()->getForm($form);
  }

  /**
   * Renders the edit form for the first node of the given type
   * owned by the current user
   */
  public function editDgmNode(Request $request, string $bundle, string $variant = NULL, string $formMode = 'default') {
    $user = $this->entityTypeManager()->getStorage('user')->load($this->currentUser()->id());
    $node = $this->dgmHelper->getDgmNode($user, $bundle, $variant);

    if ($node instanceof Node) {
      $form = $this->entityTypeManager()
        ->getFormObject('node', $formMode)
        ->setEntity($node);
      $form = $this->formBuilder()->getForm($form, $node);
      /** @var  $title \Drupal\Core\StringTranslation\TranslatableMarkup */
      $title = $form['#title'];
      $bundleLabel = $this->entityTypeManager()
        ->getStorage('node_type')
        ->load($node->bundle())
        ->label();
      $form['#title'] = new TranslatableMarkup(
        'Edit @type @variant',
        $title->getArguments() + ['@variant' => $this->dgmHelper->getVariantLabel($node), '@type' => $bundleLabel]
      );

      $link = $this->getPreviewUri($request, $node, FALSE);
      if ($link && $node->bundle() != 'a_z') {
        $bundleLabel = $this->entityTypeManager()
          ->getStorage('node_type')
          ->load($node->bundle())
          ->label();
        $markup = [
          '#type' => 'inline_template',
          '#template' => '<p>{% trans %}Use this public link for your personal {{ bundle|placeholder }}{% endtrans %}: '
            .'<input type="text" value="{{- link -}}" id="copy_dgm_link_source" class="visually-hidden"><a href="{{- link -}}">{{- link -}}</a> &nbsp; '
            .'<button type="button" class="button button--small" id="copy_dgm_link_btn">{% trans %}Copy link{% endtrans %}</button></p>'
            .'<script>function copy_dgm_link() {document.querySelector("#copy_dgm_link_source").select(); document.execCommand("copy");} document.querySelector("#copy_dgm_link_btn").addEventListener("click", copy_dgm_link);</script>',
          '#context' => [
            'link' => $link,
            'link_length' => strlen($link),
            'bundle' => $bundleLabel,
          ]
        ];
        array_unshift($form, $markup);
      }

      return $form;
    }

    throw new NotFoundHttpException();
  }

  public function translateDgmNode(Request $request, string $bundle, string $variant = NULL) {
    if ($this->languageManager()->getCurrentLanguage()->getId() == 'en') {
      $targetLang = $this->languageManager()->getLanguage('de');
    }
    else {
      $targetLang = $this->languageManager()->getLanguage('en');
    }
    return $this->redirect(
      str_replace('.translate', '', $this->routeMatch->getRouteName()),
      ['bundle' => $bundle, 'variant' => $variant,],
      ['language' => $targetLang,],
    );
  }

  public function redirectToFrontendDgmNodeVariant(Request $request, string $bundle, string $variant) {
    $user = $this->entityTypeManager()->getStorage('user')->load($this->currentUser()->id());
    $node = $this->dgmHelper->getDgmNode($user, $bundle, $variant);
    $urlString = $this->getPreviewUri($request, $node, TRUE);
    if (!$urlString) {
      throw new NotFoundHttpException();
    }
    $response = new TrustedRedirectResponse($urlString);
    // setting this to uncacheable is very important to prevent infinite redirects
    // module "internal page cache" must be disabled too, because it does not support max-age
    $response->getCacheableMetadata()->setCacheMaxAge(0);
    return $response;
  }

  protected function getPreviewHost(Request $request, $channel = 'default_channel') {
    if ($this->dgmHelper->hasDgmDomain()) {
      $user = $this->currentUser();
    }
    else {
      $user = $this->dgmHelper->getFrontendUser();
    }
    if (!($user instanceof AccountInterface)) {
      return NULL;
    }

    $host = $request->getHost();
    $config = $this->config('wt_dgm.settings');
    $dgmDomains = $config->get('guide_domains');
    $subdomain = $user->getAccountName();
    foreach ($dgmDomains as $dgmDomain) {
      if (substr_compare($host, $dgmDomain, -strlen($dgmDomain)) === 0) {
        $guideDomain = $dgmDomain;
        break;
      }
    }
    if (!$guideDomain && $this->dgmHelper->hasDgmDomain() && $request->getHost() == $config->get('default_portal_domain')) {
      $guideDomain = $config->get('default_guide_domain');
    }

    if (!$guideDomain) {
      return NULL;
    }

    $default = $this->config('wt_dgm.settings')->get('default_channel');
    if ($channel != $default) {
      $subdomain .= '-' . $channel;
    }

    return $request->getScheme() . '://' . $subdomain . '.' . $guideDomain;
  }

  protected function getPreviewUri(Request $request, Node $node, $forceVariant = FALSE) {
    if (!($node instanceof Node)) {
      return NULL;
    }
    if (!in_array($node->bundle(), [
      'dgmtv',
      'dgmmobile',
      'dgminfopoint',
      'a_z',
    ])) {
      return NULL;
    }
    if (!$user = $this->dgmHelper->hasDgmDomain()) {
      return NULL;
    }

    if ($node->bundle() == 'dgmtv') {
      $plainlink = $this->getPreviewHost($request, PublicController::CHANNEL_TV);
    }
    else {
      $plainlink = $this->getPreviewHost($request, PublicController::CHANNEL_TOUCH);
    }
    if (!$plainlink) {
      return NULL;
    }
    if ($forceVariant) {
      $plainlink .= '/node/' . $node->id();
    }
    return $plainlink;
  }

  public function accessCreateDgmNodeVariant(AccountInterface $account, $bundle, $variant) {
    $query = $this->entityTypeManager()->getStorage('node')->getQuery();
    $query->condition('type', $bundle);
    if ($variant == SeasonCacheContext::SUMMER_SEASON) {
      $query->notExists(SeasonCacheContext::FIELDNAME);
    }
    elseif ($variant == DgmHelper::NIGHTTIME) {
      $query->notExists(DgmHelper::TIMECONTEXT_FIELDNAME);
    }
    else {
      return AccessResult::forbidden();
    }
    $query->condition('uid', $account->id());
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();

    return AccessResult::allowedIf($account->hasPermission('has guest guide subdomain') && count($nodeIds) == 1);
  }

  public function accessEditDgmNodeVariant(AccountInterface $account, $bundle, $variant) {
    $query = $this->entityTypeManager()->getStorage('node')->getQuery();
    $query->condition('type', $bundle);
    if ($variant == SeasonCacheContext::SUMMER_SEASON) {
      $query->condition(SeasonCacheContext::FIELDNAME, $variant);
    }
    elseif ($variant == DgmHelper::NIGHTTIME) {
      $query->condition(DgmHelper::TIMECONTEXT_FIELDNAME, $variant);
    }
    else {
      return AccessResult::forbidden();
    }
    $query->condition('uid', $account->id());
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();

    return AccessResult::allowedIf($account->hasPermission('use guest guide') && count($nodeIds) >= 1);
  }

  public function accessDeleteDgmNodeVariant(AccountInterface $account, $bundle, $variant) {
    return $this->accessEditDgmNodeVariant($account, $bundle, $variant);
  }


  public function accessViewDgmNodeVariant(AccountInterface $account, $bundle, $variant) {
    $query = $this->entityTypeManager()->getStorage('node')->getQuery();
    $query->condition('type', $bundle);
    if (in_array($variant, array_keys(SeasonCacheContext::getSeasons()))) {
      $query->condition(SeasonCacheContext::FIELDNAME, $variant);
    }
    elseif (in_array($variant, array_keys(DgmHelper::getTimeContexts()))) {
      $query->condition(DgmHelper::TIMECONTEXT_FIELDNAME, $variant);
    }
    else {
      return AccessResult::forbidden();
    }
    $query->condition('uid', $account->id());
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();

    return AccessResult::allowedIf($account->hasPermission('use guest guide') && count($nodeIds) == 1);
  }

  public function getViewVariantTitle($bundle, $variant) {
    return $this->dgmHelper->getViewVariantTitle($bundle, $variant);
  }

  /**
   * Instructions on i-frame embedding of DGM content
   */
  public function shareIframe(Request $request, string $hashid) {
    $hasher = new Hashids(PublicController::CHANNEL_IFRAME, 10);
    if ($currentUserId = $this->currentUser()->id()) {
      $user = $this->entityTypeManager()->getStorage('user')->load($currentUserId);
      if (!$user->hasPermission('has guest guide subdomain')) {
        throw new AccessDeniedHttpException('You are not allowed to use the "Embed content" feature because you do not have a digital guest guide subscription.');
      }
      $hashid = $hasher->encode($currentUserId);
    }
    else {
      $user = $this->dgmHelper->getFrontendUser();
      $channel = $this->dgmHelper->getChannel();
      if ($channel != PublicController::CHANNEL_IFRAME) {
        throw new NotFoundHttpException();
      }
      $userIds = $hasher->decode($hashid);
      $userId = reset($userIds);
      if (!$hashid || $userId != $user->id()) {
        throw new AccessDeniedHttpException();
      }
    }

    if (!$user) {
      throw new NotFoundHttpException();
    }
    $cacheTags = $user->getCacheTags();

    $userInfo = [
      'name' => $user->get('field_marketing_name')->value ?? $user->getAccountName(),
      'location' => implode(' ', [$user->get('field_postcode')->value, $user->get('field_city')->value]),
      'geo' => implode(' / ', [$user->get('field_geo')->lat, $user->get('field_geo')->lng]),
    ];

    $query = $this->entityTypeManager()->getStorage('node')->getQuery();
    $query->accessCheck(TRUE);
    $query->condition('type', 'weather');
    $query->condition('status', 1);
    $query->sort('changed', 'DESC');
    $query->range(0,1);
    $examplePageIds = $query->execute();

   $examplePageUrl = '/my-page';
    if ($examplePageId = reset($examplePageIds)) {
      $examplePageUrl = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $examplePageId])->toString();
    }


    return [
      '#theme' => 'share_iframe',
      '#hashid' => $hashid,
      '#user' => $userInfo,
      '#domain_tv' => $this->getPreviewHost($request, PublicController::CHANNEL_TV),
      '#domain_touch' => $this->getPreviewHost($request, PublicController::CHANNEL_TOUCH),
      '#domain_iframe' => $this->getPreviewHost($request, PublicController::CHANNEL_IFRAME),
      '#example_page' => $examplePageUrl,
      '#attached' => [
        'drupalSettings' => [
          'wt' => [
            'share_iframe' => [
              'domain_tv' => $this->getPreviewHost($request, PublicController::CHANNEL_TV),
              'domain_touch' => $this->getPreviewHost($request, PublicController::CHANNEL_TOUCH),
              'domain_iframe' => $this->getPreviewHost($request, PublicController::CHANNEL_IFRAME),
              'example_page' => $examplePageUrl,
            ]
          ]
        ]
      ],
      '#cache' => [
        'tags' => $cacheTags,
      ],
    ];
  }
}
