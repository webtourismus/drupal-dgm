<?php

namespace Drupal\wt_dgm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\storage\Entity\Storage;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\wt_dgm\DgmVocabularyListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Imagesets routes.
 */
class ContentOverrideController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function getTitle(EntityInterface $entity): TranslatableMarkup {
    return $this->getStorageEntityTitle($entity);
  }

  public function editVocabulary(Vocabulary $entity) {
    return $this->buildForm($entity, DgmVocabularyListBuilder::STORAGE_BUNDLE);
  }

  public function editTerm(Term $entity) {
    return $this->buildForm($entity, DgmVocabularyListBuilder::STORAGE_BUNDLE);
  }

  protected function getStorageEntityTitle(EntityInterface $entity): TranslatableMarkup {
    if ($entity->getEntityTypeId() == 'node') {
      $typeLabel = $entity->type->entity->label();
    }
    else {
      $typeLabel = $entity->getEntityType()->getLabel();
    }
    return $this->t('Override @entity_type @entity_id', [
      '@entity_type' => $typeLabel,
      '@entity_id' => $entity->label(),
    ]);
  }

  /**
   * Builds the response.
   */
  protected function buildForm(EntityInterface $entity, string $storageType):array {
    $uid = $this->currentUser()->id();
    if (empty($entity) || empty($uid)) {
      throw new NotFoundHttpException;
    }

    $entityProps = [
      'user_id' => $uid,
      'type' => $storageType,
      'field_entity_type' => $entity->getEntityTypeId(),
      'field_entity_id' => $entity->id(),
    ];

    $overrides = $this->entityTypeManager->getStorage('storage')->loadByProperties($entityProps);
    $override = reset($overrides);
    if (empty($override)) {
      $override = Storage::create($entityProps);
    }

    $override->set('name', $this->getStorageEntityTitle($entity)->render());

    $form = $this->entityTypeManager()
      ->getFormObject('storage', 'default')
      ->setEntity($override);
    return $this->formBuilder()->getForm($form);
  }
}
