<?php /** @noinspection DuplicatedCode */

namespace Drupal\wt_dgm\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PublicController extends ControllerBase {

  /**
   * @var $dgmHelper \Drupal\wt_dgm\DgmHelper
   */
  protected $dgmHelper;

  public const CHANNEL_TV = 'tv';

  public const CHANNEL_TOUCH = 'touch';

  public const CHANNEL_IFRAME = 'iframe';

  public static function create(ContainerInterface $container) {
    return new static($container->get('wt_dgm.helper'));
  }

  public function __construct($dgm_helper) {
    $this->dgmHelper = $dgm_helper;
  }

  /**
   * Redirects to the first, active form node using the given
   * webform and owned by the current DGM frontend user
   *
   * @param string $webformId
   *     The webform ID, defaults to 'dgmcontact'
   *
   */
  public function redirectToFormNode($webformId = 'dgmcontact') {
    $user = $this->dgmHelper->getFrontendUser();

    if (!$user || !$webformId) {
      throw new NotFoundHttpException();
    }

    $query = $this->entityTypeManager()->getStorage('node')->getQuery();
    $query->condition('type', 'dgmform');
    $query->condition('field_form.target_id', $webformId);
    $query->condition('status', 1);
    $query->condition('uid', $user->id());
    $query->sort('nid');
    $query->accessCheck(FALSE);
    $nodeIds = $query->execute();
    $nodeId = reset($nodeIds);
    $node = $this->entityTypeManager()->getStorage('node')->load($nodeId);

    if ($node && $node->hasLinkTemplate('canonical')) {
      return $this->redirect(
        $node->toUrl('canonical')->getRouteName(),
        $node->toUrl('canonical')->getRouteParameters());
    }

    throw new NotFoundHttpException();
  }

  /**
   * Redirects to the first, active node of the given type
   * owned by the DGM frontend user
   *
   * @param string $bundle
   *     The bundle machine name
   */
  public function redirectToNodeType($bundle, Request $request) {
    $user = $this->dgmHelper->getFrontendUser();

    if (!$user || !$bundle) {
      throw new NotFoundHttpException();
    }

    $node = $this->dgmHelper->getDgmNode($user, $bundle, '_current');

    $channel = $this->dgmHelper->getChannel();
    if ($node && $node->bundle() == 'dgmtv' && $channel != self::CHANNEL_TV) {
      return $this->redirectChannelFrontpageToCorrectSubdomain($request, $user->getAccountName(), self::CHANNEL_TV);
    }

    if ($node && $node->hasLinkTemplate('canonical')) {
      return $this->redirect(
        $node->toUrl('canonical')->getRouteName(),
        $node->toUrl('canonical')->getRouteParameters());
    }

    throw new NotFoundHttpException();
  }

  /**
   * The interactive touch frontpage, consisting of a "dmgmobile" node and
   * a "dgminfopoint" node
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function touchFront(Request $request) {
    $user = $this->dgmHelper->getFrontendUser();

    if (!$user) {
      throw new NotFoundHttpException();
    }
    $cacheTags = $user->getCacheTags();
    $cacheContexts = ['season'];

    $channel = $this->dgmHelper->getChannel();
    if ($channel != self::CHANNEL_TOUCH) {
      return $this->redirectChannelFrontpageToCorrectSubdomain($request, $user->getAccountName(), self::CHANNEL_TOUCH);
    }

    $mobileRenderArray = [];
    $mobileNode = $this->dgmHelper->getDgmNode($user, 'dgmmobile', '_current');
    if ($mobileNode) {
      $mobileRenderArray = $this->entityTypeManager()
        ->getViewBuilder('node')
        ->view($mobileNode);
      $mobileRenderArray += ['#entity' => $mobileNode];
      $cacheTags = Cache::mergeTags($cacheTags, $mobileNode->getCacheTags());
      $cacheContexts = Cache::mergeContexts($cacheContexts, $mobileNode->getCacheContexts());
    }

    $infopointRenderArray = [];
    $infopointNode = $this->dgmHelper->getDgmNode($user, 'dgminfopoint', '_current');
    if ($infopointNode) {
      $infopointRenderArray = $this->entityTypeManager()
        ->getViewBuilder('node')
        ->view($infopointNode);
      $infopointRenderArray += ['#entity' => $infopointNode];
      $cacheTags = Cache::mergeTags($cacheTags, $infopointNode->getCacheTags());
      $cacheContexts = Cache::mergeContexts($cacheContexts, $infopointNode->getCacheContexts());
    }

    return [
      '#theme' => 'touchfront',
      '#mobile' => $mobileRenderArray,
      '#infopoint' => $infopointRenderArray,
      '#cache' => [
        'tags' => $cacheTags,
        'contexts' => $cacheContexts
      ],
    ];
  }

  function iframeFront() {
    throw new NotFoundHttpException();
  }

  /**
   * Controller used to redirect the legacy paths "/mobile" and "/infopoint"
   * to the new, unified touch frontpage.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function redirectToTouchFront() {
    return $this->redirect('wt_dgm.frontend.touch');
  }

  private function redirectChannelFrontpageToCorrectSubdomain(Request $request, $username, $targetChannel) {
    $host = $request->getHost();
    $config = $this->config('wt_dgm.settings');
    $dgmDomains = $config->get('guide_domains');
    $subdomain = $username;
    foreach ($dgmDomains as $dgmDomain) {
      if (substr_compare($host, $dgmDomain, -strlen($dgmDomain)) === 0) {
        if ($targetChannel != $config->get('default_channel')) {
          $subdomain .= '-' . $targetChannel;
        }
        $response = new TrustedRedirectResponse($request->getScheme() . '://' . $subdomain . '.' . $dgmDomain . '/' . $targetChannel);
        // setting this to uncacheable is very important to prevent infinite redirects
        // module "internal page cache" must be disabled too, because it does not support max-age
        $response->getCacheableMetadata()->setCacheMaxAge(0);
        return $response;

      }
    }

    throw new NotFoundHttpException();
  }
}
