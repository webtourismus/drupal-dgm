<?php

namespace Drupal\wt_dgm\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\entity_usage\Controller\ListUsageController;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for our pages.
 */
class EntityUsageListController extends ListUsageController {

  /**
   * The DGM helper service.
   *
   * @var \Drupal\wt_dgm\DgmHelper
   */
  protected $dgmHelper;


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dgmHelper = $container->get('wt_dgm.helper');
    return $instance;
  }


  /**
   * @inheritDoc
   */
  public function listUsagePage($entity_type, $entity_id) {
    $all_rows = $this->getRows($entity_type, $entity_id);
    if (empty($all_rows)) {
      return [
        '#markup' => $this->t('There are no recorded usages for entity of type: @type with id: @id', ['@type' => $entity_type, '@id' => $entity_id]),
      ];
    }

    $header = [
      $this->t('Entity'),
      $this->t('Owner'),
      $this->t('Type'),
      $this->t('Language'),
      $this->t('Field name'),
      $this->t('Status'),
      $this->t('Used in'),
    ];

    $total = count($all_rows);
    $pager = $this->pagerManager->createPager($total, $this->itemsPerPage);
    $page = $pager->getCurrentPage();
    $page_rows = $this->getPageRows($page, $this->itemsPerPage, $entity_type, $entity_id);
    // If all rows on this page are of entities that have usage on their default
    // revision, we don't need the "Used in" column.
    $used_in_previous_revisions = FALSE;
    foreach ($page_rows as $row) {
      if ($row[6] == $this->t('Translations or previous revisions')) {
        $used_in_previous_revisions = TRUE;
        break;
      }
    }
    if (!$used_in_previous_revisions) {
      unset($header[6]);
      array_walk($page_rows, function (&$row, $key) {
        unset($row[6]);
      });
    }
    $build[] = [
      '#theme' => 'table',
      '#rows' => $page_rows,
      '#header' => $header,
    ];

    $build[] = [
      '#type' => 'pager',
      '#route_name' => '<current>',
    ];

    return $build;
  }

  /**
   * @inheritDoc
   */
  protected function getPageRows($page, $num_per_page, $entity_type, $entity_id) {
    $offset = $page * $num_per_page;
    return array_slice($this->getRows($entity_type, $entity_id), $offset, $num_per_page);
  }

  /**
   * @inheritDoc
   */
  public function getRows($entity_type, $entity_id) {
    if (!empty($this->allRows)) {
      return $this->allRows;
      // @todo Cache this based on the target entity, invalidating the cached
      // results every time records are added/removed to the same target entity.
    }
    $rows = [];
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    if (!$entity) {
      return $rows;
    }
    $entity_types = $this->entityTypeManager->getDefinitions();
    $languages = $this->languageManager()->getLanguages(LanguageInterface::STATE_ALL);
    $all_usages = $this->entityUsage->listSources($entity);
    foreach ($all_usages as $source_type => $ids) {
      $type_storage = $this->entityTypeManager->getStorage($source_type);
      foreach ($ids as $source_id => $records) {
        // We will show a single row per source entity. If the target is not
        // referenced on its default revision on the default language, we will
        // just show indicate that in a specific column.
        $source_entity = $type_storage->load($source_id);
        if (!$source_entity) {
          // If for some reason this record is broken, just skip it.
          continue;
        }
        $field_definitions = $this->entityFieldManager->getFieldDefinitions($source_type, $source_entity->bundle());
        if ($source_entity instanceof RevisionableInterface) {
          $default_revision_id = $source_entity->getRevisionId();
          $default_langcode = $source_entity->language()->getId();
          $used_in_default = FALSE;
          $default_key = 0;
          foreach ($records as $key => $record) {
            if ($record['source_vid'] == $default_revision_id && $record['source_langcode'] == $default_langcode) {
              $default_key = $key;
              $used_in_default = TRUE;
              break;
            }
          }
          $used_in_text = $used_in_default ? $this->t('Default') : $this->t('Translations or previous revisions');
        }
        $owner = NULL;
        if ($source_entity instanceof EntityOwnerInterface) {
          $owner = $source_entity->getOwner();
          $ownerLabel = $owner->getAccountName();
        }
        $link = $this->getSourceEntityLink($source_entity);
        // If the label is empty it means this usage shouldn't be shown
        // on the UI, just skip this row.
        if (empty($link)) {
          continue;
        }
        if (strpos((string) $link->getText(), $this->t('previous revision')->render()) > 0 ) {
          // we are not interested in old paragraphs in our entity usage statistics
          continue;
        }
        if ($owner && $owner->hasPermission('use guest guide')) {
          $href = parse_url($link->getUrl()->toString());
          $channelSuffix = '';
          $host = $source_entity;
          $recursionSafeguard = 0;
          while ($host && $host->getEntityTypeId() == 'paragraph' && $recursionSafeguard <= 3) {
            $host = $host->getParentEntity();
            $recursionSafeguard++;
          }
          if ($host->bundle() == 'dgmtv') {
            $channelSuffix = '-tv';
          }
          if ($host->bundle() == 'dgmmenu') {
            $href['path'] = '/';
          }
          $scheme = isset($href['scheme']) ? $href['scheme'] . ':' : '';
          $ownerHref = "{$scheme}//{$owner->getAccountName()}{$channelSuffix}.{$this->config('wt_dgm.settings')->get('default_guide_domain')}{$href['path']}";
          $url = Url::fromUri($ownerHref, ['attributes' => ['target' => '_blank']]);
          $link->setUrl($url);
        }
        $published = $this->getSourceEntityStatus($source_entity);
        $field_label = isset($field_definitions[$records[$default_key]['field_name']]) ? $field_definitions[$records[$default_key]['field_name']]->getLabel() : $this->t('Unknown');
        $rows[] = [
          $link,
          $ownerLabel,
          $entity_types[$source_type]->getLabel(),
          $languages[$default_langcode]->getName(),
          $field_label,
          $published,
          $used_in_text,
        ];
      }
    }

    $this->allRows = $rows;
    return $this->allRows;
  }

  /**
   * Retrieve a link to the source entity.
   *
   * Note that some entities are special-cased, since they don't have canonical
   * template and aren't expected to be re-usable. For example, if the entity
   * passed in is a paragraph or a block content, the link we produce will point
   * to this entity's parent (host) entity instead.
   *
   * @param \Drupal\Core\Entity\EntityInterface $source_entity
   *   The source entity.
   * @param string|null $text
   *   (optional) The link text for the anchor tag as a translated string.
   *   If NULL, it will use the entity's label. Defaults to NULL.
   *
   * @return \Drupal\Core\Link|string|false
   *   A link to the entity, or its non-linked label, in case it was impossible
   *   to correctly build a link. Will return FALSE if this item should not be
   *   shown on the UI (for example when dealing with an orphan paragraph).
   */
  protected function getSourceEntityLink(EntityInterface $source_entity, $text = NULL) {
    // Note that $paragraph_entity->label() will return a string of type:
    // "{parent label} > {parent field}", which is actually OK for us.
    $entity_label = $source_entity->access('view label') ? $source_entity->label() : $this->t('- Restricted access -');

    $rel = NULL;
    if ($source_entity->hasLinkTemplate('revision')) {
      $rel = 'revision';
    }
    elseif ($source_entity->hasLinkTemplate('canonical')) {
      $rel = 'canonical';
    }

    // Block content likely used in Layout Builder inline blocks.
    if ($source_entity instanceof BlockContentInterface && !$source_entity->isReusable()) {
      $rel = NULL;
    }

    $link_text = $text ?: $entity_label;
    if ($rel) {
      // Prevent 404s by exposing the text unlinked if the user has no access
      // to view the entity.
      return $source_entity->access('view') ? $source_entity->toLink($link_text, $rel) : $link_text;
    }

    // Treat paragraph entities in a special manner. Normal paragraph entities
    // only exist in the context of their host (parent) entity. For this reason
    // we will use the link to the parent's entity label instead.
    /** @var \Drupal\paragraphs\ParagraphInterface $source_entity */
    if ($source_entity->getEntityTypeId() == 'paragraph') {
      $parent = $source_entity->getParentEntity();
      if ($parent) {
        return $this->getSourceEntityLink($parent, $link_text);
      }
    }
    // Treat block_content entities in a special manner. Block content
    // relationships are stored as serialized data on the host entity. This
    // makes it difficult to query parent data. Instead we look up relationship
    // data which may exist in entity_usage tables. This requires site builders
    // to set up entity usage on host-entity-type -> block_content manually.
    // @todo this could be made more generic to support other entity types with
    // difficult to handle parent -> child relationships.
    elseif ($source_entity->getEntityTypeId() === 'block_content') {
      $sources = $this->entityUsage->listSources($source_entity, FALSE);
      $source  = reset($sources);
      if ($source !== FALSE) {
        $parent = $this->entityTypeManager()->getStorage($source['source_type'])->load($source['source_id']);
        if ($parent) {
          return $this->getSourceEntityLink($parent);
        }
      }
    }

    // As a fallback just return a non-linked label.
    return $link_text;
  }

}
