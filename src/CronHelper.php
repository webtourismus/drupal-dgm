<?php

namespace Drupal\wt_dgm;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Drupal\migrate_tools\MigrateExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CronHelper {

  use StringTranslationTrait;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected TimeInterface $time;

  protected DateFormatterInterface $dateFormatter;

  protected MigrationPluginManager $migrationPluginManager;

  protected ConfigFactoryInterface $configFactory;

  protected KeyValueFactoryInterface $keyValue;

  protected StateInterface $state;

  protected MailManagerInterface $mailManager;

  protected LanguageManagerInterface $languageManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, TimeInterface $time, DateFormatterInterface $dateFormatter, MigrationPluginManager $migrationPluginManager, ConfigFactoryInterface $configFactory, KeyValueFactoryInterface $keyValue, StateInterface $state, MailManagerInterface $mailManager, LanguageManagerInterface $languageManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->time = $time;
    $this->dateFormatter = $dateFormatter;
    $this->migrationPluginManager = $migrationPluginManager;
    $this->configFactory = $configFactory;
    $this->keyValue = $keyValue;
    $this->state = $state;
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
  }

  public function create(ContainerInterface $container) {
    return new static (
      $container->get('entity_type.manager'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('plugin.manager.migration'),
      $container->get('config.factory'),
      $container->get('keyvalue'),
      $container->get('state'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
    );
  }

  /**
   * This helper runs all migrations of a given group once per day (default at midnight).
   * On unsuccessful migrations it will try to reset itself and re-run itself more rapidly.
   * It will send one warning message daily if the last successful import is older than 28 hours.
   *
   * @param string $group ... mandatory migration group ID
   * @param string $nextRun ... time for next run, e.g. "tomorrow 09:00"
   * @param bool $force ... force run
   * @param string|null $cronScheduleId ... override cron state variable,
   *   defaults to "$group", change when running same cron multiple times a day
   *
   * @deprecated use "runMigrationGroup" in combination with ultimate_cron module instead
   */
  public function runMigrationGroupDaily(string $group, string $nextRun = 'tomorrow 00:01', bool $force = FALSE, string $cronScheduleId = NULL) {
    $requestTime = $this->time->getRequestTime();
    if (!is_string($cronScheduleId) || empty($cronScheduleId)) {
      $cronScheduleId = $group;
    }
    $nextExecution = $this->state->get("{$cronScheduleId}.next_cron", 0);
    $environment = Settings::get('wt.environment');


    // check for fully succeessful import on entire migration group
    if (($environment !== 'dev' && $requestTime >= $nextExecution) || $force ) {
      $query = $this->entityTypeManager->getStorage('migration')->getQuery();
      $query->condition('migration_group', $group)
        ->condition('status', TRUE)
        ->sort('id');
      $query->accessCheck(FALSE);
      $migrationIds = $query->execute();

      $nextRun = strtotime($nextRun);

      foreach ($migrationIds as $migrationId) {
        $lastSuccessfulImport = $this->keyValue->get('migrate_last_imported')->get($migrationId, 0) / 1000;

        if ($lastSuccessfulImport + 86400 > $requestTime) {
          // check for partially succeessful import in individual IDs
          continue;
        }

        $migration = $this->migrationPluginManager->createInstance($migrationId);

        if ($requestTime - $lastSuccessfulImport > (86400 * 4)) {
          $today = date('Y-m-d', $requestTime);
          if ($this->state->get("{$cronScheduleId}.last_warning_sent", '1970-01-01') != $today) {
            $mailParams = [
              'from' => $this->configFactory->get('system.site')->get('mail'),
              'context' => [
                'subject' => $this->t('Warning: Migration @migration_id failed', [
                  '@migration_id' => $migrationId,
                ]),
                'message' => $this->t("The import @group / @migration_id on @site_name failed for more than 4 days. \r\nThe last successful import was on @date. \r\n@adminlink", [
                  '@site_name' => $this->configFactory->get('system.site')->get('name'),
                  '@date' => $this->dateFormatter->format($lastSuccessfulImport ,'custom', 'Y-m-d H:i:s'),
                  '@group' => $cronScheduleId,
                  '@migration_id' => $migrationId,
                  '@adminlink' => "https://{$this->configFactory->get('wt_dgm.settings')->get('default_portal_domain')}/admin/structure/migrate/manage/{$group}/migrations",
                ]),
              ],
            ];
            $this->mailManager->mail('system', 'mail', 'entwicklung@webtourismus.at', $this->languageManager->getDefaultLanguage()
                ->getId(), $mailParams, FALSE);
            $this->state->set("{$cronScheduleId}.last_warning_sent", $today);
          }
        }

        if ($migration->getStatus() != MigrationInterface::STATUS_IDLE) {
          $lastCron = $this->state->get("{$cronScheduleId}.last_cron", 0);
          // after 2 hours non-idle there definitely is something wrong.
          // as a simple repair attempt, reset and re-run it
          if (($requestTime - $lastCron) > 3600 * 2) {
            $migration->setStatus(MigrationInterface::STATUS_IDLE);
          }
        }

        if ($migration->getStatus() == MigrationInterface::STATUS_IDLE) {
          $this->state->set("{$cronScheduleId}.last_cron", $requestTime);
          $executable = new MigrateExecutable($migration, new MigrateMessage());
          $result = $executable->import();

          if ($result !== MigrationInterface::RESULT_COMPLETED) {
            // something went wrong, don't wait a full day
            $nextRun = $requestTime + 3600;
          }
        }
      }
      $this->state->set("{$cronScheduleId}.next_cron", $nextRun);
    }
  }

  /**
   * This helper runs all migrations of a given group on production environments.
   * On unsuccessful migrations it will try to reset itself.
   * It will send one warning message daily if the last successful import is older "$warningTreshold" days.
   *
   * @param string $group ... mandatory migration group ID
   * @param bool $forceUpdate ... force updates of rows
   * @param int $warningTreshold ... sent warning after x days of unsuccessful cron runs
   */
  public function runMigrationGroup(string $group, bool $forceUpdate = FALSE, int $warningTreshold = 3) {
    $environment = Settings::get('wt.environment');
    if ($environment === 'dev') {
      return;
    }

    $requestTime = $this->time->getRequestTime();

    $query = $this->entityTypeManager->getStorage('migration')->getQuery();
    $query->condition('migration_group', $group)
      ->condition('status', TRUE)
      ->sort('id');
    $query->accessCheck(FALSE);
    $migrationIds = $query->execute();

    foreach ($migrationIds as $migrationId) {
      $migration = $this->migrationPluginManager->createInstance($migrationId);

      if ($migration->getStatus() != MigrationInterface::STATUS_IDLE) {
        $migration->setStatus(MigrationInterface::STATUS_IDLE);
      }

      if ($forceUpdate) {
        $migration->getIdMap()->prepareUpdate();
      }

      $executable = new MigrateExecutable($migration);
      $result = $executable->import();

      $lastSuccessfulImport = $this->keyValue->get('migrate_last_imported')->get($migrationId, 0) / 1000;
      // add an extra grace period of 1 hour to $warningTreshold because the import itself might take an hour
      if ($requestTime - $lastSuccessfulImport > (86400 * $warningTreshold + 3600)) {
        $today = date('Y-m-d', $requestTime);
        if ($this->state->get("{$migrationId}.last_warning_sent", '1970-01-01') != $today) {
          $mailParams = [
            'from' => $this->configFactory->get('system.site')->get('mail'),
            'context' => [
              'subject' => $this->t('Warning: Migration @migration_id failed', [
                '@migration_id' => $migrationId,
              ]),
              'message' => $this->t("The import @group / @migration_id on @site_name failed for more than 4 days. \r\nThe last successful import was on @date. \r\n@adminlink", [
                '@site_name' => $this->configFactory->get('system.site')->get('name'),
                '@date' => $this->dateFormatter->format($lastSuccessfulImport ,'custom', 'Y-m-d H:i:s'),
                '@group' => $group,
                '@migration_id' => $migrationId,
                '@adminlink' => "https://{$this->configFactory->get('wt_dgm.settings')->get('default_portal_domain')}/admin/structure/migrate/manage/{$group}/migrations",
              ]),
            ],
          ];
          $this->mailManager->mail('system', 'mail', 'entwicklung@webtourismus.at', $this->languageManager->getDefaultLanguage()
              ->getId(), $mailParams, FALSE);
          $this->state->set("{$migrationId}.last_warning_sent", $today);
        }
      }
    }
  }
}

