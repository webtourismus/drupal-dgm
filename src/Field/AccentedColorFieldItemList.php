<?php

namespace Drupal\wt_dgm\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\user\Entity\User;

class AccentedColorFieldItemList extends FieldItemList implements FieldItemListInterface {

  use ComputedItemListTrait;

  protected function computeValue() {
    if (($user = $this->getParent()->getEntity()) instanceof User) {
      $this->list[0] = $this->createItem(0, $this->getAccentColor($user->get('field_color')->value, $user->get('field_color_inverse')->value));
    }
  }

  /**
   * Calculates an "accented" color of same hue, but with different lightness.
   *
   * Use this function to create hover effect color changes or similar.
   *
   * @param string $hex
   *   CSS style 3 or 6 digit hex RGB source color
   *
   * @param string $shift
   *   one of the following 3 strings or a hex color code:
   *   'auto'      ... calculate shift to dark or light depending on lightness of $hex (default)
   *                   lightness($hex)  < 0.5 ... shift to light
   *                   lightness($hex) >= 0.5 ... shift to dark
   *   'dark'      ... force shift to darker color
   *   'light'     ... force shift to lighter color
   *   $inverse    ... RGB hex color code for inverse color
   *                   lightness($hex)  < 0.4 ... shift to light
   *                   lightness($hex) >= 0.6 ... shift to dark
   *                   from 0.4 to 0.6 shift depending on lightness($inverse)
   *
   * @param float $lightnessChange
   *   amount to shift the lightness
   *
   *
   * @return string
   *   CSS style hex RGB color value with leading hash
   *   accented highlight color with shifted lightness
   */
  public static function getAccentColor(string $hex, string $shift = 'auto', float $lightnessChange = 0.15) {
    $hex = str_replace('#', '', $hex);
    if (!ctype_xdigit($hex) || $hex == '' || is_null($hex)) {
      throw new \Exception('Must use a hexadecimal number as color source.');
    }
    if (strlen($hex) == 3) {
      $hex = $hex[0].$hex[0].$hex[1].$hex[1].$hex[2].$hex[2];
    }
    if (strlen($hex) != 6) {
      throw new \Exception('Must use a 3 or 6 digit hex RGB color as source.');
    }
    $rgb = self::convertHexToFloat($hex);
    $hsl = self::convertRgbToHsl(...$rgb);

    // the resulting accent color is NOT calculated on the $shift color value
    // -    if $shift is empty
    // - OR if $shift is no hex color at all, but a string (dark|light|auto)
    // - OR if the source $hex color is very dark (lightness < 0.4)
    // - OR if the source $hex color is very light (lightness > 0.6)
    if (is_null($shift) || in_array($shift, ['dark', 'light', 'auto', '']) || $hsl[2] < 0.4 || $hsl[2] > 0.6) {
      if ($shift === 'dark') {
        $hsl[2] -= $lightnessChange;
      }
      elseif ($shift === 'light') {
        $hsl[2] += $lightnessChange;
      }
      elseif ($hsl[2] > 0.5) {
        $hsl[2] -= $lightnessChange;
      }
      else {
        $hsl[2] += $lightnessChange;
      }
      $result = self::convertHslToRgb(...$hsl);
      return '#' . self::convertFloatToHex($result);
    }

    // in any other case the resulting accent color is calculated based
    // upon the lightness of the given $shift hex color value
    $iRgb = str_replace('#', '', $shift);
    if (!ctype_xdigit($iRgb) || $iRgb == '' || is_null($iRgb)) {
      throw new \Exception('Must use auto|dark|light or a hex RGB color for shift calculation.');
    }
    if (strlen($iRgb) == 3) {
      $inverse = $iRgb[0].$iRgb[0].$iRgb[1].$iRgb[1].$iRgb[2].$iRgb[2];
    }
    if (strlen($iRgb) != 6) {
      throw new \Exception('Must use a 3 or 6 digit hex RGB color as inverse.');
    }
    $iHsl = self::convertRgbToHsl(...self::convertHexToFloat($iRgb));
    if ($iHsl[2] > 0.5) {
      $hsl[2] -= $lightnessChange;
    }
    else {
      $hsl[2] += $lightnessChange;
    }
    $result = self::convertHslToRgb(...$hsl);
    return '#' . self::convertFloatToHex($result);
  }

  private static function convertHexToFloat($hex) {
    $r = hexdec($hex[0] . $hex[1]) / 255;
    $g = hexdec($hex[2] . $hex[3]) / 255;
    $b = hexdec($hex[4] . $hex[5]) / 255;
    return [$r, $g, $b];
  }

  private static function convertFloatToHex($rgb) {
    return sprintf('%02x', round($rgb[0] * 255)) . sprintf('%02x', round($rgb[1] * 255)) . sprintf('%02x', round($rgb[2] * 255));
  }

  private static function convertRgbToHsl($r, $g, $b) {
    $max = max($r, $g, $b);
    $min = min($r, $g, $b);
    $h = $s = $l = ($max + $min) / 2;

    if ($max == $min) {
      $h = $s = 0; // achromatic
    }
    else {
      $d = $max - $min;
      $s = $l > 0.5 ? $d / (2 - $max - $min) : $d / ($max + $min);
      switch ($max) {
        case $r:
          $h = ($g - $b) / $d + ($g < $b ? 6 : 0);
          break;
        case $g:
          $h = ($b - $r) / $d + 2;
          break;
        case $b:
          $h = ($r - $g) / $d + 4;
          break;
      }
      $h /= 6;
    }

    return [$h, $s, $l];
  }

  private static function convertHslToRgb($h, $s, $l) {
    if ($s == 0) {
      $r = $g = $b = $l; // achromatic
    }
    else {

      $q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
      $p = 2 * $l - $q;
      $r = self::hue2rgb($p, $q, $h + 1 / 3);
      $g = self::hue2rgb($p, $q, $h);
      $b = self::hue2rgb($p, $q, $h - 1 / 3);
    }

    return [$r, $g, $b];
  }

  private static function hue2rgb($p, $q, $t) {
    if ($t < 0) {
      $t += 1;
    }
    if ($t > 1) {
      $t -= 1;
    }
    if ($t < 1 / 6) {
      return $p + ($q - $p) * 6 * $t;
    }
    if ($t < 1 / 2) {
      return $q;
    }
    if ($t < 2 / 3) {
      return $p + ($q - $p) * (2 / 3 - $t) * 6;
    }
    return $p;
  }

}
