<?php

namespace Drupal\wt_dgm\Path;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;

class OutboundHostProcessor implements OutboundPathProcessorInterface {

  /**
   * @var $currentUser AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var $configFactory ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var $frontendCache array['username' => 'scheme:://username.dgmdomain.com']
   */
  protected $frontendCache;

  public function __construct(AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->frontendCache = [];
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = array(), Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if ($this->currentUser->isAnonymous()) {
      return $path;
    }

    try {
      $url = Url::fromUri('internal:' .  $path);
    }
    catch (\Throwable $throwable) {
      return $path;
    }
    if (!$url) {
      return $path;
    }

    try {
      $routeName = $url->getRouteName();
    }
    catch (\Throwable $throwable) {
      return $path;
    }
    if ($routeName != 'entity.node.canonical') {
      return $path;
    }

    if (!$this->currentUser->hasPermission('has guest guide subdomain')) {
      return $path;
    }

    $params = $url->getRouteParameters();
    if (!isset($params['node'])) {
      return $path;
    }

    $node = $params['node'];
    if (!($node instanceof Node) && is_numeric($node)) {
      $node = Node::load($node);
    }
    if (!($node instanceof Node)) {
      return $path;
    }

    $config = $this->configFactory->get('wt_dgm.settings');
    $dgmNodes = $config->get('guide_nodetypes');
    if (!in_array($node->bundle(), $dgmNodes)) {
      return $path;
    }

    if ($node->bundle() == 'dgmdashboard') {
      return $path;
    }

    if ($this->currentUser->hasPermission('administer portal')) {
      $portalNodes = $config->get('portal_nodetypes');
      if (in_array($node->bundle(), $portalNodes)) {
        return $path;
      }
    }

    $userDomain = $this->getDgmFrontendHost($request);
    if (!$userDomain) {
      return $path;
    }

    $options['absolute'] = TRUE;
    $options['base_url'] = $request->getScheme() . '://' . $userDomain;

    if ($bubbleable_metadata) {
      $bubbleable_metadata->addCacheContexts([
        'user',
      ]);
    }
    return $path;
  }

  protected function getDgmFrontendHost(Request $request) {
    //simple caching
    $username = $this->currentUser->getAccountName();
    if (array_key_exists($username, $this->frontendCache)) {
      return $this->frontendCache[$username];
    }
    else {
      //initialize with NULL
      $this->frontendCache[$username] = NULL;
    }

    if (!($this->currentUser->hasPermission('has guest guide subdomain'))) {
      return NULL;
    }

    $config = $this->configFactory->get('wt_dgm.settings');
    $dgmDomains = $config->get('guide_domains');
    $host = $request->getHost();
    foreach ($dgmDomains as $dgmDomain) {
      if (substr_compare($host, $dgmDomain, -strlen($dgmDomain)) === 0) {
        $this->frontendCache[$username] = $username . '.' . $dgmDomain;
        return $this->frontendCache[$username];
      }
    }
    /**
     * If a DGM user logs in on a portal/non-DGM domain (or a portal admin masquerades
     * as DGM user), we need to change subdomain AND main domain
     */
    $portalDomains = $config->get('portal_domains');
    if (in_array($host, $portalDomains)) {
      $this->frontendCache[$username] = $username . '.' . $config->get('default_guide_domain');
      return $this->frontendCache[$username];
    }

    return NULL;
  }
}
