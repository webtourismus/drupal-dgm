<?php
declare(strict_types=1);

namespace Drupal\wt_dgm\Entity;
use Drupal\wt_dgm\DgmVocabularyListBuilder;

trait DgmTermTrait {
  public function getParent(): ?DgmTermInterface {
    return $this->get('parent')->entity;
  }

  public function getAncestors(bool $includeSelf = FALSE, int $maxLevels = 10): array {
    $result = [];
    if ($includeSelf) {
      $result[] = $this;
    }
    $parent = $this->getParent();
    while($parent) {
      $result[] = $parent;
      $parent = $parent->getParent();
      if (count($result) >= $maxLevels) {
        break;
      }
    }
    return $result;
  }

  public function getImageUri(): ?string {
    $dgmHelper = \Drupal::service('wt_dgm.helper');

    if ($dgmHelper->hasDgmDomain()) {
      $userId = $this->currentUser()->id();
    }
    elseif ($dgmHelper->getFrontendUser()) {
      $userId = $dgmHelper->getFrontendUser()->id();
    }
    else {
      $userId = NULL;
    }

    $termTree = $this->getAncestors(TRUE);
    foreach($termTree as $term)  {
      $overrideContents = $this->entityTypeManager()->getStorage('storage')->loadByProperties([
        'type' => DgmVocabularyListBuilder::STORAGE_BUNDLE,
        'user_id' => $userId,
        'field_entity_type' => $term->getEntityTypeId(),
        'field_entity_id' => $term->id(),
      ]);
      $overrideContent = reset($overrideContents);
      if ($overrideContent && !$overrideContent->get('field_image')->isEmpty()) {
        return $overrideContent->get('field_image')->first()->entity->getFileUri();
      }

      if ($term->hasField('field_imagefile') && !$term->get('field_imagefile')->isEmpty()) {
        return $term->get('field_imagefile')->first()->entity->getFileUri();
      }
    }

    return $dgmHelper->getVocabularyImageUri($this->vid->entity);
  }
}
