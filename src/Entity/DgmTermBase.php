<?php

declare(strict_types=1);

namespace Drupal\wt_dgm\Entity;

use Drupal\taxonomy\Entity\Term;

abstract class DgmTermBase extends Term implements DgmTermInterface {

  use DgmTermTrait;
}
