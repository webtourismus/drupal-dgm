<?php
declare(strict_types=1);

namespace Drupal\wt_dgm\Entity;

use Drupal\taxonomy\TermInterface;

interface DgmTermInterface extends TermInterface {
  public function getParent(): ?DgmTermInterface;
  public function getAncestors(bool $includeSelf = FALSE, int $maxLevels = 10): array;
  public function getImageUri(): ?string;
}
