<?php

namespace Drupal\wt_dgm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\storage\Entity\Storage;
use Drupal\taxonomy\VocabularyListBuilder;

/**
 * Vocabularies overview with override iamges
 *
 * See https://blog.dcycle.com/blog/2022-02-07/alter-list-view/.
 */
class DgmVocabularyListBuilder extends VocabularyListBuilder {

  /**
   * The storage entity bundle type for overriding content of terms and vocabularies
   */
  public const STORAGE_BUNDLE = 'override_category';

  /**
   * - keys are paragraph bundle names used as tiles in DGM
   * - values are taxonomy vocabularies suitable for image overrides
   */
  public const OVERRIDE_VOCABULARIES = [
    'dgmpois' => 'poi',
    'dgmevents' => 'event',
    'dgmgastronomies' => 'gastronomy',
    'dgminfrastructures' => 'infrastructure',
    'dgmtours' => 'tour',
  ];

  protected function getEntityIds() {
    if (
      \Drupal::currentUser()->hasPermission('wt_dgm.content_override.categories') &&
      !\Drupal::currentUser()->hasPermission('administer taxonomy')
    ) {
      return self::OVERRIDE_VOCABULARIES;
    }
    return parent::getEntityIds();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    if (
      \Drupal::currentUser()->hasPermission('wt_dgm.content_override.categories') &&
      !\Drupal::currentUser()->hasPermission('administer taxonomy')
    ) {
      unset($this->weightKey);
    }
    return parent::render();
  }

  public function buildHeader() {
    $header['image'] = t('Image');

    return parent::buildHeader() + $header;
  }

  public function buildRow(EntityInterface $entity) {
    $dgmHelper = \Drupal::service('wt_dgm.helper');
    $images = $dgmHelper->getOverrideContents($entity, self::STORAGE_BUNDLE);
    $image = reset($images);
    $row = ['image' => NULL];
    if ($image instanceof Storage && !$image->get('field_image')->isEmpty()) {
      $row['image'] = [
        'data' => [
          '#theme' => 'image_style',
          '#style_name' => '50x50',
          '#uri' => $image->get('field_image')->first()->entity->getFileUri(),
        ],
      ];
    }
    return parent::buildRow($entity) + $row;
  }
}
