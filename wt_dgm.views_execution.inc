<?php

use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;

/**
 * Contrary to wt_dgm.views.inc this file is not permanently cached
 * e.g. query_alter needs to be run on multiple times when filters change
 */


/**
 * @see wt_dgm_views_data_alter
 * @see \Drupal\wt_dgm\Plugin\views\filter\DgmUser
 *
 * inspired by CurrentUser filter from core user module
 */
function wt_dgm_views_query_substitutions(ViewExecutable $view) {
  $user = \Drupal::service('wt_dgm.helper')->getFrontendUser();
  if ($user) {
    $user_id = $user->id();
  }
  else {
    $user_id = -1;
  }
  return ['***DGM_USER***' => $user_id];
}


/**
 * Modifies a 2-field date filter to work with a single filter input value.
 *
 *
 * Events have a mandatory start date and a mandatory end date.
 * (2 fields in full date+time format: field_dates_value [start] and field_dates_endvalue [end]).
 *
 * The user filters one single day in the browser UI. But in SQL we have to filter both fields like
 *    field_dates_value <= selected day
 *    AND
 *    field_dates_endvalue >= selected day
 * to also include long running events spanning multiple days.
 *
 * So technically we have 2 date filters, but the 2nd filter (the endvalue filter) is just a placeholder filter value
 * which needs to be replaced with the first value (which is whatever the user selected in frontend).
 */
function wt_dgm_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() != 'event' || strpos($view->current_display, 'dgm_all_') !== 0) {
    return;
  }


  /** @var array $query->where */
  foreach ($query->where as &$condition_group) {
    foreach ($condition_group['conditions'] as &$condition) {
      // We only care about date filter conditions.
      if (!strpos($condition['field'], '.field_dates_') || $condition['operator'] != 'formula') {
        continue;
      }

      // Start value filter: Safe the user-selected date for later use.
      // We use the user exposed form filter value (HTTP GET query) to calculate the comparision value
      // (and not the timezone-depending SQL string-to-date filter value)
      // to avoid "off-by-one-hour-errors" when crossing daylight saving modes
      if (is_array($view->exposed_raw_input) &&
        array_key_exists('field_dates_value', $view->exposed_raw_input) &&
        preg_match('/^\d{4}-\d{2}-\d{2}$/', $view->exposed_raw_input['field_dates_value']) == 1
      ) {
        $day = $view->exposed_raw_input['field_dates_value'];
      } else {
        $day = date_format((new DateTime()), 'Y-m-d');
      }

      if (strpos($condition['field'], '<=')) {
        [$startCompareLeft, $startCompareRight] = explode('<=', $condition['field']);
        // Do not compare with midnight, this could lead to "off-by-one-hour-errors" due daylight saving modes.
        // E.g. today is 2024-03-28, daylight saving starts on night from 2024-03-31 to 2024-04-01,
        // then 2024-03-28 && 2024-03-29 would work, all day filters after the switch would be incorrect.
        $condition['field'] = $startCompareLeft . " < '{$day}\T23:00:00'";
      }

      // End value filter: Replace the dummy filter value with the user-selected date
      if (strpos($condition['field'], '>=')) {
        [$endCompareLeft, $endCompareRight] = explode('>=', $condition['field']);
        // Do not compare with midnight, this could lead to "off-by-one-hour-errors" due daylight saving modes.
        // E.g. today is 2024-03-28, daylight saving starts on night from 2024-03-31 to 2024-04-01,
        // then 2024-03-28 && 2024-03-29 would work, but all day filters after the switch would be incorrect.
        $condition['field'] = $endCompareLeft . " > '{$day}\T01:00:00'";
      }
    }
  }
}
