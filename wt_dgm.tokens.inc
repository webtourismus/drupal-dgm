<?php

/**
 * Implements hook_token_info().
 */
function wt_dgm_token_info() {
  $info = ['types' => [], 'tokens' => []];
  $info['types']['dgm'] = [
    'name' => t('DGM'),
    'description' => t('Tokens related to guest guide and the DGM user')
  ];
  $info['tokens']['dgm']['channel'] = [
    'name' => t('DGM channel'),
  ];
  $info['tokens']['dgm']['id'] = [
    'name' => t('DGM user ID'),
  ];
  $info['tokens']['dgm']['account_name'] = [
    'name' => t('DGM user account name'),
    'description' => t('Username and subdomain name'),
  ];
  $info['tokens']['dgm']['marketing_name'] = [
    'name' => t('DGM user marketing name'),
    'description' => t('The official/marketing name used for communication with customers'),
  ];
  $info['tokens']['dgm']['legal_name'] = [
    'name' => t('DGM user legal name'),
    'description' => t('The legal name (company, owner,...) of the DGM user'),
  ];
  $info['tokens']['dgm']['street'] = [
    'name' => t('DGM user street'),
  ];
  $info['tokens']['dgm']['postcode'] = [
    'name' => t('DGM user postcode'),
  ];
  $info['tokens']['dgm']['city'] = [
    'name' => t('DGM user city'),
  ];
  $info['tokens']['dgm']['country_name'] = [
    'name' => t('DGM user country'),
  ];
  $info['tokens']['dgm']['country_iso'] = [
    'name' => t('DGM user country ISO2 code'),
  ];
  $info['tokens']['dgm']['geo_lat'] = [
    'name' => t('DGM user geo latitude'),
  ];
  $info['tokens']['dgm']['geo_lng'] = [
    'name' => t('DGM user geo longitude'),
  ];
  $info['tokens']['dgm']['phone'] = [
    'name' => t('DGM user phone number'),
  ];
  $info['tokens']['dgm']['email'] = [
    'name' => t('DGM user email address'),
  ];
  $info['tokens']['dgm']['homepage'] = [
    'name' => t('DGM user homepage'),
  ];
  $info['tokens']['dgm']['vat'] = [
    'name' => t('DGM user VAT number'),
  ];
  $info['tokens']['dgm']['fbnr'] = [
    'name' => t('DGM user commercial registry number'),
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function wt_dgm_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  $dgmReplacments = _wt_dgm_tokens_dgm($type, $tokens, $data, $options, $bubbleable_metadata);
  $eventDateReplacments = _wt_dgm_tokens_wt_eventdate($type, $tokens, $data, $options, $bubbleable_metadata);

  return array_merge($replacements, $dgmReplacments, $eventDateReplacments);
}

function _wt_dgm_tokens_dgm($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type != 'dgm') {
    return [];
  }

  /** @var $helper \Drupal\wt_dgm\DgmHelper */
  $helper = \Drupal::service('wt_dgm.helper');
  $user = $helper->getFrontendUser();

  //if we have a logged in user in admin area, he might be eligible as DGM frontend user
  if (!$user) {
    if (in_array('dgm', \Drupal::currentUser()->getRoles())) {
      $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    }
  }

  if (!$user) {
    return [];
  }

  foreach ($tokens as $name => $original) {
    //most tokens are simple field values
    if ($user->hasField('field_' . $name) && $user->get('field_' . $name)->getValue()) {
      $replacements[$original] = $user->get('field_' . $name)->value;
      continue;
    }
    switch ($name) {
      case 'channel':
        $replacements[$original] = $helper->getChannel();
        break;
      case 'id':
        $replacements[$original] = $user->id();
        break;
      case 'account_name':
        $replacements[$original] = $user->getAccountName();
        break;
      case 'email':
        $replacements[$original] = $user->getEmail();
        break;
      case 'country_name':
        $replacements[$original] = $user->get('field_country')->getString();
        break;
      case 'country_iso':
        $replacements[$original] = $user->get('field_country')->value;
        break;
      case 'geo_lat':
        $replacements[$original] = $user->get('field_geo')->lat;
        break;
      case 'geo_lng':
        $replacements[$original] = $user->get('field_geo')->lng;
        break;
      default:
        break;
    }
  }

  $bubbleable_metadata->addCacheableDependency($user);
  return $replacements;
}

/**
 * replacements for formatted wt_eventtime tokens
 *
 * @see \field_tokens() in token.tokens.inc
 */
function _wt_dgm_tokens_wt_eventdate($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if (!empty($data['field_property'])) {
    foreach ($tokens as $token => $original) {
      $filtered_tokens = $tokens;
      $delta = 0;
      $parts = explode(':', $token);
      if (is_numeric($parts[0])) {
        if (count($parts) > 1) {
          $delta = $parts[0];
          $property_name = $parts[1];
          // Pre-filter the tokens to select those with the correct delta.
          $filtered_tokens = \Drupal::token()->findWithPrefix($tokens, $delta);
          // Remove the delta to unify between having and not having one.
          array_shift($parts);
        }
        else {
          // Token is fieldname:delta, which is invalid.
          continue;
        }
      }
      else {
        $property_name = $parts[0];
      }

      if (isset($data[$data['field_name']][$delta])) {
        $field_item = $data[$data['field_name']][$delta];
      }
      else {
        // The field has no such delta, abort replacement.
        continue;
      }

      if (in_array($field_item->getFieldDefinition()->getType(), ['wt_eventdate']) && in_array($property_name, ['date', 'start_date', 'end_date']) && !empty($field_item->$property_name)) {
        $timestamp = $field_item->{$property_name}->getTimestamp();
        // If the token is an exact match for the property or the delta and the
        // property, use the timestamp as-is.
        if ($property_name == $token || "$delta:$property_name" == $token) {
          $replacements[$original] = $timestamp;
        }
        else {
          $date_tokens = \Drupal::token()->findWithPrefix($filtered_tokens, $property_name);
          $replacements += \Drupal::token()->generate('date', $date_tokens, ['date' => $timestamp], $options, $bubbleable_metadata);
        }
      }
    }
  }
  return $replacements;

}

/**
 * provide date format tokens for wt_eventdate field type
 *
 * @see \field_token_info_alter() in token.tokens.inc
 */
function wt_dgm_token_info_alter(&$info) {
  // The Token module provides the base settings that we extend.
  if (!\Drupal::hasService('token.entity_mapper')) {
    return;
  }

  // Attach field tokens to their respecitve entity tokens.
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->entityClassImplements(\Drupal\Core\Entity\ContentEntityInterface::class)) {
      continue;
    }

    // Make sure a token type exists for this entity.
    $token_type = \Drupal::service('token.entity_mapper')->getTokenTypeForEntityType($entity_type_id);
    if (empty($token_type) || !isset($info['types'][$token_type])) {
      continue;
    }

    $fields = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type_id);
    foreach ($fields as $field_name => $field) {
      /** @var \Drupal\field\FieldStorageConfigInterface $field */
      if ($field->getType() != 'wt_eventdate') {
        continue;
      }

      $field_token_name = $token_type . '-' . $field_name;

      // Provide format token for datetime fields.
      if ($field->getType() == 'wt_eventdate') {
        $info['tokens'][$field_token_name]['start_date'] = $info['tokens'][$field_token_name]['value'];
        $info['tokens'][$field_token_name]['start_date']['name'] .= ' ' . t('formatted');
        $info['tokens'][$field_token_name]['start_date']['description'] .= t('Append desired date format with colon, e.g. ":short"');
        $info['tokens'][$field_token_name]['start_date']['type'] = 'date';
        $info['tokens'][$field_token_name]['end_date'] = $info['tokens'][$field_token_name]['end_value'];
        $info['tokens'][$field_token_name]['end_date']['name'] .= ' ' . t('formatted');
        $info['tokens'][$field_token_name]['end_date']['description'] .= t('Append desired date format with colon, e.g. ":short"');
        $info['tokens'][$field_token_name]['end_date']['type'] = 'date';
      }
    }
  }
}

