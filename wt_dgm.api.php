<?php

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

function hook_wt_dgm_alter_channel_defaultvalues(&$values, FieldableEntityInterface $entity, FieldDefinitionInterface $definition) {
  // @see wt_dgm_channel_defaultvalues()
}
