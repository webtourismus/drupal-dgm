<?php

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\Entity\Role;

function wt_dgm_post_update_1000_set_nighttime_and_summerseason_for_all_users() {
  $userStorage = \Drupal::entityTypeManager()->getStorage('user');
  $query = $userStorage->getQuery();
  $query->accessCheck(FALSE);
  $userIds = $query->execute();
  $users = $userStorage->loadMultiple($userIds);
  foreach ($users as $user) {
    $user->set('field_summer_start', '2000-05-01');
    $user->set('field_summer_end', '2000-11-30');
    $user->set('field_night_start', '22:00');
    $user->set('field_night_end', '06:00');
    $user->save();
  }
}


function wt_dgm_post_update_1001_replace_page_paragraph_with_az_paragraph() {
  $stuffNeedingUpdate = [
    'dgmmobile' => ['field_mobile'],
    'dgmmenu' => ['field_menu'],
    'dgminfopoint' => ['field_slot1', 'field_slot2', 'field_slot3', 'field_slot4', 'field_slot5', 'field_slot6', 'field_slot7', 'field_slot8'],
  ];
  $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
  $message = '';
  foreach ($stuffNeedingUpdate as $bundle => $fields) {
    $count = 0;
    $message .= "Updating $bundle \r\n";
    foreach ($fields as $field) {
      $query = $nodeStorage->getQuery();
      $query->condition('type', $bundle);
      $query->condition($field . '.entity:paragraph.type', 'dgmpage');
      $query->accessCheck(FALSE);
      $nodeIds = $query->execute();
      $nodes = $nodeStorage->loadMultiple($nodeIds);
      /** @var $node \Drupal\node\Entity\Node*/
      foreach ($nodes as $node) {
        foreach ($node->get($field)->referencedEntities() as $idx => $oldParagraph) {
          /** @var  $oldParagraph \Drupal\paragraphs\Entity\Paragraph */
          if ($oldParagraph->bundle() !== 'dgmpage') {
            continue;
          }
          $targetPage = $oldParagraph->get('field_node')->entity;
          if (!$targetPage) {
            continue;
          }
          if ($targetPage->bundle() != 'a_z') {
            continue;
          }
          $newParagraph = Paragraph::create([
            'type' => 'dgma_z',
            'field_tilesize' => $oldParagraph->get('field_tilesize')->value,
          ]);
          $newParagraph->save();
          $oldParagraph->delete();
          $node->get($field)->set($idx, [
            'target_id' => $newParagraph->id(),
            'target_revision_id' => $newParagraph->getRevisionId(),
          ]);
          $node->save();
          $message .= "Node {$node->id()} Field {$field} Nr. {$idx} updated\r\n";
          $count++;
        }
      }
    }
    $message .= "---------------------------------------------\r\n";
    $message .= "{$count} {$bundle} fields updated\r\n";
    $message .= "=============================================\r\n";
  }
  return $message;
}

function wt_dgm_post_update_1100_touren() {
  $configFactory = \Drupal::configFactory();
  $message = '';

  /**
   * Add new permissions for new vocabulary and new content type
   */
  $permissionsToUpdate = [
    'dgm' => [
      'create tour content',
      'delete own tour content',
      'edit own tour content',
    ],
    'association' => [
      'create tour content',
      'delete own tour content',
      'edit own tour content',
    ],
    'dgmadmin' => [
      'create tour content',
      'delete any tour content',
      'edit any tour content',
      'create terms in tour',
      'delete terms in tour',
      'edit terms in tour',
    ],
    'editor' => [
      'create tour content',
      'delete any tour content',
      'edit any tour content',
      'create terms in tour',
      'delete terms in tour',
      'edit terms in tour',
    ],
  ];
  /** @var $editorRole Role */
  $rolesUpdated = [];
  foreach ($permissionsToUpdate as $role => $permArr) {
    /** @var $roleObj Role */
    $roleObj = Role::load($role);
    if ($roleObj instanceof Role) {
      foreach ($permArr as $perm) {
        $roleObj->grantPermission($perm);
      }
      $roleObj->save();
      $rolesUpdated[] = $role;
    }
  }
  if ($rolesUpdated) {
    $message .= "=============================================\r\n";
    $message .= "Updated permissions for these roles: " . join(', ', $rolesUpdated). "\r\n";
  }

  /**
   * wt_dgm settings for tour node type
   */
  $dgmConfig = $configFactory->getEditable('wt_dgm.settings');
  $guideNodeTypes = $dgmConfig->get('guide_nodetypes');
  $configUpdated = [];
  if (!in_array('tour', $guideNodeTypes)) {
    $dgmConfig->set('guide_nodetypes', array_merge($guideNodeTypes, ['tour']));
    $configUpdated[] = 'guide_nodetypes';
  }
  $portalNodeTypes = $dgmConfig->get('portal_nodetypes');
  if (!in_array('tour', $portalNodeTypes)) {
    $dgmConfig->set('portal_nodetypes', array_merge($portalNodeTypes, ['tour']));
    $configUpdated[] = 'portal_nodetypes';
  }
  if ($configUpdated) {
    $dgmConfig->save(TRUE);
    $message .= "=============================================\r\n";
    $message .= "Added tour node type to these wt_dgm.settings: " . join(', ', $configUpdated). "\r\n";
  }

  $message .= "=============================================\r\n";
  return $message;
}
