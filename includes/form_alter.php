<?php

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\wt_cms\CacheContext\SeasonCacheContext;

/**
 *
 * USER/ACCOUNT SPECIFIC FORM ALTERING
 *
 */

function wt_dgm_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  _wt_dgm_form_user_form_alter_hide_masquerade($form, $form_state, $form_id);
}

/**
 * Hide the masquerade input field from module "masquerade_field" (not module "masquerade")
 * on any form except the core "user_form"
 */
function _wt_dgm_form_user_form_alter_hide_masquerade(&$form, FormStateInterface $form_state, $form_id) {
  if (array_key_exists('masquerade_as', $form) && $form_id != 'user_form') {
    $form['masquerade_as']['#access'] = FALSE;
  }
}

function wt_dgm_form_user_dgmdesign_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $fieldToEnhance = 'field_css';
  if (!isset($form[$fieldToEnhance])) {
    return;
  }
  $inlineTemplate = <<<CSS_INFO
<style>
.custom_css_table {
  margin: 1rem 0;
  font-size: 0.8rem;
}
.custom_css_table td {
  height: auto;
  padding-top: 0.1em;
  padding-bottom: 0.1em;
}
.custom_css_table td:first-child {
  font-family: monospace;
}
</style>
<table class="custom_css_table">
  <tr>
    <th>Custom CSS property</th>
    <th>Beschreibung</th>
  </tr>
  <tr>
    <td>--font-family--base</td>
    <td>Schriftfamilie für Fließtexte</td>
  </tr>
  <tr>
    <td>--font-weight--base</td>
    <td>Gewicht/Stärke von Fließtexten</td>
  </tr>
  <tr>
    <td>--font-weight--bold</td>
    <td>Gewicht/Stärke von fetten Fließtext-Schrift</td>
  </tr>
  <tr>
    <td>--font-family--decorative</td>
    <td>Schriftfamilie für Überschriften</td>
  </tr>
  <tr>
    <td>--color--foreground</td>
    <td>Textfarbe</td>
  </tr>
  <tr>
    <td>--color--background</td>
    <td>Hintergrundfarbe</td>
  </tr>
  <tr>
    <td>--color--background-accent</td>
    <td>Hintergrundfarbe für akzentuierte Inhalte</td>
  </tr>
</table>
CSS_INFO;
  $renderElementToAppend[$fieldToEnhance . '_append'] = [
    '#type' => 'details',
    '#title' => 'Häufig verwendete CSS Variablen in der Digitalen Gästemappe',
    '#parents' => $form[$fieldToEnhance]['#parents'] ?? [],
    '#weight' => isset($form[$fieldToEnhance]['#weight']) ? $form[$fieldToEnhance]['#weight'] + 1 : NULL,
  ];
  $renderElementToAppend[$fieldToEnhance . '_append']['content'] = [
    '#type' => 'inline_template',
    '#template' => $inlineTemplate,
  ];
  $form[$fieldToEnhance] = array_merge($form[$fieldToEnhance], $renderElementToAppend);
  $placeholderText = <<<PLACEHOLDER_TEXT
p.example_class {
  font-size: 14px;
  color: purple;
}
div.example_two {
  background-color: yellow !important;
}
PLACEHOLDER_TEXT;
  $form[$fieldToEnhance]['widget'][0]['value']['#placeholder'] = $placeholderText;
}


/**
 * Hide the "internal_key" field for all users except administrators, this
 * field is only ever used as hardcoded "magic value" in templates or logic
 */
function wt_dgm_form_taxonomy_term_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $term = $form_state->getFormObject()->getEntity();
  if ($term->hasField('internal_key') && !in_array('administrator', \Drupal::currentUser()->getRoles())) {
    $form['internal_key']['#access'] = FALSE;
  }
}



/**
 * Global altering of node forms
 * (single-bunde modifications are not done in here)
 */
function wt_dgm_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  _wt_dgm_node_form_alter_hidepromote($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_remote_data($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_channel_data($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_seo($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_sidebar($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_hidetitle($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_disableredirect($form, $form_state, $form_id);
  //_wt_dgm_node_form_alter_addTranslation($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_livedata($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_videodescription($form, $form_state, $form_id);
  _wt_dgm_node_form_alter_alterDgmSaveMessage($form, $form_state, $form_id);
}


/**
 * Always hide Drupal core promotion options (frontpage, sticky)
 */
function _wt_dgm_node_form_alter_hidepromote(&$form, FormStateInterface $form_state, $form_id) {
  if (in_array('administrator', \Drupal::currentUser()->getRoles())) {
    return;
  }

  if (array_key_exists('options', $form)) {
    $form['options']['#access'] = FALSE;
  }
}

/**
 * Remote_* fields are metadata, move them to sidebar
 *
 * Remark: "remote_datasource" & "remote_id should" be hidden in form display settings!
 */
function _wt_dgm_node_form_alter_remote_data(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();

  $remoteMetadata = ['remote_datasource', 'remote_id', 'remote_email'];
  $nodeHasRemoteMetadata = FALSE;
  foreach ($remoteMetadata as $rm) {
    if ($node->hasField($rm)) {
      $nodeHasRemoteMetadata = TRUE;
      break;
    }
  }
  if (!$nodeHasRemoteMetadata) {
    return;
  }



  if (\Drupal::currentUser()->hasPermission('manage remote metadata')) {
    $remoteDatasourceMarkup = FALSE;
    if ($node->remote_datasource->value) {
      $remoteDatasourceMarkup = '<div class="description form-item" id="entity-meta-remote_intro">'
      . t('This data was imported from a remote datasource. It is recommended to edit this data in the remote source and not here.')
      . '</div>'
      . '<div class="container-inline form-item" id="edit-meta-remote_datasource">'
      . '<label for="edit-meta-remote_datasource">'
      . $node->get('remote_datasource')->getFieldDefinition()->getLabel()
      . '</label> ' . $node->remote_datasource->value . '</dd>'
      . '</div>'
      . '<div class="container-inline form-item" id="edit-meta-remote_id">'
      . '<label for="edit-meta-remote_datasource">'
      . $node->get('remote_id')->getFieldDefinition()->getLabel()
      . '</label> ' . $node->remote_id->value ?: '-'
        . '</div>';
    }
    if ($remoteDatasourceMarkup || $node->hasField('remote_email')) {
      $form['remote_metadata'] = [
        '#type' => 'details',
        '#title' => t('Remote data'),
        '#group' => 'advanced',
        '#open' => FALSE,
        '#weight' => 1000,
      ];
      if ($remoteDatasourceMarkup) {
        $form['remote_metadata']['remote_source'] = [
          '#type' => 'item',
          '#markup' => $remoteDatasourceMarkup,
        ];
      }
      if ($node->hasField('remote_email')) {
        $form['remote_email']['#group'] = 'remote_metadata';
      }
    }
  }
  else {
    foreach ($remoteMetadata as $rm) {
      if (array_key_exists($rm, $form)) {
        $form[$rm]['#access'] = FALSE;
      }
    }
  }
}

/**
 * Remove clutter on edit forms
 */
function _wt_dgm_node_form_alter_sidebar(&$form, FormStateInterface $form_state, $form_id) {
  if (!array_key_exists('advanced', $form)) {
    return;
  }

  $permissionsAllowingSidebar = [
    'administer nodes',
    'bypass node access',
    'administer redirects',
    'create url aliases',
    'administer sitemap settings',
    'administer meta tags',
    'manage remote metadata',
    'manage channel publishing',
  ];
  $hasSidebarPermissions = FALSE;
  foreach ($permissionsAllowingSidebar as $perm) {
    if (\Drupal::currentUser()->hasPermission($perm)) {
      $hasSidebarPermissions = TRUE;
      break;
    }
  }

  if (isset($form['field_originator'])) {
    if ($hasSidebarPermissions) {
      $form['field_originator']['#group'] = 'author';
    }
    else {
      $form['field_originator']['#access'] = FALSE;
    }
  }

  if (!$hasSidebarPermissions) {
    $form['advanced']['#access'] = FALSE;
    return;
  }

  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if (in_array($node->bundle(), ['dgmtv', 'dgmmobile', 'dgminfopoint', 'dgmmenu', 'dgmdashboard', 'a_z'])) {
    $form['advanced']['#access'] = FALSE;
    return;
  }
}

/**
 * Channel publishing is metadata, move it to sidebar
 */
function _wt_dgm_node_form_alter_channel_data(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if (!$node->hasField('channels')) {
    return;
  }

  if (\Drupal::currentUser()->hasPermission('manage channel publishing')) {
    $form['channel_publishing'] = [
      '#type' => 'details',
      '#title' => t('Channels'),
      '#group' => 'advanced',
      '#open' => FALSE,
      '#weight' => 1100,
    ];
    $form['channels']['#group'] = 'channel_publishing';
  }
  else {
    $form['channels']['#access'] = FALSE;
  }
}

/**
 * Only admins should see SEO options, but not DGM users
 */
function _wt_dgm_node_form_alter_seo(&$form, FormStateInterface $form_state, $form_id) {
  $node = $form_state->getFormObject()->getEntity();
  if (!\Drupal::currentUser()->hasPermission('manage seo settings') ||
    in_array($node->bundle(), ['dgmtv', 'dgmmobile', 'dgminfopoint', 'dgmmenu', 'dgmpage', 'dgmlist', 'a_z', 'dgmform'])
  ) {
    if (array_key_exists('simple_sitemap', $form)) {
      $form['simple_sitemap']['#access'] = FALSE;
    }
    if (array_key_exists('field_seo', $form)) {
      $form['field_seo']['#access'] = FALSE;
    }
    if (array_key_exists('path', $form)) {
      $form['path']['#access'] = FALSE;
    }
    if (array_key_exists('url-redirects', $form)) {
      $form['url-redirects']['#access'] = FALSE;
    }
  }
}

/**
 * Titles of specific node types must not be edited manually, but created automatically
 * @see wt_dgm_node_presave()
 */
function _wt_dgm_node_form_alter_hidetitle(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if (!in_array($node->bundle(), ['dgmtv', 'dgmmobile', 'dgminfopoint', 'dgmmenu'])) {
    return;
  }

  $form['title']['#access'] = FALSE;
}

/**
 * Sets livedata fields to readonly
 */
function _wt_dgm_node_form_alter_livedata(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if (!$node->hasField('field_rawdata') ||  in_array('administrator', \Drupal::currentUser()->getRoles())) {
   return;
  }

  $form['field_rawdata']['widget'][0]['value']['#attributes']['readonly'] = 'readonly';
  $form['field_rawdata']['widget'][0]['value']['#attributes']['style'] = 'background-color: #dedede;';
}

/**
 * Sets livedata fields to readonly
 */
function _wt_dgm_node_form_alter_videodescription(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if (!$node->hasField('field_video')) {
    return;
  }

  if ($node->get('field_video')->getFieldDefinition()->getType() == 'link') {
    $form['field_video']['widget'][0]['uri']['#description'] = t('Must be a link to a Youtube-Video.');
  }
}

/**
 * Removes the incorrect link in saved messages for DGM front nodes
 */
function _wt_dgm_node_form_alter_alterDgmSaveMessage(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  $dgmNodeTypes = ['dgmtv', 'dgmmobile', 'dgminfopoint', 'dgmmenu'];
  if (in_array($node->bundle(), $dgmNodeTypes)) {
    $form['actions']['submit']['#submit'][] = '_wt_dgm_alter_dgm_nodetype_save_message';
  }
  if (!in_array($node->bundle(), [...$dgmNodeTypes, 'a_z'])) {
    $form['actions']['submit']['#submit'][] = '_wt_dgm_alter_general_node_save_message';
  }
}

function _wt_dgm_alter_dgm_nodetype_save_message(array $form, FormStateInterface $form_state) {
  $messenger = \Drupal::messenger();
  $saveMessage = $messenger->messagesByType('status');
  if ($saveMessage) {
    /** @var $node \Drupal\node\Entity\Node */
    $node = $form_state->getFormObject()->getEntity();
    $messenger->deleteByType('status');
    $messenger->addStatus(t('@type %title has been updated.', ['@type' => $node->type->entity->label(), '%title' => '']));
  }
}

function _wt_dgm_alter_general_node_save_message(array $form, FormStateInterface $form_state) {
  $messenger = \Drupal::messenger();
  $saveMessage = $messenger->messagesByType('status');
  if ($saveMessage) {
    $node = $form_state->getFormObject()->getEntity();
    $messenger->deleteByType('status');
    $messenger->addStatus(
      t('@type %label was saved. <a href=":view_url">View page</a> | <a href=":edit_url">Edit again</a>',
        [
          '@type' => $node->type->entity->label(),
          '%label' => $node->label(),
          ':view_url' => Url::fromRoute('entity.node.canonical', ['node' => $node->id()])->toString(),
          ':edit_url' => preg_replace(
            "|/node/add/[a-z0-9_]+|",
            "/node/{$node->id()}/edit",
            \Drupal::request()->getRequestUri()
          ),
        ]
      ));
  }
}


/**
 * Singleton node types like DGM mobile or DGM TV startpage should stay on their edit form
 */
function _wt_dgm_node_form_alter_disableredirect(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if (!in_array($node->bundle(), ['dgmtv', 'dgmmobile', 'dgminfopoint', 'dgmmenu', 'a_z'])) {
    return;
  }

  $form['actions']['submit']['#submit'][] = '_wt_dgm_disable_redirect_submit_handler';
}

function _wt_dgm_disable_redirect_submit_handler(array $form, FormStateInterface $form_state) {
  // $form_state->disableRedirect() would cause error on the 2nd save action
  // so simply redirect to the page itself
  $form_state->setRedirect(\Drupal::routeMatch()->getRouteName());
}

/**
 * Singleton node types like DGM mobile or DGM TV startpage
 * have their own route and therefore we need to manually add translation links
 *
 * April 2021: disabled, use local tabs instead
 * @TODO: delete this code
 */
function _wt_dgm_node_form_alter_addTranslation(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  $customTranslationRoutes = [
    'dgmtv' => 'wt_dgm.admin.guide.tv',
    'dgmmobile' => 'wt_dgm.admin.guide.mobile',
    'dgminfopoint' => 'wt_dgm.admin.guide.infopoint',
    'dgmmenu' => 'wt_dgm.admin.guide.menu',
    'a_z' => 'wt_dgm.admin.info.a_z',
  ];
  if (!in_array($node->bundle(), array_keys($customTranslationRoutes))) {
    return;
  }

  $languageManager = \Drupal::languageManager();
  foreach ($languageManager->getLanguages() as $lang) {
    if ($lang->getId() != $languageManager->getCurrentLanguage()->getId()) {
      $form['actions']['translate_'.$lang->getId()] = [
        '#type' => 'link',
        '#url' => Url::fromRoute($customTranslationRoutes[$node->bundle()], [], ['language' => $lang]),
        '#title' => t('Edit %label', ['%label' => $lang->getName()]),
        '#attributes' => [
          'class' => ['action-link']
        ],
        '#weight' => 40,
      ];
    }
  }
}

/**
 *
 * NODE TYPE SPECIFIC FORM ALTERING
 *
 */

/**
 * New events by accomodations can use their own address as default
 * location and organizer data
 */
function wt_dgm_form_node_event_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if ($node->bundle() != 'event') {
    return;
  }

  /** @var  $dgmHelper \Drupal\wt_dgm\DgmHelper */
  $dgmHelper = \Drupal::service('wt_dgm.helper');
  if ($dgmHelper->isDgmUser() && !$dgmHelper->isChannelPublisher()) {
    $user = User::load(\Drupal::currentUser()->getAccount()->id());
    $form['field_locationname']['widget'][0]['value']['#default_value'] = $user->get('field_marketing_name')->value;
    $form['field_locationname']['widget'][0]['value']['#default_value'] = $user->get('field_marketing_name')->value;
    $form['field_street']['widget'][0]['value']['#default_value'] = $user->get('field_street')->value;
    $form['field_city']['widget'][0]['value']['#default_value'] = $user->get('field_city')->value;
    $form['field_postcode']['widget'][0]['value']['#default_value'] = $user->get('field_postcode')->value;
    $form['field_country']['widget'][0]['value']['#default_value'] = $user->get('field_country')->value;
    $form['field_organizername']['widget'][0]['value']['#default_value'] = $user->get('field_marketing_name')->value;
    $form['field_phone']['widget'][0]['value']['#default_value'] = $user->get('field_phone')->value;
    $form['field_email']['widget'][0]['value']['#default_value'] = $user->get('mail')->value;
  }
}


/**
 * Custom validation for webcams
 */
function wt_dgm_form_node_webcam_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  wt_dgm_form_node_webcam_form_alter($form, $form_state, $form_id);
}

function wt_dgm_form_node_webcam_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if ($node->bundle() != 'webcam') {
    return;
  }

  $form['#attached']['library'][] = 'wt_dgm/webcam';
  $form['#validate'][] = '_wt_dgm_node_webcam_validate';
}

function _wt_dgm_node_webcam_validate(&$form, FormStateInterface $form_state) {
  $type = $form_state->getValue(['field_webcam', '0', 'first']);
  $value = $form_state->getValue(['field_webcam', '0', 'second']);
  $result = [];
  switch ($type) {
    case 'iframe':
    case 'image':
      if (strpos($value, 'https://') !== 0 || filter_var($value, FILTER_VALIDATE_URL) == FALSE) {
        $form_state->setErrorByName('field_webcam][0][second', t('%uri is not a secure site (not starting with https://)', ['%uri' => $value]));
      }
      break;
    case 'feratel':
    case 'panomax':
    preg_match('/\d+/', $value, $result);
    if (empty($result) || $result[0] != $value || !intval($value)) {
        $form_state->setErrorByName('field_webcam][0][second', t('ID must be a number'));
      }
      break;
    case 'fotoeu':
      preg_match('/[\w\-]+/', $value, $result);
      if (!array_key_exists(0, $result) || $result[0] != $value || !$value) {
        $form_state->setErrorByName('field_webcam][0][second', t('ID has invalid characters'));
      }
      break;
    default:
      $form_state->setErrorByName('field_webcam][0][first', t('Unknown type'));
      break;
  }
}


/**
 * Add dashboard layout on top of layout edit form as visual hint for editors
 */
function wt_dgm_form_node_dgminfopoint_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  wt_dgm_form_node_dgminfopoint_form_alter($form, $form_state, $form_id);
}

function wt_dgm_form_node_dgminfopoint_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $dashboardHtmlStructure = <<<NOWDOC
   <div class="infopoint__slots">
     <div class="infopoint__slot infopoint__slot--1">1</div>
     <div class="infopoint__slot infopoint__slot--2">2</div>
     <div class="infopoint__slot infopoint__slot--3">3</div>
     <div class="infopoint__slot infopoint__slot--4">4</div>
     <div class="infopoint__slot infopoint__slot--5">5</div>
     <div class="infopoint__slot infopoint__slot--6">6</div>
     <div class="infopoint__slot infopoint__slot--7">7</div>
     <div class="infopoint__slot infopoint__slot--8">8</div>
   </div>
NOWDOC;

  $dashboardVisualization = [
    '#type' => 'inline_template',
    '#template' => $dashboardHtmlStructure,

  ];
  array_unshift($form, $dashboardVisualization);
}

/**
 * Hide the webform config
 */
function wt_dgm_form_node_dgmform_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  wt_dgm_form_node_dgmform_form_alter($form, $form_state, $form_id);
}

function wt_dgm_form_node_dgmform_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $form_state->getFormObject()->getEntity();
  if ($node->bundle() != 'dgmform') {
    return;
  }

  if (!\Drupal::currentUser()->hasPermission('administer guest guide')) {
    if (array_key_exists('field_form', $form)) {
      $form['field_form']['#access'] = FALSE;
    }
  }
}


/**
 *
 * #############################################################################
 * PARAGRAPHS
 * #############################################################################
 *
 */

/**
 * Preprocessing of paragraph forms
 */
function wt_dgm_field_widget_single_element_paragraphs_form_alter(&$element, FormStateInterface $form_state, $context) {
  /** @var  $hostEntity \Drupal\Core\Entity\EntityInterface */
  $hostEntity = $context['items']->getParent()->getEntity();
  $hostEntityBundle = $context['items']->getParent()->getEntity()->bundle();
  $paragraphType = $element['#paragraph_type'];

  _wt_dgm_field_widget_paragraphs_form_alter__global($element, $form_state, $context);


  if ($paragraphType == 'dgmimage') {
    _wt_dgm_field_widget_paragraphs_form_alter__dgmimage($element, $form_state, $context);
  }
  if ($paragraphType == 'dgmiframe') {
    _wt_dgm_field_widget_paragraphs_form_alter__dgmiframe($element, $form_state, $context);
  }
  if ($paragraphType == 'dgmtext') {
    _wt_dgm_field_widget_paragraphs_form_alter__dgmtext($element, $form_state, $context);
  }
  if ($paragraphType == 'dgmslogan') {
    _wt_dgm_field_widget_paragraphs_form_alter__dgmslogan($element, $form_state, $context);
  }
  if ($paragraphType == 'dgmaccordion') {
    _wt_dgm_field_widget_paragraphs_form_alter__dgmaccordion($element, $form_state, $context);
  }
  if ($paragraphType == 'dgmcolumns') {
    _wt_dgm_field_widget_paragraphs_form_alter__dgmcolumns($element, $form_state, $context);
  }
  if ($paragraphType == 'dgmvideo') {
    _wt_dgm_field_widget_paragraphs_form_alter__dgmvideo($element, $form_state, $context);
  }
}

/**
 * Preprocess form fields shared across many paragraph types
 */
function _wt_dgm_field_widget_paragraphs_form_alter__global(&$element, FormStateInterface $form_state, $context) {
  $hostEntityBundle = $context['items']->getParent()->getEntity()->bundle();
  $hostFieldName = $context['items']->getFieldDefinition()->getName();

  if (isset($element['subform']['field_tilesize']['#access'])) {
    $element['subform']['field_tilesize']['#access'] = in_array($hostEntityBundle, ['dgmmobile']);
  }

  if (isset($element['subform']['field_slideduration']['#access'])) {
    $element['subform']['field_slideduration']['#access'] = in_array($hostEntityBundle, ['dgmtv']);
  }

  if (isset($element['top']['actions']['dropdown_actions']['remove_button'])) {
    $element['top']['actions']['actions']['remove_button'] = $element['top']['actions']['dropdown_actions']['remove_button'];
    $element['top']['actions']['actions']['remove_button']['#attributes']['class'] = ['button--extrasmall'];
    unset($element['top']['actions']['dropdown_actions']['remove_button']);
  }
}

function _wt_dgm_field_widget_paragraphs_form_alter__dgmimage(&$element, FormStateInterface $form_state, $context) {
  $hostEntityBundle = $context['items']->getParent()->getEntity()->bundle();
  $hostFieldName = $context['items']->getFieldDefinition()->getName();

  if ($hostEntityBundle != 'dgmtv') {
    $element['subform']['field_imagecrop']['#access'] = FALSE;
  }
}

function _wt_dgm_field_widget_paragraphs_form_alter__dgmiframe(&$element, FormStateInterface $form_state, $context) {
  $element['subform']['field_link']['widget'][0]['uri']['#description'] = t('This must be an external URL such as %url.', ['%url' => 'https://www.domain.com']);
}

function _wt_dgm_field_widget_paragraphs_form_alter__dgmtext(&$element, FormStateInterface $form_state, $context) {
  if (@$element['subform']['field_tilesize']['#access']) {
    $element['subform']['field_tilesize']['#access'] = FALSE;
  }
}


function _wt_dgm_field_widget_paragraphs_form_alter__dgmslogan(&$element, FormStateInterface $form_state, $context) {
  if (@$element['subform']['field_tilesize']['#access']) {
    $element['subform']['field_tilesize']['#access'] = FALSE;
  }
}

function _wt_dgm_field_widget_paragraphs_form_alter__dgmaccordion(&$element, FormStateInterface $form_state, $context) {
  foreach ($element['subform']['field_textsummaries']['widget'] as $idx => $widget) {
    if (isset($element['subform']['field_textsummaries']['widget'][$idx]['summary']['#attributes'])) {
      unset($element['subform']['field_textsummaries']['widget'][$idx]['summary']['#attributes']);
      unset($element['subform']['field_textsummaries']['widget'][$idx]['summary']['#description']);
      $element['subform']['field_textsummaries']['widget'][$idx]['summary']['#title'] = t('Title') . ' ' .($idx + 1);;
      $element['subform']['field_textsummaries']['widget'][$idx]['summary']['#attributes']['rows'] = 1;
      $element['subform']['field_textsummaries']['widget'][$idx]['#title'] = t('Text') . ' ' .($idx + 1);
      $element['subform']['field_textsummaries']['widget'][$idx]['#title_display'] = 'before';
    }
  }
}

function _wt_dgm_field_widget_paragraphs_form_alter__dgmcolumns(&$element, FormStateInterface $form_state, $context) {
  $selector = sprintf('select[name="field_paragraphs[%d][subform][field_collayout]"]', $element['#delta']);
  $element['subform']['field_col3']['#states'] = [
    'visible' => [
      [
        $selector => ['value' => '33-34-33'],
      ]
    ]
  ];
}

/**
 * @see _wt_dgm_dgmvideo_library_presave()
 */
function _wt_dgm_field_widget_paragraphs_form_alter__dgmvideo(&$element, FormStateInterface $form_state, $context) {
  $selector = sprintf('select[name$="[%d][subform][field_node]"]', $element['#delta']);
  $element['subform']['field_node']['widget']['#options']['_none'] = '- Youtube-Link selbst eingeben -';
  $element['subform']['field_link']['#states'] = [
    'visible' => [
      [
        $selector => ['value' => '_none'],
      ],
    ]
  ];
  $element['subform']['field_slideduration']['#states'] = [
    'visible' => [
      [
        $selector => ['value' => '_none'],
      ],
    ]
  ];
  // Videos must be fullwidth on the mobile frontpage
  if ($context['items']?->getEntity()?->bundle() == 'dgmmobile') {
    $element['subform']['field_tilesize']['#access'] = FALSE;
  }
}


/**
 *
 * #############################################################################
 * GLOBAL FIELD WIDGETS
 * #############################################################################
 *
 */

// similar to @see wt_cms_field_widget_single_element_form_alter(), but here for basic image fields on touristic data nodes
function wt_dgm_field_widget_single_element_form_alter(&$element, FormStateInterface $form_state, $context) {
  if ($context['widget'] instanceof ImageWidget) {
    $element['#process'][] = '_wt_dgm_image_field_widget_process';
  }
}

function _wt_dgm_image_field_widget_process($element, FormStateInterface &$form_state, $form) {
  if (isset($element['alt'])) {
    $element['alt']['#title'] = t('Image name');
    unset($element['alt']['#description']);
  }
  return $element;
}


/**
 *
 * #############################################################################
 * VIEWS EXPOSED FORMS
 * #############################################################################
 *
 */


/**
 * Frontend views form preprocessing
 */
function wt_dgm_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  _preprocess_views_category_filter($form,  $form_state, $form_id);
  _preprocess_views_poi_season_filter($form,  $form_state, $form_id);
  _preprocess_views_proximity_filter_label($form,  $form_state, $form_id);
  _preprocess_views_tour_difficulty_filter($form,  $form_state, $form_id);
}

function _preprocess_views_poi_season_filter(&$form, FormStateInterface $form_state, $form_id) {
  $view = $form_state->getStorage('view');
  $display_id = $view['view']->current_display;

  if ($form_id != 'views_exposed_form' || $view['view']->id() != 'poi' || !in_array($display_id, ['dgm_all_list', 'dgm_all_map'])) {
    return;
  }

  if (!isset($form['field_season_target_id']['#options'])) {
    return;
  }

  foreach ($form['field_season_target_id']['#options'] as $idx => $entry) {
    if ($idx === 'All') {
      $form['field_season_target_id']['#options'][$idx] = new TranslatableMarkup('- All seasons -');
      continue;
    }
    else {
      if ($entry instanceof stdClass && is_array($entry->option)) {
        $termId = key($entry->option);
      }
      else {
        $termId = $idx;
      }
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($termId);
      if ($term instanceof Term && $term->hasField('internal_key')) {
        $notInValue = $term->get('internal_key')->value;
        $oppositeValue = ($notInValue == SeasonCacheContext::SUMMER_SEASON) ? SeasonCacheContext::WINTER_SEASON : SeasonCacheContext::SUMMER_SEASON;
        $oppositeTerms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
          'vid' => 'season',
          'internal_key' => $oppositeValue
        ]);
        $oppositeTerm = reset($oppositeTerms);
        if ($oppositeTerm instanceof Term) {
          $oppositeTerm = \Drupal::service('entity.repository')->getTranslationFromContext($oppositeTerm);
          $form['field_season_target_id']['#options'][$idx] = $oppositeTerm->label();
        }
      }
    }
  }
}

function _preprocess_views_proximity_filter_label(&$form, FormStateInterface $form_state, $form_id) {
  $view = $form_state->getStorage('view');

  if (!isset($form['field_geo_proximity']['#options'])) {
    return;
  }

  foreach ($form['field_geo_proximity']['#options'] as $idx => $entry) {
    if ($idx === 'All') {
      $form['field_geo_proximity']['#options'][$idx] = new TranslatableMarkup('No distance limit');
      break;
    }
  }
}

/**
 * - changes the text of the all (or context filter root) option to "- All categories"
 * - removes empty categories
 * - removes categories outside context filter
 * - removes leading hierarchy symbols if they are lower than the context filter root level
 * - converts leading hierarchy symbols '-' to '&nbsp;'
 *
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _preprocess_views_category_filter(&$form, FormStateInterface $form_state, $form_id) {
  $view = $form_state->getStorage('view');
  $type = $view['view']->id();

  if (!isset($form['term_node_tid_depth']['#options'])) {
    return;
  }

  if (!in_array($type, ['event', 'poi', 'gastronomy', 'tour', 'infrastructure'])) {
    return;
  }

  $termsHavingContent = [];
  $termsInContextFilter = [];
  $contextTermId = -1;
  $contextTermLevel = 0;
  $currentFilterValue = $form_state->getUserInput()['term_node_tid_depth'];
  /**
   * always allow the root term of a DGM list
   */
  if (!empty($view['view']->args) && $view['view']->args[0]) {
    $contextTermId = $view['view']->args[0];
    $termsHavingContent[] = $contextTermId;

    $contextTree = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($type, $contextTermId);
    array_walk($contextTree, function(&$v) {
      $v = (int) $v->tid;
    });
    $termsInContextFilter = array_merge([$contextTermId], $contextTree);
    $contextTermLevel = 1;
  }
  else {
    $termsHavingContent[] = 'All';
  }

  /**
   * simplify the filter options
   */
  $simpleFilterArray = [];
  foreach ($form['term_node_tid_depth']['#options'] as $idx => $entry) {
    if ($entry instanceof TranslatableMarkup) {
      $simpleFilterArray[$idx] = $entry->render();
      continue;
    }
    if ($entry instanceof stdClass) {
      $simpleFilterArray[key($entry->option)] = current($entry->option);
      continue;
    }
    $simpleFilterArray[$idx] = $entry;
  }

  $db = \Drupal::database();
  foreach ($simpleFilterArray as $termId => $termName) {
    /**
     * The "all" option text must be descriptive like a form label in the UI
     */
    if ($termId === 'All') {
      $simpleFilterArray[$termId] = t('- All categories -');
      continue;
    }

    if ($termId == $contextTermId) {
      if (preg_match('/[^\-]+/', $termName, $matches, PREG_OFFSET_CAPTURE)) {
        $contextTermLevel = $matches[0][1] + 1;
      }
      $simpleFilterArray[$termId] = t('- All categories -');
      if (empty($currentFilterValue) || $currentFilterValue == 'All') {
        //we've removed the default empty/All entry, reset it to our newly created, faked '- Any -' category
        $form_state->setUserInput(['term_node_tid_depth' => $contextTermId]);
      }
    }

    $descendTerms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($type, $termId);
    array_walk($descendTerms, function(&$v) {
      $v = (int) $v->tid;
    });
    $descendTermIds = array_merge([$termId], $descendTerms);

    $query = $db->select('node_field_data', 'n');
    $query->join("node__field_{$type}", 'f', 'n.nid = f.entity_id AND n.vid=f.revision_id');
    $query->addField('f', "field_{$type}_target_id");
    $query->condition('n.status', 1);
    $query->condition('n.default_langcode', 1);
    $query->condition('n.type', $type);
    $query->condition("field_{$type}_target_id", $descendTermIds, 'IN');
    $hits = $query->countQuery()->execute()->fetchField();
    if ($hits > 0 ) {
      $termsHavingContent[] = $termId;
    }
  }

  $simpleFilterArray = array_intersect_key($simpleFilterArray, array_flip($termsHavingContent));
  if ($termsInContextFilter) {
    $simpleFilterArray = array_intersect_key($simpleFilterArray, array_flip($termsInContextFilter));
  }
  foreach ($simpleFilterArray as $termId => $termName) {
    if ($termId == 'All' || $termId == $contextTermId) {
      continue;
    }
    $termName = preg_replace('/^---/', '         ', $termName);
    $termName = preg_replace('/^--/', '      ', $termName);
    $termName = preg_replace('/^-/', '   ', $termName);
    for ($i=0; $i<$contextTermLevel; $i++) {
      $termName = preg_replace('/   /', '', $termName, 1);
    }
    $simpleFilterArray[$termId] = $termName;
  }
  $form['term_node_tid_depth']['#options'] = $simpleFilterArray;
}

function _preprocess_views_tour_taxonomy_filter(&$form, FormStateInterface $form_state, $form_id) {
  $view = $form_state->getStorage('view');
  $display_id = $view['view']->current_display;

  if ($form_id != 'views_exposed_form' || $view['view']->id() != 'tour' || !in_array($display_id, ['dgm_all_list'])) {
    return;
  }

  if (!isset($form['field_tour_target_id']['#options'])) {
    return;
  }

  $filter = $view['view']->getHandler($display_id, 'filter', 'field_tour_target_id');
  if (($filter['vid'] ?? 'DUMMY_NOT_A_TAXONOMY_FILTER') != 'tour') {
    return;
  }

  $db = \Drupal::database();
  $query = $db->select('node', 'n');
  $query->join('node_field_data', 'nfd', 'n.nid = nfd.nid AND n.vid = nfd.vid');
  $query->join('node__field_tour', 'f', 'n.nid = f.entity_id AND n.vid=f.revision_id');
  $query->addField('f', 'field_tour_target_id');
  $query->condition('nfd.status', 1);
  $query->condition('nfd.type', 'tour');
  $termsHavingContent = $query->distinct()->execute()->fetchCol();
  $termsHavingContent[] = 'All';
  /**
   * hide unused categories
   */
  if (($form['field_tour_target_id']['#options'][0] ?? NULL) instanceof stdClass) {
    foreach ($form['field_tour_target_id']['#options'] as $idx => $opt) {
      if ($idx == 'All') {
        continue;
      }
      if (!in_array(key($opt->option), $termsHavingContent)) {
        unset($form['field_tour_target_id']['#options'][$idx]);
      }
    }
  }
  else {
    $form['field_tour_target_id']['#options'] = array_intersect_key($form['field_tour_target_id']['#options'], array_flip($termsHavingContent));
  }

  foreach ($form['field_tour_target_id']['#options'] as $idx => $entry) {
    if ($idx === 'All') {
      $form['field_tour_target_id']['#options'][$idx] = new TranslatableMarkup('- All tour types -');
    }
  }
}

function _preprocess_views_tour_difficulty_filter(&$form, FormStateInterface $form_state, $form_id) {
  $view = $form_state->getStorage('view');
  $display_id = $view['view']->current_display;

  if ($form_id != 'views_exposed_form' || $view['view']->id() != 'tour' || !in_array($display_id, ['dgm_all_list'])) {
    return;
  }

  if (!isset($form['field_difficulty_value']['#options'])) {
    return;
  }

  foreach ($form['field_difficulty_value']['#options'] as $idx => $entry) {
    if ($idx === 'All') {
      $form['field_difficulty_value']['#options'][$idx] = new TranslatableMarkup('- All ratings -', [], ['context' => 'Tour difficulty level']);
      continue;
    }
  }
}
/**
 * @deprecated
 *
 * Now using Solr Search API instead of normal views
 */
function _preprocess_views_gastronomy_taxonomy_filter(&$form, FormStateInterface $form_state, $form_id) {
  $view = $form_state->getStorage('view');
  $display_id = $view['view']->current_display;

  if ($form_id != 'views_exposed_form' || $view['view']->id() != 'gastronomy' || !in_array($display_id, ['dgm_all_list', 'dgm_all_map'])) {
    return;
  }

  if (!isset($form['term_node_tid_depth']['#options'])) {
    return;
  }

  $filter = $view['view']->getHandler($display_id, 'filter', 'term_node_tid_depth');
  if (($filter['vid'] ?? 'DUMMY_NOT_A_TAXONOMY_FILTER') != 'gastronomy') {
    return;
  }

  $db = \Drupal::database();
  $query = $db->select('node', 'n');
  $query->join('node_field_data', 'nfd', 'n.nid = nfd.nid AND n.vid = nfd.vid');
  $query->join('node__field_gastronomy', 'f', 'n.nid = f.entity_id AND n.vid=f.revision_id');
  $query->addField('f', 'field_gastronomy_target_id');
  $query->condition('nfd.status', 1);
  $query->condition('nfd.type', 'gastronomy');
  $termsHavingContent = $query->distinct()->execute()->fetchCol();
  $termsHavingContent[] = 'All';
  /**
   * hide unused categories
   */
  /**
   * hide unused categories
   */
  if (($form['term_node_tid_depth']['#options'][0] ?? NULL) instanceof stdClass) {
    foreach ($form['term_node_tid_depth']['#options'] as $idx => $opt) {
      if ($idx == 'All') {
        continue;
      }
      if (!in_array(key($opt->option), $termsHavingContent)) {
        unset($form['term_node_tid_depth']['#options'][$idx]);
      }
    }
  }
  else {
    $form['term_node_tid_depth']['#options'] = array_intersect_key($form['term_node_tid_depth']['#options'], array_flip($termsHavingContent));
  }
}
