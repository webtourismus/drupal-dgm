<?php

use Drupal\Core\Config\FileStorage;
use Drupal\field\Entity\FieldConfig;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\user\Entity\Role;
use \Drupal\Core\Config\InstallStorage;
use Symfony\Component\Yaml\Yaml;

/**
 * Copy default images from wg_dgm module to public files dir
 * and create file_managed database entry so they can be used for image styles
 */
function wt_dgm_update_9001(&$sandbox) {
  $sourceDir = \Drupal::service('file_system')->realpath( \Drupal::service('extension.list.module')->getPath('wt_dgm') ) . '/images/defaultimages/';
  $targetDir = \Drupal::service('file_system')->realpath('public://defaultimages/') . '/';
  if (!file_exists($targetDir)) {
    mkdir($targetDir, 0775);
  }
  foreach (new \FilesystemIterator($sourceDir) as $file) {
    if (file_exists($targetDir . $file->getFilename())) {
      continue;
    }
    $image = file_get_contents($sourceDir . $file->getFilename());
    $file = \Drupal::service('file.repository')->writeData($image, 'public://defaultimages/' . $file->getFilename());
  }
}

/**
 * Mirror referenced event location+organizer into event fields.
 */
function wt_dgm_update_9002(&$sandbox) {
  $etm = \Drupal::entityTypeManager()->getStorage('node');
  $orgCounter = 0;
  $locCounter = 0;

  $query = $etm->getQuery();
  $query->condition('type', 'event');
  $query->exists('field_location');
  $query->accessCheck(FALSE);
  $eventIds = $query->execute();

  if ($eventIds) {
    foreach ($eventIds as $eventId) {
      $event = $etm->load($eventId);
      $location = $etm->load($event->field_location->target_id);
      if ($location) {
        $event->set('field_locationname', $location->get('title')->getValue());
        $event->set('field_street', $location->get('field_street')->getValue());
        $event->set('field_city', $location->get('field_city')->getValue());
        $event->set('field_postcode', $location->get('field_postcode')->getValue());
        $event->set('field_country', $location->get('field_country')->getValue());
        $event->set('field_geo', $location->get('field_geo')->getValue());
        $event->set('field_parking', $location->get('field_parking')->getValue());
        $event->set('field_region', $location->get('field_region')->getValue());
        $event->save();
        $locCounter++;
      }
    }
  }

  $query = $etm->getQuery();
  $query->condition('type', 'event');
  $query->exists('field_organizer');
  $query->accessCheck(FALSE);
  $eventIds = $query->execute();

  if ($eventIds) {
    foreach ($eventIds as $eventId) {
      $event = $etm->load($eventId);
      $organizer = $etm->load($event->field_organizer->target_id);
      if ($organizer) {
        $event->set('field_organizername', $organizer->get('title')->getValue());
        $event->set('field_email', $organizer->get('field_email')->getValue());
        $event->set('field_phone', $organizer->get('field_phone')->getValue());
        $event->set('field_homepage', $organizer->get('field_homepage')->getValue());
        $event->save();
        $orgCounter++;
      }
    }
  }

  return "$orgCounter event organisations updated, $locCounter event locations updated";
}

/**
 * Sort multivalue event dates by start and end date.
 * Fill missing values.
 */
function wt_dgm_update_9003(&$sandbox) {
  $etm = \Drupal::entityTypeManager()->getStorage('node');
  $counter = 0;

  $query = $etm->getQuery();
  $query->condition('type', 'event');
  $query->exists('field_dates');
  $query->accessCheck(FALSE);
  $eventIds = $query->execute();


  if ($eventIds) {
    foreach ($eventIds as $eventId) {
      $event = $etm->load($eventId);
      $dates = $event->get('field_dates')->getValue();
      if (count($dates) > 1) {
        usort($dates, function($a, $b) {
          if ($a['value'] ==  $b['value'] && $a['end_value'] ==  $b['end_value']) {
            return 0;
          }
          if ($a['value'] < $b['value']) {
            return -1;
          }
          if ($a['end_value'] < $b['end_value']) {
            return -1;
          }
          return 1;
        });
        $event->set('field_dates', $dates);
        $event->save();
        $counter++;
      }
    }
  }

  return "Sorted start and end dates in $counter events";
}

/**
 * Copy default images from wg_dgm module to public files dir
 * and create file_managed database entry so they can be used for image styles
 */
function wt_dgm_update_9004(&$sandbox) {
  $sourceDir = \Drupal::service('file_system')->realpath( \Drupal::service('extension.list.module')->getPath('wt_dgm') ) . '/images/cityimages/';
  $targetDir = \Drupal::service('file_system')->realpath('public://cityimages/') . '/';
  if (!file_exists($targetDir)) {
    mkdir($targetDir, 0775);
  }
  foreach (new \FilesystemIterator($sourceDir) as $file) {
    if (file_exists($targetDir . $file->getFilename())) {
      continue;
    }
    $image = file_get_contents($sourceDir . $file->getFilename());
    $file = \Drupal::service('file.repository')->writeData($image, 'public://cityimages/' . $file->getFilename());
  }
}

/**
 * Create default taxonomies for hotel star rating or similar
 */
function wt_dgm_update_9005(&$sandbox) {
  $vocabularyId = 'hotelstars';
  $vocabulary = \Drupal\taxonomy\Entity\Vocabulary::load($vocabularyId);
  //check for existance of vocabulary
  if (!$vocabulary) {
    return;
  }

  //only create initial entries when vocabulary is empty
  $query = \Drupal::entityQuery('taxonomy_term');
  $query->condition('vid', $vocabularyId);
  $query->accessCheck(FALSE);
  $termIds = $query->execute();
  if ($termIds) {
    return;
  }

  //hotelstars.eu
  $parentTerm = Term::create([
    'vid' => $vocabularyId,
    'langcode' => 'de',
    'default_langcode' => 1,
    'name' => 'Hotel Sterne (EU)',
    'parent' => null,
    'status' => 1,
    'weight' => 0,
  ]);
  $parentTerm->save();
  foreach (['1 Stern', '1 Stern Superior', '2 Sterne', '2 Sterne Superior', '3 Sterne', '3 Sterne Superior', '4 Sterne', '4 Sterne Superior', '5 Sterne', '5 Sterne Superior'] as $termWeight => $termName) {
    $starTerm = Term::create([
      'vid' => $vocabularyId,
      'langcode' => 'de',
      'default_langcode' => 1,
      'name' => $termName,
      'parent' => $parentTerm->id(),
      'status' => 1,
      'weight' => $termWeight,
    ]);
    $starTerm->save();
  }

  //privatvermieter österreich
  $parentTerm = Term::create([
    'vid' => $vocabularyId,
    'langcode' => 'de',
    'default_langcode' => 1,
    'name' => 'Privatvermieterverband (AT)',
    'parent' => null,
    'status' => 1,
    'weight' => 1,
  ]);
  $parentTerm->save();
  foreach (['2 Edelweiß', '3 Edelweiß', '4 Edelweiß', '5 Edelweiß'] as $termWeight => $termName) {
    $starTerm = Term::create([
      'vid' => $vocabularyId,
      'langcode' => 'de',
      'default_langcode' => 1,
      'name' => $termName,
      'parent' => $parentTerm->id(),
      'status' => 1,
      'weight' => $termWeight,
    ]);
    $starTerm->save();
  }

  //urlaub am bauernhof österreich
  $parentTerm = Term::create([
    'vid' => $vocabularyId,
    'langcode' => 'de',
    'default_langcode' => 1,
    'name' => 'Urlaub am Bauernhof (AT)',
    'parent' => null,
    'status' => 1,
    'weight' => 2,
  ]);
  $parentTerm->save();
  foreach (['2 Blumen', '3 Blumen', '4 Blumen', '5 Blumen'] as $termWeight => $termName) {
    $starTerm = Term::create([
      'vid' => $vocabularyId,
      'langcode' => 'de',
      'default_langcode' => 1,
      'name' => $termName,
      'parent' => $parentTerm->id(),
      'status' => 1,
      'weight' => $termWeight,
    ]);
    $starTerm->save();
  }
}

/**
 * Create default taxonomies for accomodation type taxonomy
 */
function wt_dgm_update_9006(&$sandbox) {
  $vocabularyId = 'accommodation';
  $vocabulary = \Drupal\taxonomy\Entity\Vocabulary::load($vocabularyId);
  //check for existance of vocabulary
  if (!$vocabulary) {
    return;
  }

  //only create initial entries when vocabulary is empty
  $query = \Drupal::entityQuery('taxonomy_term');
  $query->condition('vid', $vocabularyId);
  $query->accessCheck(FALSE);
  $termIds = $query->execute();
  if ($termIds) {
    return;
  }

  $defaultAccomodationTypes = [
    'hotel' => 'Hotel',
    'bed-breakfast' => 'Gasthof / Hotel Garni / B&B',
    'ferienwohnung' => 'Ferienwohnung / Apartment',
    'gaestehaus' => 'Gästehaus / Pension',
    'chalet' => 'Chalet / Hütte',
    'bauernhof' => 'Bauernhof',
    'wellness-hotel' => 'Wellness-hotel',
    'golfhotel' => 'Golfhotel',
    'familienhotel' => 'Kinderhotel / Familienhotel',
    'reiterhof' => 'Reiterhof',
    'hostel' => 'Hostel / Jugendherberge',
    'campingplatz' => 'Campingplatz',
  ];

  $idx = 0;
  foreach ($defaultAccomodationTypes as $termUrl => $termLabel) {
    $starTerm = Term::create([
      'vid' => $vocabularyId,
      'langcode' => 'de',
      'default_langcode' => 1,
      'name' => $termUrl,
      'field_facet' => $termLabel,
      'parent' => NULL,
      'status' => 1,
      'weight' => $idx,
    ]);
    $starTerm->save();
    $idx++;
  }
}

/**
 * Add new "administer portal" permission to "editor" role
 */
function wt_dgm_update_9007(&$sandbox) {
  /** @var $editorRole Role */
  $editorRole = Role::load('editor');
  $editorRole->grantPermission('administer portal');
  $editorRole->save();
}

/**
 * Add new "default_admin_domain" setting
 */
function wt_dgm_update_9008(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable('wt_dgm.settings');
  if (!$config->get('default_admin_domain')) {
    $config->set('default_admin_domain', 'admin' . $config->get('default_guide_domain'));
    $config->save(TRUE);
  }
}

/**
 * Create tour node. Create new DGM tour paragraph.
 *
 * @deprecated
 */
function wt_dgm_update_9009(&$sandbox) {
  /*
  $configInstaller = \Drupal::service('config.installer');
  $modulePath = \Drupal::service('module_handler')->getModule('wt_dgm')->getPath();
  $configPath = $modulePath . '/' . InstallStorage::CONFIG_OPTIONAL_DIRECTORY;

  // Field storages need to be installed first, before other config
  $fieldStorageFiles = [
    'field.storage.node.field_difficulty',
    'field.storage.node.field_duration',
    'field.storage.node.field_tour',
    'field.storage.node.field_gpx',
  ];
  foreach ($fieldStorageFiles as $fieldStorageFile) {
    $fieldStorageConfig = \Drupal::configFactory()->getEditable($fieldStorageFile);
    if (!$fieldStorageConfig->get('uuid')) {
      $fieldStorageContent = file_get_contents("{$configPath}/{$fieldStorageFile}.yml");
      $fieldStorageData = (array) Yaml::parse($fieldStorageContent);
      $fieldStorageConfig->setData($fieldStorageData);
      $fieldStorageConfig->save(TRUE);
    }
  }

  // Install all missing optional configs. No fear of overwriting config,
  // optional config will be ignored if existing or missing dependencies.
  $storage = new FileStorage($configPath);
  $configInstaller->installOptionalConfig($storage);
  */
}

/**
 * Add tour to known Portal and DGM node types
 */
function wt_dgm_update_9010(&$sandbox) {
  // Update wt_dgm specific config
  $dgmConfig = \Drupal::configFactory()->getEditable('wt_dgm.settings');
  $portalNodeTypes = $dgmConfig->get('portal_nodetypes');
  if (!in_array('tour', $portalNodeTypes)) {
    $portalNodeTypes[] = 'tour';
    $dgmConfig->set('portal_nodetypes', $portalNodeTypes);
    $guideNodeTypes = $dgmConfig->get('guide_nodetypes');
    $guideNodeTypes[] = 'tour';
    $dgmConfig->set('guide_nodetypes', $guideNodeTypes);
    $dgmConfig->save(TRUE);
  }
}

/**
 * Add tour to allowed slides in DGM TV
 */
function wt_dgm_update_9011(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmtv.field_slides");
  if (!in_array('dgmtour', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmtour']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgmtour', 'dgmtour');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmevent.weight') ?? 1;
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmtour', ['enabled' => TRUE, 'weight' => $weight]);
    $config->save(TRUE);
  }
}

/**
 * Add tour and tour list to allowed tiles in DGM infopoint
 */
function wt_dgm_update_9012(&$sandbox) {
  $fields = [
    'field_slot1',
    'field_slot2',
    'field_slot3',
    'field_slot4',
    'field_slot5',
    'field_slot6',
    'field_slot7',
    'field_slot8',
  ];
  foreach ($fields as $field) {
    $config = \Drupal::configFactory()->getEditable("field.field.node.dgminfopoint.{$field}");
    if (!in_array('dgmtour', $config->get('settings.handler_settings.target_bundles'))) {
      $configDependencies = $config->get('dependencies.config');
      $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmtour', 'paragraphs.paragraphs_type.dgmtours']));
      $config->set('dependencies.config', $configDependencies);
      $config->set('settings.handler_settings.target_bundles.dgmtour', 'dgmtour');
      $config->set('settings.handler_settings.target_bundles.dgmtours', 'dgmtours');
      $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmevent.weight') ?? 1;
      $config->set('settings.handler_settings.target_bundles_drag_drop.dgmtour', ['enabled' => TRUE, 'weight' => $weight - 1]);
      $config->set('settings.handler_settings.target_bundles_drag_drop.dgmtours', ['enabled' => TRUE, 'weight' => $weight]);
      $config->save(TRUE);
    }
  }
}

/**
 * Add tour and tour list to allowed tiles in DGM infopoint menu
 */
function wt_dgm_update_9013(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmmenu.field_menu");
  if (!in_array('dgmtour', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmtour', 'paragraphs.paragraphs_type.dgmtours']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgmtour', 'dgmtour');
    $config->set('settings.handler_settings.target_bundles.dgmtours', 'dgmtours');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmevent.weight');
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmtour', ['enabled' => TRUE, 'weight' => $weight - 1]);
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmtours', ['enabled' => TRUE, 'weight' => $weight]);
    $config->save(TRUE);
  }
}

/**
 * Add tour and tour list to allowed tiles in DGM mobile
 */
function wt_dgm_update_9014(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmmobile.field_mobile");
  if (!in_array('dgmtour', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmtour', 'paragraphs.paragraphs_type.dgmtours']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgmtour', 'dgmtour');
    $config->set('settings.handler_settings.target_bundles.dgmtours', 'dgmtours');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmevent.weight');
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmtour', ['enabled' => TRUE, 'weight' => $weight - 1]);
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmtours', ['enabled' => TRUE, 'weight' => $weight]);
    $config->save(TRUE);
  }
}

/**
 * Create tour related permissions
 */
function wt_dgm_update_9015(&$sandbox) {
  $permissionSet = [
    'editor' => [
      'create tour content',
      'delete any tour content',
      'edit any tour content',
      'create terms in tour',
      'delete terms in tour',
      'edit terms in tour',
    ],
    'association' => [
      'create tour content',
      'delete own tour content',
      'edit own tour content',
    ],
    'dgm' => [
      'create tour content',
      'delete own tour content',
      'edit own tour content',
    ],
  ];
  foreach ($permissionSet as $role => $permissions) {
    /** @var $role Role */
    $role = Role::load($role);
    if ($role) {
      foreach ($permissions as $permission) {
        $role->grantPermission($permission);
      }
      $role->save();
    }
  }
}

/**
 * Copy tour default image from wg_dgm module to public files dir
 */
function wt_dgm_update_9016(&$sandbox) {
  $sourceDir = \Drupal::service('file_system')->realpath( \Drupal::service('extension.list.module')->getPath('wt_dgm') ) . '/images/defaultimages/';
  $targetDir = \Drupal::service('file_system')->realpath('public://defaultimages/') . '/';
  if (!file_exists($targetDir)) {
    mkdir($targetDir, 0775);
  }
  $fileName = 'tour.jpg';
  if (file_exists("{$targetDir}{$fileName}")) {
    return;
  }
  $image = file_get_contents("{$sourceDir}{$fileName}");
  $file = \Drupal::service('file.repository')->writeData($image, "public://defaultimages/{$fileName}");
}

/**
 * Add avalanche to known Portal and DGM node types
 */
function wt_dgm_update_9017(&$sandbox) {
  // Update wt_dgm specific config
  $dgmConfig = \Drupal::configFactory()->getEditable('wt_dgm.settings');
  $portalNodeTypes = $dgmConfig->get('portal_nodetypes');
  if (!in_array('avalanche', $portalNodeTypes)) {
    $portalNodeTypes[] = 'avalanche';
    $dgmConfig->set('portal_nodetypes', $portalNodeTypes);
    $guideNodeTypes = $dgmConfig->get('guide_nodetypes');
    $guideNodeTypes[] = 'avalanche';
    $dgmConfig->set('guide_nodetypes', $guideNodeTypes);
    $dgmConfig->save(TRUE);
  }
}

/**
 * Add avalanche to allowed slides in DGM TV
 */
function wt_dgm_update_9018(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmtv.field_slides");
  if (!in_array('dgmavalanche', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmavalanche']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgmavalanche', 'dgmavalanche');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmlink.weight') ?? 1;
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmavalanche', ['enabled' => TRUE, 'weight' => $weight - 1]);
    $config->save(TRUE);
  }
}

/**
 * Add avalanche to allowed tiles in DGM infopoint
 */
function wt_dgm_update_9019(&$sandbox) {
  $fields = [
    'field_slot1',
    'field_slot2',
    'field_slot3',
    'field_slot4',
    'field_slot5',
    'field_slot6',
    'field_slot7',
    'field_slot8',
  ];
  foreach ($fields as $field) {
    $config = \Drupal::configFactory()->getEditable("field.field.node.dgminfopoint.{$field}");
    if (!in_array('dgmavalanche', $config->get('settings.handler_settings.target_bundles'))) {
      $configDependencies = $config->get('dependencies.config');
      $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmavalanche']));
      $config->set('dependencies.config', $configDependencies);
      $config->set('settings.handler_settings.target_bundles.dgmavalanche', 'dgmavalanche');
      $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmlink.weight') ?? 1;
      $config->set('settings.handler_settings.target_bundles_drag_drop.dgmavalanche', ['enabled' => TRUE, 'weight' => $weight - 1]);
      $config->save(TRUE);
    }
  }
}

/**
 * Add avalanche to allowed tiles in DGM infopoint menu
 */
function wt_dgm_update_9020(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmmenu.field_menu");
  if (!in_array('dgmavalanche', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmavalanche']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgmavalanche', 'dgmavalanche');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmlink.weight');
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmavalanche', ['enabled' => TRUE, 'weight' => $weight - 1]);
    $config->save(TRUE);
  }
}

/**
 * Add avalanche to allowed tiles in DGM mobile
 */
function wt_dgm_update_9021(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmmobile.field_mobile");
  if (!in_array('dgmavalanche', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgmavalanche']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgmavalanche', 'dgmavalanche');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmlink.weight');
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgmavalanche', ['enabled' => TRUE, 'weight' => $weight - 1]);
    $config->save(TRUE);
  }
}

/**
 * Create "masquerade_field" related permissions
 */
function wt_dgm_update_9023(&$sandbox) {
  $permissionSet = [
    'editor' => [
      'view own masquerade field',
      'view any masquerade field',
      'edit masquerade field',
    ],
    'association' => [
      'view own masquerade field',
    ],
    'organizer' => [
      'view own masquerade field',
    ],
    'dgm' => [
      'view own masquerade field',
    ],
  ];
  foreach ($permissionSet as $role => $permissions) {
    /** @var $role Role */
    $role = Role::load($role);
    if ($role) {
      foreach ($permissions as $permission) {
        $role->grantPermission($permission);
      }
      $role->save();
    }
  }
}

/**
 * Copy infrastructure default image from wg_dgm module to public files dir
 */
function wt_dgm_update_9024(&$sandbox) {
  $sourceDir = \Drupal::service('file_system')->realpath( \Drupal::service('extension.list.module')->getPath('wt_dgm') ) . '/images/defaultimages/';
  $targetDir = \Drupal::service('file_system')->realpath('public://defaultimages/') . '/';
  if (!file_exists($targetDir)) {
    mkdir($targetDir, 0775);
  }
  $fileName = 'infrastructure.jpg';
  if (file_exists("{$targetDir}{$fileName}")) {
    return;
  }
  $image = file_get_contents("{$sourceDir}{$fileName}");
  /**
   * @var $file \Drupal\file\FileInterface
   */
  $file = \Drupal::service('file.repository')->writeData($image, "public://defaultimages/{$fileName}");
  if ($file) {
    /**
     * @var $crop \Drupal\crop\Entity\Crop
     */
    $crop = \Drupal::entityTypeManager()->getStorage('crop')->create([
      'type' => 'focal_point',
      'langcode' => 'de',
      'entity_id' => $file->id(),
      'entity_type' => 'file',
      'uri' => $file->getFileUri(),
      'x' => 580,
      'y' => 460,
      'default_langcode' => 1,
    ]);
    $crop->save();
  }
}

/**
 * Add infrastructure to known DGM node types
 */
function wt_dgm_update_9025(&$sandbox) {
  // Update wt_dgm specific config
  $dgmConfig = \Drupal::configFactory()->getEditable('wt_dgm.settings');
  $guideNodeTypes = $dgmConfig->get('guide_nodetypes');
  if (!in_array('infrastructure', $guideNodeTypes)) {
    $guideNodeTypes[] = 'infrastructure';
    $dgmConfig->set('guide_nodetypes', $guideNodeTypes);
    $dgmConfig->save(TRUE);
  }
}

/**
 * Add infrastructure to allowed tiles in DGM infopoint
 */
function wt_dgm_update_9026(&$sandbox) {
  $fields = [
    'field_slot1',
    'field_slot2',
    'field_slot3',
    'field_slot4',
    'field_slot5',
    'field_slot6',
    'field_slot7',
    'field_slot8',
  ];
  foreach ($fields as $field) {
    $config = \Drupal::configFactory()->getEditable("field.field.node.dgminfopoint.{$field}");
    if (!in_array('dgminfrastructures', $config->get('settings.handler_settings.target_bundles'))) {
      $configDependencies = $config->get('dependencies.config');
      $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgminfrastructure', 'paragraphs.paragraphs_type.dgminfrastructures']));
      $config->set('dependencies.config', $configDependencies);
      $config->set('settings.handler_settings.target_bundles.dgminfrastructure', 'dgminfrastructure');
      $config->set('settings.handler_settings.target_bundles.dgminfrastructures', 'dgminfrastructures');
      $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmgastronomies.weight') ?? 1;
      $config->set('settings.handler_settings.target_bundles_drag_drop.dgminfrastructure', ['enabled' => TRUE, 'weight' => $weight + 1]);
      $config->set('settings.handler_settings.target_bundles_drag_drop.dgminfrastructures', ['enabled' => TRUE, 'weight' => $weight + 1]);
      $config->save(TRUE);
    }
  }
}

/**
 * Add infrastructure list to allowed tiles in DGM infopoint menu
 */
function wt_dgm_update_9027(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmmenu.field_menu");
  if (!in_array('dgminfrastructures', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgminfrastructure', 'paragraphs.paragraphs_type.dgminfrastructures']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgminfrastructure', 'dgminfrastructure');
    $config->set('settings.handler_settings.target_bundles.dgminfrastructures', 'dgminfrastructures');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmgastronomies.weight');
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgminfrastructure', ['enabled' => TRUE, 'weight' => $weight + 1]);
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgminfrastructures', ['enabled' => TRUE, 'weight' => $weight + 1]);
    $config->save(TRUE);
  }
}

/**
 * Add infrastructure list to allowed tiles in DGM mobile
 */
function wt_dgm_update_9028(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmmobile.field_mobile");
  if (!in_array('dgminfrastructures', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgminfrastructure', 'paragraphs.paragraphs_type.dgminfrastructures']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgminfrastructure', 'dgminfrastructure');
    $config->set('settings.handler_settings.target_bundles.dgminfrastructures', 'dgminfrastructures');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmgastronomies.weight');
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgminfrastructure', ['enabled' => TRUE, 'weight' => $weight + 1]);
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgminfrastructures', ['enabled' => TRUE, 'weight' => $weight + 1]);
    $config->save(TRUE);
  }
}

/**
 * Create infrastructure related permissions
 */
function wt_dgm_update_9029(&$sandbox) {
  $permissionSet = [
    'editor' => [
      'create infrastructure content',
      'delete any infrastructure content',
      'edit any infrastructure content',
      'create terms in infrastructure',
      'delete terms in infrastructure',
      'edit terms in infrastructure',
    ],
    'association' => [
      'create infrastructure content',
      'delete own infrastructure content',
      'edit own infrastructure content',
    ],
    'dgm' => [
      'create infrastructure content',
      'delete own infrastructure content',
      'edit own infrastructure content',
    ],
  ];
  foreach ($permissionSet as $role => $permissions) {
    /** @var $role Role */
    $role = Role::load($role);
    if ($role) {
      foreach ($permissions as $permission) {
        $role->grantPermission($permission);
      }
      $role->save();
    }
  }
}


/**
 * Add infrastructure to allowed slides in DGM TV
 */
function wt_dgm_update_9030(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable("field.field.node.dgmtv.field_slides");
  if (!in_array('dgminfrastructure', $config->get('settings.handler_settings.target_bundles'))) {
    $configDependencies = $config->get('dependencies.config');
    $configDependencies = array_unique(array_merge($configDependencies, ['paragraphs.paragraphs_type.dgminfrastructure']));
    $config->set('dependencies.config', $configDependencies);
    $config->set('settings.handler_settings.target_bundles.dgminfrastructure', 'dgminfrastructure');
    $weight = $config->get('settings.handler_settings.target_bundles_drag_drop.dgmgastronomies.weight') ?? 1;
    $config->set('settings.handler_settings.target_bundles_drag_drop.dgminfrastructure', ['enabled' => TRUE, 'weight' => $weight + 1]);
    $config->save(TRUE);
  }
}

/**
 * Add default content override permissions
 */
function wt_dgm_update_9031(&$sandbox) {
  $roleIds = ['association', 'editor'];
  foreach ($roleIds as $roleId) {
    $role = Role::load($roleId);
    if ($role instanceof Role) {
      $role->grantPermission('wt_dgm.content_override.categories');
      $role->save();
    }
  }
}
