<?php

use Drupal\Core\Cache\Cache;
use Drupal\wt_dgm\DgmHelper;

/**
 * this preprocess is called only once very early in rendering
 * contrary to a global hook_template_preprocess() this hook is called
 * only once and then statically quest for the rest of the request
 */
function wt_dgm_template_preprocess_default_variables_alter(&$variables) {
  /** @var $helper \Drupal\wt_dgm\DgmHelper */
  $helper = \Drupal::service('wt_dgm.helper');
  $variables['dgm']['channel'] = $helper->getChannel();
  $variables['dgm']['user'] = $helper->getFrontendUser();
}

/**
 * we need these preprocessings in both portal and DGM, so we do it in module
 * and not in theme
 */
function wt_dgm_preprocess_node(&$variables) {
  _wt_dgm_preprocess_node_channel_classes($variables);
  _wt_dgm_preprocess_rawdata_to_object($variables);
  _wt_dgm_preprocess_weather_nodes($variables);
  _wt_dgm_preprocess_node_youtube_video($variables);
}

/**
 * Include channel specific CSS classes on node's root DOM tag
 */
function _wt_dgm_preprocess_node_channel_classes(&$variables) {
  $channel = \Drupal::service('wt_dgm.helper')->getChannel();
  if ($channel) {
    $variables['attributes']['class'] = array_merge($variables['attributes']['class'] ?? [], [
      'node--' . $channel,
      $variables['node']->bundle() . '--' . $channel,
      $variables['node']->bundle() . '--' . $channel . '--' . $variables['view_mode'],
    ]);
  }
}

/**
 * include object versions of  field_rawdata strings so
 * we can easily access and traverse through that data in Twig
 */
function _wt_dgm_preprocess_rawdata_to_object(&$variables) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $variables['elements']['#node'];

  if (!$node->hasField('field_rawdata')) {
    return;
  }

  $rawString = trim($node->get('field_rawdata')->value);
  if (!$rawString) {
    return;
  }

  if (stripos($rawString, '<!DOCTYPE html>') === 0 ||
    preg_match('/^<(html|body|div|p|span)(?![a-z])/i', $rawString) === 1) {
    $type = 'html';
  }
  elseif (strpos($rawString, '<') === 0) {
    $type = 'xml';
  }
  elseif (strpos($rawString, '{') === 0) {
    $type = 'json';
  }
  else {
    $variables['rawdata'] = [
      '#type' => NULL,
      '#data' => 'Unknown rawdata type',
    ];
    return;
  }

  // rawdata from migrate might deliver sub-parts of an XML file
  if ($type == 'xml' && !strpos($rawString, '<?xml') === 0) {
    $rawString = '<?xml version="1.0" encoding="UTF-8" ?>' . $rawString;
  }

  $variables['rawdata']['#type'] = $type;
  switch ($type) {
    case 'html':
      libxml_clear_errors();
      $oldErrorHandling = libxml_use_internal_errors(true);
      $domDocument = new \DOMDocument();
      $domDocument->loadHTML($rawString);
      $variables['rawdata']['#data'] = $domDocument;
      libxml_clear_errors();
      libxml_use_internal_errors($oldErrorHandling);
      break;
    case 'xml':
      $variables['rawdata']['#data'] = simplexml_load_string($rawString);
      break;
    case 'json':
      $variables['rawdata']['#data'] = json_decode($rawString, FALSE, 512, JSON_INVALID_UTF8_IGNORE);
      break;
    default:
      break;
  }
}

/**
 * add object and caching information so we can
 * easily show the "current" weather (by hour of day)
 */
function _wt_dgm_preprocess_weather_nodes(&$variables) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $variables['elements']['#node'];

  if ($node->bundle() != 'weather') {
    return;
  }

  // weather nodes are cached/might not be updated for a longer time
  // (usually imported only once a day),
  // but the "current" weather should to be recalculated every hour
  $variables['#cache']['tags'][] = \Drupal\wt_cms\CmsHelper::CACHETAG_HOUR;

  if ($node->get('remote_datasource')->value == 'DASWETTER') {
    /** @var $xml \SimpleXMLElement */
    $xml = $variables['rawdata']['#data'];
    if (!$xml instanceof SimpleXMLElement) {
      if (!strpos($xml, '<?xml') === 0) {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>' . $xml;
      }
      $xml = simplexml_load_string($xml);
    }
    if (!$xml instanceof SimpleXMLElement) {
      $variables['rawdata']['daswetter_now'] = 'No weather data found';
      return;
    }
    $dayString = date('Ymd');
    $dayXml = $xml->xpath('day[@value="' . $dayString . '"]');
    if (!isset($dayXml[0])) {
      $variables['rawdata']['daswetter_now'] = 'Current day not found';
      return;
    }

    // dasWetter.com has weather values every 3 hours at 02:00, 05:00, 08:00 etc.
    $hourNumber = intval(max(0, (round(intval(date('G') + 1) / 3) - 1)) * 3 + 2);
    $hourString = str_pad($hourNumber, 2, '0', STR_PAD_LEFT) . ':00';
    $hourXml = $dayXml[0]->xpath('hour[@value="' . $hourString . '"]');
    if (!isset($hourXml[0])) {
      $variables['rawdata']['daswetter_now'] = 'Current hour not found';
      return;
    }
    $variables['rawdata']['daswetter_now'] = $hourXml[0];
  }
}


function _wt_dgm_preprocess_node_youtube_video(&$variables) {
  /** @var $node \Drupal\node\Entity\Node */
  $node = $variables['elements']['#node'];

  if (!$node->hasField('field_video')) {
    return;
  }

  if ($node->get('field_video')->isEmpty()) {
    return;
  }

  $variables['embed_link'] = FALSE;
  $variables['video_id'] = FALSE;

  $link = $node->get('field_video')->first()->getUrl()->toString();

  /* convert "normal" youtube links to embed links with privacy */
  preg_match('/(https?:\/\/)?(((m|www)\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)/i', $link, $matches);
  if (isset($matches[8]) && strlen($matches[8]) == 11) {
    // on touch devices we currently embed the nocookie iframe
    $variables['embed_link'] = 'https://www.youtube-nocookie.com/embed/' . $matches[2];
    $variables['video_id'] = $matches[8];
  }
  else {
    $variables['video_id'] = $link;
  }
}

function wt_dgm_preprocess_field(array &$variables, $hook) {
  _wt_dgm_preprocess_field_eventdate($variables, $hook);
  _wt_dgm_preprocess_field_tourgpx($variables, $hook);
}

function _wt_dgm_preprocess_field_eventdate(array &$variables, $hook) {
  if ($variables['field_type'] != 'wt_eventdate') {
    return;
  }
  $variables['attributes']['class'][] = 'eventdate';

  foreach ($variables['items'] as $idx => $item) {
    $variables['items'][$idx]['attributes']['class'][] = 'eventdate__item';
    if (array_key_exists('soldout', $variables['items'][$idx]['content'])) {
      $variables['items'][$idx]['attributes']['class'][] = 'eventdate__item--soldout';
    }
    if (array_key_exists('canceled', $variables['items'][$idx]['content'])) {
      $variables['items'][$idx]['attributes']['class'][] = 'eventdate__item--canceled';
    }
  }
}

function _wt_dgm_preprocess_field_tourgpx(array &$variables, $hook) {
  if ($variables['field_type'] != 'file' || $variables['field_name'] != 'field_gpx') {
    return;
  }
  $variables['attributes']['class'][] = 'leaflet__maps';
  $variables['attributes']['class'][] = 'leaflet__maps--tour';

  $node = $variables['element']['#object'];
  foreach ($variables['items'] as $idx => $item) {
    $variables['items'][$idx]['attributes']['id'] = 'leaflet__map--' . $node->id() . '_' . $idx;
    $variables['items'][$idx]['attributes']['data-gpx-file'] = $variables['element']['#object']->get('field_gpx')[$idx]->entity->createFileUrl(FALSE);
    $variables['items'][$idx]['attributes']['class'][] = 'leaflet__map';
    $variables['items'][$idx]['attributes']['class'][] = 'leaflet__map--tour';
  }
  if (!empty($variables['dgm']['user'])) {
    $variables['#attached']['drupalSettings']['wt_dgm']['user']['field_geo'] = [
      $variables['dgm']['user']->get('field_geo')->lat,
      $variables['dgm']['user']->get('field_geo')->lng
    ];
    $variables['#attached']['drupalSettings']['wt_dgm']['user']['field_marketing_name'] = $variables['dgm']['user']->get('field_marketing_name')->value;
  }
}

function wt_dgm_preprocess_breadcrumb(&$variables) {
  _wt_dgm_preprocess_breadcrumb_geoContextFilter($variables);
}

function _wt_dgm_preprocess_breadcrumb_geoContextFilter(&$variables) {
  $parameters = \Drupal::routeMatch()->getParameters()->all();
  if (empty($parameters['view_id']) || $parameters['view_id'] != 'cms') {
    return;
  }
  $geoArg = FALSE;
  foreach ($parameters as $key => $value) {
    if (strpos($key, 'arg_') != 0) {
      continue;
    }
    if (empty($value)) {
      continue;
    }
    if (preg_match('/^\d+(\.\d+)?,\d+(\.\d+)?$/', $value) == 1) {
      $geoArg = $key;
      break;
    }
  }
  if ($geoArg === FALSE) {
    return;
  }

  //there are 2 breadcrumbs before any view arg breadcrumb: home + the view itself
  $breadcrumbPosition = intval(substr($geoArg, strlen('arg_'))) + 2;

  $variables['breadcrumb'][$breadcrumbPosition]['text'] = t('Location filter');
  $variables['breadcrumb'][$breadcrumbPosition]['url'] = NULL;

  //remove succeeding geo-radius breadcrumb
  foreach ($variables['breadcrumb'] as $idx => $crumb) {
    if ($idx <= $breadcrumbPosition) {
      continue;
    }
    unset($variables['breadcrumb'][$idx]);
  }
}

function wt_dgm_preprocess_paragraph(&$variables) {
  _wt_dgm_preprocess_paragraph_tile_language($variables);
}

function _wt_dgm_preprocess_paragraph_tile_language(&$variables) {
  /** @var $paragraph \Drupal\paragraphs\Entity\Paragraph */
  $paragraph = $variables['paragraph'];
  if (!in_array($paragraph->getParentEntity()->bundle(), ['dgminfopoint', 'dgmmenu', 'dgmmobile', 'dgmtv'])) {
    return;
  }

  // prevent stale cache on untranslatable ERR host fields with retrospectivly translated referenced entities on the paragraph
  $cacheContext = $paragraph->getCacheContexts();
  $variables['#cache']['contexts'] = Cache::mergeContexts($cacheContext, ['languages:language_content', 'languages:language_interface', 'languages:language_url']);
}
