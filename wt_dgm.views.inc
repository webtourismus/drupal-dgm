<?php

/**
 * @file
 * Provides views data for the wt_eventdate field type
 */

use Drupal\field\FieldStorageConfigInterface;
use Solarium\QueryType\Select\Query\Query;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Entity\Index;
use \Drupal\Core\Utility\Error;

/**
 * Implements hook_field_views_data().
 */
function wt_dgm_field_views_data(FieldStorageConfigInterface $field_storage) {
  // Include datetime.views.inc file in order for helper function
  // datetime_type_field_views_data_helper() to be available.
  \Drupal::moduleHandler()->loadInclude('datetime', 'inc', 'datetime.views');

  // Get datetime field data for value and end_value.
  $data = datetime_type_field_views_data_helper($field_storage, [], 'value');
  $data = datetime_type_field_views_data_helper($field_storage, $data, 'end_value');

  return $data;
}

function wt_dgm_views_data_alter(&$data) {
  _wt_dgm_views_data_alter__core__current_dgm_user($data);
  _wt_dgm_views_data_alter__searchapi__current_dgm_values($data);
}

/**
 * @see wt_dgm_views_query_substitutions
 * @see \Drupal\wt_dgm\Plugin\views\filter\DgmUser
 *
 * inspired by CurrentUser filter from core user module
 */
function _wt_dgm_views_data_alter__core__current_dgm_user(&$data) {
  $data['users']['dgm_user'] = [
    'real field' => 'uid',
    'title' => t('DGM Frontend User'),
    'help' => t('Filter the view to the DGM frontend user of the domain.'),
    'filter' => [
      'id' => 'dgm_user',
      'type' => 'yes-no',
    ],
  ];
}

/**
 * refactored _wt_dgm_views_data_alter__core__current_dgm_user for search_api
 * module
 *
 * @see \Drupal\wt_dgm\Plugin\views\filter\DgmUserSearchApi
 * @see \Drupal\wt_dgm\Plugin\views\filter\DgmSeasonSearchApi
 */
function _wt_dgm_views_data_alter__searchapi__current_dgm_values(&$data) {

  /** @var \Drupal\search_api\IndexInterface $index */
  foreach (Index::loadMultiple() as $index) {
    try {
      $table = 'search_api_index_' . $index->id();
      $data[$table]['dgm_user_search_api'] = [
        'title' => t('DGM Frontend User'),
        'help' => t('Filter the view to the DGM frontend user of the domain (Search API).'),
        'filter' => [
          'field' => 'uid',
          'id' => 'dgm_user_search_api',
          'type' => 'yes-no',
        ],
      ];
      $data[$table]['dgm_season_search_api'] = [
        'title' => t('DGM Frontend Season'),
        'help' => t('Filter the view to the DGM frontend season of the domain (Search API).'),
        'filter' => [
          'field' => 'field_season_internal_key',
          'id' => 'dgm_season_search_api',
          'type' => 'yes-no',
        ],
      ];
    }
    catch (\Exception $e) {
      $args = [
        '%index' => $index->label(),
      ];
      $logger = \Drupal::logger('search_api');
      Error::logException($logger, $e, '%type while computing Views data for index %index: @message in %function (line %line of %file).', $args);
    }
  }
}
